<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class TourEndCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $connection = ConnectionManager::get('default');
        $today      = date('Y-m-d');

        $query      = "UPDATE mst_tour " .
            "SET status = 9 " .
            "WHERE id IN " .
            "( " .
                "SELECT tour_id " .
                "FROM mst_tour_price " .
                "GROUP BY tour_id " .
                "HAVING MAX(depdt) < '" . $today . "' " .
            ");";
           
        echo $query . "\n";
        $result     = $connection->query($query);
        echo "done.\n";
    }
}
