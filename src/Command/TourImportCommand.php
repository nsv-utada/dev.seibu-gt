<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

use App\Helper\TourImport;

class TourImportCommand extends Command
{
    protected function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser->addOption('mail', [
            'help' => 'Set to "no" or "n" to skip report by email, default is yes'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Begin');
        $io->out("Started import from API");

        $allowEmail      = $args->getOption('mail');
        $start           = date('Y/m/d H:i:s');
        $import          = new TourImport();
        $import->showLog = true;
        if (in_array(strtolower($allowEmail), ["n", "no"])) {
            $allowEmail = false;
        } else {
            $allowEmail = true;
        }

        $response = $import->main();
        if ($response['success']) {
            $messages = $response['messages'];
            foreach ($messages as $tourcd => $message) {
                $io->out($tourcd . ": " . $message);
            }
        } else {
            $io->out($response['error']);
        }
        $end = date('Y/m/d H:i:s');
        
        if ($allowEmail) {
            $this->sendReport($response, $start, $end);
        }

        $io->out("Ended import from API");
        
        $io->out('End');
    }

    public function sendReport($response, $start, $end)
    {
        $email  = new Email('default');
        /*
        $admins = Configure::read("admins");
        $sendTo = [];
        foreach ($admins as $admin) {
            $sendTo[] = $admin['email'];
        }
        if (!count($sendTo)) {
            return;
        }
        */

        $sendTo = ['h.kasuta@bud-international.co.jp'];
        //$sendTo = ['t.utada@dip-tech.jp'];
        
        $email->setViewVars([
            'response' => $response,
            'start'    => $start,
            'end'      => $end
        ]);

        $env = Configure::read("env");

        if ($response['success']) {
            if ($env == 'dev') {
                $subject = "【西部バス-開発-結果:OK】ツアーデータ更新完了のご連絡";
            } else {
                $subject = "【西部バス-本番-結果:OK】ツアーデータ更新完了のご連絡";
            }
        } else {
            if ($env == 'dev') {
                $subject = "【西部バス-開発-結果:NG】ツアーデータ更新完了のご連絡";
            } else {
                $subject = "【西部バス-本番-結果:NG】ツアーデータ更新完了のご連絡";
            }
        }

        $email->viewBuilder()->setTemplate('tour_import_report');
        $email->setFrom(["no-reply@seibu-gt.com" => "Seibu Bus"])
            ->setTo($sendTo)
            ->setBcc("pg_groupe@netsurfvn.com")
            ->setEmailFormat('html')
            ->setSubject($subject)
            ->send();
    }
}
