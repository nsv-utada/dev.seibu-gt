<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class NaiyoCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tourcd    = '18075-14';
        $schedules = TableRegistry::get('MstTourSchedule')->find()
            ->where(['tourcd' => $tourcd])
            ->order(['MstTourSchedule.nichiji' => 'ASC', 'MstTourSchedule.gyono' => 'ASC']);
        
        foreach ($schedules as $schedule) {
            $this->format($schedule);
        }
    }

    public function format($schedule)
    {
        $fields = ['naiyo1', 'naiyo2', 'naiyo3'];
        foreach ($fields as $field) {
            $value = $schedule->$field;
            echo $field . ": '" . $value , "'\n";
            //echo "result: \n";
            $result  = [];
            $matches = [];
            preg_match_all("/ +|\xc2\xa0+|[^( |\xc2\xa0)]+/u", $value, $matches);
            foreach ($matches[0] as $match) {
                echo "'" . $match . "'\n";
            }
            echo "\n";
        }
    }
}
