<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Auth\DefaultPasswordHasher;

class MakePasswordCommand extends Command
{
    protected function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser->addOption(
            'password',
            [
                'help' => 'Specify Password',
            ]
        );

        return $parser;
    }
    
    public function execute(Arguments $args, ConsoleIo $io)
    {
        echo "\n";
        $password = $args->getOption('password');
        echo $password . "\n";
        $hasher   = new DefaultPasswordHasher();
        $password = $hasher->hash($password);
        echo $password . "\n";
    }
}
