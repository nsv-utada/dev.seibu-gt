<?php

namespace App\Helper;

use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Cake\Log\Log;

class TourImport
{
    public $showLog     = false;
    public $batchNumber = 0;
    public $importTempData  = [];
    public $importMainData  = [];

    public function main()
    {
        $response = ['success' => 0, 'error' => ''];
        // step 1 read xml
        $result   = $this->getXml();
        if (!$result['success']) {
            return $result;
        }
        $toursData  = $result['data'];

        // step 2 build data
        if (!count($toursData)) {
            $response['error'] = "データが見つかりません。";
            return $response;
        }

        // step 3 clean temp tables
        $result = $this->deleteTempTables();
        if (!$result['success']) {
            return $result;
        }

        // step 3 import data to temp tables
        try {
            // first import is main tour data and tour list with pattern 1
            $this->importTempTables($toursData, 1);

            
            $tourcds      = array_keys($toursData);
            $batches      = array_chunk($tourcds, 10);
            $inputParams  = [];
            // get more data from pattern 2 and 3
            $this->batchNumber     = 0;
            foreach ($batches as $batch) {
                $this->batchNumber++;
                $inputParams['HV_TOUR_CD'] = implode(",", $batch);

                $inputParams['HV_PTN'] = 2;
                $result = $this->getXml($inputParams);
                $this->importTempTables($result['data'], 2);

                $inputParams['HV_PTN'] = 3;
                $result = $this->getXml($inputParams);
                $this->importTempTables($result['data'], 3);
            }

            //$this->log(json_encode($this->importTempData));
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
            $response['error'] = $e->getMessage();
            return $response;
        }
        
        // step 4 import data to main tables
        try {
            $messages = $this->updateMainTables();
            $response['success']  = 1;
            $response['messages'] = $messages;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * get xml from api and return result
     *
     * @param boolean|array $inputParams params of API
     * @return array
     */
    public function getXml($inputParams = false)
    {
        $response = ['success' => 0, 'error' => ''];
        //$xmlFile = WWW_ROOT . "/tours/data/import.xml";
        //$xml = simplexml_load_file($xmlFile);
        try {
            $url    = Configure::read('tour_import_api.url');
            $params = Configure::read('tour_import_api.params');

            if (!$inputParams) {
                $tourCds     = [];
                // using current year to set into params (from last 2 digit + 00 to last 2 digi + 99)                
                // add last year
                $lastYear    = (int)date('Y') - 1;
                $last2Digit  = (int)substr((string)$lastYear, -2);
                $startYear   = (int)($last2Digit . "00");
                $endYear     = (int)($last2Digit . "99");
                for ($i = $startYear; $i <= $endYear; $i++) {
                    $tourCds[] = $i;
                }
                
                // this year               
                $thisYear    = (int)date('Y');
                $last2Digit  = (int)substr($thisYear, -2);
                $startYear   = (int)($last2Digit . "00");
                $endYear     = (int)($last2Digit . "99");
                for ($i = $startYear; $i <= $endYear; $i++) {
                    $tourCds[] = $i;
                }


                $tourCds              = implode(",", $tourCds);
                //echo $tourCds;exit;
                $params['HV_TOUR_CD'] = $tourCds;
            } else {
                // if there is input params then overwrite the default
                $params['HV_TOUR_CD'] = $inputParams['HV_TOUR_CD'];
                $params['HV_PTN']     = $inputParams['HV_PTN'];
            }

            $url     = $url . "?" . http_build_query($params);
            $this->log($url);
            $http    = new Client();
            $httpRes = $http->get($url);
            if (!$httpRes->isOk()) {
                throw new \Exception("API endpoint " . $url . " http code: " . $httpRes->getStatusCode());
            }
            $xml     = $httpRes->getStringBody();
            $path    = WWW_ROOT . "tours/data/";
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            // save xml to file
            file_put_contents($path . "import_" . $this->batchNumber . "_" . $inputParams['HV_PTN'] . ".xml", $xml);
            $xml    = mb_convert_encoding($xml, "UTF-8", "SJIS-win");
            libxml_use_internal_errors(true);
            $xml    = @simplexml_load_string($xml);
            if ($xml === false) {
                $errors = [];
                foreach (libxml_get_errors() as $error) {
                    $errors[] = $error->message;
                }
                $response['error'] = implode(", ", $errors);
            } else {
                $toursData           = [];
                foreach ($xml->tourlist->tour as $tourXml) {
                    $tourcd = (string)$tourXml->tourcd;
                    if ($tourcd) {
                        $toursData[$tourcd] = $tourXml;
                    }
                }
                $response['success'] = 1;
                $response['data']    = $toursData;
            }
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
            $response['error'] = $e->getMessage();
        }

        return $response;
    }

    public function importTempTables($toursData, $pattern)
    {
        $table   = TableRegistry::get('TempMstTour');
        foreach ($toursData as $tourcd => $tourXml) {
            if (!isset($this->importTempData[$tourcd])) {
                $this->importTempData[$tourcd] = [];
            }
            $item = TableRegistry::get('TempMstTour')->find()
                ->where(['tourcd' => $tourcd])
                ->first();
            if (!$item || !$item->id) {
                $item = TableRegistry::get('TempMstTour')->newEntity();
                $item->tourcd      = $tourcd;
            }
            
            if (isset($tourXml->tournm)) {
                $item->tournm                                 = (string)$tourXml->tournm;
                $this->importTempData[$tourcd]['tournm']      = $item->tournm;
            }

            if (isset($tourXml->priod)) {
                $item->priod                                  = (int)$tourXml->period;
                $this->importTempData[$tourcd]['priod']       = $item->priod;
            }

            if (isset($tourXml->destination)) {
                $item->destination                            = (string)$tourXml->destination;
                $this->importTempData[$tourcd]['destination'] = $item->destination;
            }

            if ($pattern == 1) {
                $this->importPattern1($table, $item, $tourXml);
            }

            if ($pattern == 2) {
                $this->importPattern2($table, $item, $tourXml);
            }

            if ($pattern == 3) {
                $this->importPattern3($table, $item, $tourXml);
            }
            $table->save($item);
        }
    }

    public function importPattern1($table, $item, $tourXml)
    {
        $tourcd = $item->tourcd;
        if (isset($tourXml->depdtlist->depdt)) {
            $tourPrices = [];
            foreach ($tourXml->depdtlist->children() as $depdt) {
                $date  = (string)$depdt;
                // format to Y-m-d
                $date  = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
                $price = (string)$depdt['price'];
                $tourPrices[] = [
                    'depdt'  => $date,
                    'price'  => $price,
                    'tourcd' => $item->tourcd
                ];
            }
            $this->importTempData[$tourcd]['tour_prices'] = $tourPrices;
            $table->patchEntity($item, ['tour_prices' => $tourPrices]);
        }
    }

    public function importPattern2($table, $item, $tourXml)
    {
        $tourcd                                       = $item->tourcd;
        $item->priod                                  = (int)$tourXml->period;
        $item->destination                            = (string)$tourXml->destination;
        $this->importTempData[$tourcd]['priod']       = $item->priod;
        $this->importTempData[$tourcd]['destination'] = $item->destination;
    }

    public function importPattern3($table, $item, $tourXml)
    {
        $tourcd      = $item->tourcd;
        if (isset($tourXml->depdtlist->depinfo)) {
            $platforms = [];
            foreach ($tourXml->depdtlist->children() as $depinfo) {
                $date  = (string)$depinfo->depdt;
                // format to Y-m-d
                $date  = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
                if (isset($depinfo->platformlist->platform)) {
                    foreach ($depinfo->platformlist->children() as $platform) {
                        $type = (string)$platform['type'];
                        
                        $time = (string)$platform['time'];
                        $platforms[] = [
                            'tourcd'   => $item->tourcd,
                            'depdt'    => $date,
                            'platform' => (string)$platform,
                            'time'     => $time,
                            'type'     => $type
                        ];
                    }
                }
            }
            $table->patchEntity($item, ['tour_platforms' => $platforms]);
            $this->importTempData[$tourcd]['tour_platforms'] = $platforms;
        }

        if (isset($tourXml->courselist)) {
            $schedules = [];
            foreach ($tourXml->courselist->children() as $course) {
                $stringFields = ['naiyo1', 'naiyo2', 'naiyo3'];
                $schedule     = [];
                foreach ($stringFields as $stringField) {
                    $string = (string)$course->$stringField;
                    //$string = str_replace(" ", "→", $string);
                    if (strlen($string) > 500) {
                        throw new \Exception('Data too long: "' . (string)$course->$stringField . '"');
                    }
                    $schedule[$stringField] = $string;
                }
                
                $schedule['tourcd']  = $item->tourcd;
                $schedule['type']    = (int)$course->type;
                $schedule['nichiji'] = (int)$course->nichiji;
                $schedule['gyono']   = (int)$course->gyono;

                $schedules[]         = $schedule;
            }
            $table->patchEntity($item, ['tour_schedules' => $schedules]);
            $this->importTempData[$tourcd]['tour_schedules'] = $schedules;
        }
    }

    public function updateMainTables()
    {
        $messages   = [];

        $tempItems  = TableRegistry::get('TempMstTour')->find()
            ->contain(['TempTourPrices', 'TempTourPlatforms', 'TempTourSchedules']);
        
        $table      = TableRegistry::get('MstTour');
        $attributes = ['tournm', 'priod', 'destination'];
        foreach ($tempItems as $tempItem) {
            $tourcd = $tempItem->tourcd;
            $this->importMainData[$tourcd] = [];
            
            $item   = $table->find()
                ->contain(['TourPrices', 'TourPlatforms', 'TourSchedules'])
                ->where(['tourcd' => $tourcd])
                ->first();
            
            if (!$item || !$item->id) {
                $isNew          = true;
                $item           = $table->newEntity();
                $item->tourcd   = $tourcd;

                // default values
                $item->status   = 0;
                $item->autoup   = 1;
                $item->tourcon  = 0;
                $item->busguid  = 0;
                $this->importMainData[$tourcd]['status']  = $item->status;
                $this->importMainData[$tourcd]['autoup']  = $item->autoup;
                $this->importMainData[$tourcd]['tourcon'] = $item->tourcon;
                $this->importMainData[$tourcd]['busguid'] = $item->busguid;
            } else {
                if (!$item->autoup) {
                    // skip item doesn't have auto update
                    $messages[$tourcd] = "Ignore, auto update is disabled";
                    continue;
                }
                $isNew = false;
            }

            $updates = [];
            foreach ($attributes as $attribute) {
                if ($item->$attribute != $tempItem->$attribute) {
                    $item->$attribute    = $tempItem->$attribute;
                    $updates[$attribute] = $tempItem->$attribute;

                    $this->importMainData[$tourcd][$attribute] = $item->$attribute;
                }
            }

            if (is_null($item->tournm)) {
                $item->tournm = "";
                $this->importMainData[$tourcd]['tournm'] = $item->tournm;
            }

            $tourPrices = [];
            if ($tempItem->tour_prices && count($tempItem->tour_prices)) {
                foreach ($tempItem->tour_prices as $tourPrice) {
                    $tourPrices[] = [
                        'depdt'  => $tourPrice['depdt'],
                        'tourcd' => $tourPrice['tourcd'],
                        'price'  => $tourPrice['price']
                    ];
                }
            }
            $table->patchEntity($item, ['tour_prices' => $tourPrices]);
            $this->importMainData[$tourcd]['tour_prices'] = $tourPrices;
            $this->log($item->tourcd . " tour_prices: " . count($tourPrices));

            $tourPlatforms = [];
            if ($tempItem->tour_platforms && count($tempItem->tour_platforms)) {
                foreach ($tempItem->tour_platforms as $tourPlatform) {
                    $tourPlatforms[] = [
                        'tourcd'   => $tourPlatform['tourcd'],
                        'platform' => $tourPlatform['platform'],
                        'depdt'    => $tourPlatform['depdt']
                    ];
                }
            }
            $table->patchEntity($item, ['tour_platforms' => $tourPlatforms]);
            $this->importMainData[$tourcd]['tour_platforms'] = $tourPlatforms;
            $this->log($item->tourcd . " tour_platforms: " . count($tourPlatforms));

            $tourSchedules = [];
            if ($tempItem->tour_schedules && count($tempItem->tour_schedules)) {
                foreach ($tempItem->tour_schedules as $tourSchedule) {
                    $tourSchedules[] = [
                        'tourcd'  => $tourSchedule['tourcd'],
                        'type'    => $tourSchedule['type'],
                        'nichiji' => $tourSchedule['nichiji'],
                        'gyono'   => $tourSchedule['gyono'],
                        'naiyo1'  => $tourSchedule['naiyo1'],
                        'naiyo2'  => $tourSchedule['naiyo2'],
                        'naiyo3'  => $tourSchedule['naiyo3'],
                    ];
                }
            }
            $table->patchEntity($item, ['tour_schedules' => $tourSchedules]);
            $this->importMainData[$tourcd]['tour_schedules'] = $tourSchedules;
            $this->log($item->tourcd . " tour_schedules: " . count($tourSchedules));

            try {
                $table->save($item);
                if ($isNew) {
                    $messages[$tourcd] = "Created";
                } else {
                    $messages[$tourcd] = "Updated";
                }
            } catch (\Exception $e) {
                $messages[$tourcd] = "Error: " . $e->getMessage();
                $this->log($e->getMessage());
                $this->log($e->getTraceAsString());
            }
        }

        return $messages;
    }

    public function deleteTempTables()
    {
        $response  = ['success' => 0, 'error' => ''];
        $conn      = ConnectionManager::get('default');
        try {
            $conn->begin();
            //TableRegistry::get("TempMstTourPurpose")->deleteAll([]);
            TableRegistry::get("TempMstTourPrice")->deleteAll(['id > 0']);
            TableRegistry::get("TempMstTourPlatform")->deleteAll(['id > 0']);
            TableRegistry::get("TempMstTourSchedule")->deleteAll(['id > 0']);
            TableRegistry::get("TempMstTour")->deleteAll(['id > 0']);
            
            
            $conn->commit();
            $response['success'] = 1;
        } catch (\PDOException $e) {
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
            $response['error'] = $e->getMessage();
            $conn->rollback();
        }

        return $response;
    }

    public function log($message)
    {
        Log::info($message);
        if ($this->showLog) {
            print_r($message);
            echo "\n";
        }
    }
}
