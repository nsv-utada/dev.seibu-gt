<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;

class MstTour extends Entity
{
    const STATUSES = [
        0 => "非公開",
        1 => "公開",
        9 => "終了"
    ];

    const STATUS_PRIVATE = 0;
    const STATUS_PUBLIC  = 1;
    const STATUS_ENDED   = 9;

    public function getImageUrl($imageField)
    {
        if ($imageField && $this->$imageField) {
            if (filter_var($this->$imageField, FILTER_VALIDATE_URL)) {
                return $this->$imageField;
            }

            return Router::url("/", true) . "/tours/images/" . $this->id . "/" . $this->$imageField;
        } else {
            return "";
        }
    }
}
