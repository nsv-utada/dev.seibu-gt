<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstTourPurposeTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_tour_purpose');
        $this->setPrimaryKey('id');
    }
    
}
