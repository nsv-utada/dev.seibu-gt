<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstTourScheduleTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_tour_schedule');
        $this->setPrimaryKey('id');
    }
}
