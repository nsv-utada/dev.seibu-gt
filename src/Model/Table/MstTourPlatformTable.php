<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstTourPlatformTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_tour_platform');
        $this->setPrimaryKey('id');
    }
}
