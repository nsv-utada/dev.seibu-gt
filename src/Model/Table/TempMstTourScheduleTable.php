<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TempMstTourScheduleTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('temp_mst_tour_schedule');
        $this->setPrimaryKey('id');
    }
}
