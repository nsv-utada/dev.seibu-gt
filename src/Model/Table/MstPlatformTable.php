<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstPlatformTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_platform');
        $this->setPrimaryKey('id');
    }
}
