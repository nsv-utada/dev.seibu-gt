<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TempMstTourTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('temp_mst_tour');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'register_date' => 'new',
                    'update_date'   => 'always',
                ]
            ]
        ]);


        $this->hasMany('TempTourPrices', [
            'className'    => 'App\Model\Table\TempMstTourPriceTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TempTourPrices.depdt' => 'ASC']
        ])
        ->setProperty('tour_prices');

        $this->hasMany('TempTourPlatforms', [
            'className'    => 'App\Model\Table\TempMstTourPlatformTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TempTourPlatforms.depdt' => 'ASC']
        ])
        ->setProperty('tour_platforms');

        $this->hasMany('TempTourSchedules', [
            'className'    => 'App\Model\Table\TempMstTourScheduleTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TempTourSchedules.gyono' => 'ASC']
        ])
        ->setProperty('tour_schedules');
    }
}
