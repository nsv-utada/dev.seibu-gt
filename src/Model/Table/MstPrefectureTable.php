<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstPrefectureTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_prefecture');
        $this->setPrimaryKey('id');
    }
}
