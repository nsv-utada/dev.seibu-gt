<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstTourTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_tour');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'register_date' => 'new',
                    'update_date'   => 'always',
                ]
            ]
        ]);

        $this->hasMany('Purposes', [
            'className'    => 'App\Model\Table\MstTourPurposeTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace'
        ])
        ->setProperty('purposes');

        $this->hasMany('TourPrices', [
            'className'    => 'App\Model\Table\MstTourPriceTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TourPrices.depdt' => 'ASC']
        ])
        ->setProperty('tour_prices');

        $this->hasMany('TourPlatforms', [
            'className'    => 'App\Model\Table\MstTourPlatformTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TourPlatforms.depdt, TourPlatforms.platform' => 'ASC']
        ])
        ->setProperty('tour_platforms');

        $this->hasMany('TourSchedules', [
            'className'    => 'App\Model\Table\MstTourScheduleTable',
            'foreignKey'   => 'tour_id',
            'saveStrategy' => 'replace',
            'sort'         => ['TourSchedules.nichiji,TourSchedules.gyono' => 'ASC']
        ])
        ->setProperty('tour_schedules');
    }
    
    public function mkSearchBy($conds = ['1 = 1'], $order = ['ID' => 'DESC'], $limit = 10)
    {
        $rst = $this->find()
            ->contain([
                'Purposes',
                'TourPrices',
                'TourPlatforms',
                'TourSchedules'
            ])
            ->join([
                'MstTourPurpose' => [
                    'table'      => 'mst_tour_purpose',
                    'type'       => 'LEFT',
                    'conditions' => [
                        'MstTourPurpose.tour_id = MstTour.id'
                    ]
                ],
                'TourPlatforms'  => [
                    'table'      => 'mst_tour_platform',
                    'type'       => 'LEFT',
                    'conditions' => [
                        'TourPlatforms.tour_id =MstTour.id'
                    ]
                ],
                'MstTourPrice'   => [
                    'table'      => 'mst_tour_price',
                    'type'       => 'LEFT',
                    'conditions' => [
                        'MstTourPrice.tour_id =MstTour.id'
                    ]
                ],
                'MstTourSchedule'   => [
                    'table'      => 'mst_tour_schedule',
                    'type'       => 'LEFT',
                    'conditions' => [
                        'MstTourSchedule.tour_id =MstTour.id'
                    ]
                ]
            ])
            ->select()
            ->group(['MstTour.id'])
            ->where($conds);
            //->having(['max(MstTourPrice.depdt) IS NOT' =>NULL]);
        if ($order) {
            $rst->order($order);
        }
        if ($limit) {
            $rst->limit($limit);
        }
        return $rst;
    }
}
