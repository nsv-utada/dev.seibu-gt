<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TempMstTourPlatformTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('temp_mst_tour_platform');
        $this->setPrimaryKey('id');
    }
}
