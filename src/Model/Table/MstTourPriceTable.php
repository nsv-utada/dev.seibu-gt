<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstTourPriceTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_tour_price');
        $this->setPrimaryKey('id');
    }
}
