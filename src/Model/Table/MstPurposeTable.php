<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstPurposeTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_purpose');
        $this->setPrimaryKey('id');
    }
    public function mkSearchBy($conds = ['1 = 1'], $order = ['ID' => 'DESC'], $limit = 5)
    {
    	$rst = $this->find()
    		->select()
            ->where($conds);
        if ($order) {
            $rst->order($order);
        }
        if ($limit) {
            $rst->limit($limit);
        }
        return $rst;
    }
}
