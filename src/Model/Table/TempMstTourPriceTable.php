<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TempMstTourPriceTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('temp_mst_tour_price');
        $this->setPrimaryKey('id');
    }
}
