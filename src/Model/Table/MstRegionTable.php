<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MstRegionTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('mst_region');
        $this->setPrimaryKey('id');
    }
}
