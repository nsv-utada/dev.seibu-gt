<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class LogAdminAccessTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('log_admin_access');
        $this->setPrimaryKey('id');
    }
}
