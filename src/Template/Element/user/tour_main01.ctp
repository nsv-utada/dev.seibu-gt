<style type="text/css">
    ins{
        height: auto !important;
    }
    .pre_tag{
        white-space: pre-wrap;       /* Since CSS 2.1 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
    }
</style>
<?php
use Cake\ORM\TableRegistry;
    function checkImageExist($url,$id,$path){
       if(@file_get_contents($url,0,NULL,0,1)){
            return $url;
        }
        $url_new = WWW_ROOT .'tours'.DS.'images'.DS. $id.DS.$url;
        if(file_exists($url_new) && $url !='') return  $path.'webroot/tours/images/'. $id.'/'.$url;
        return '#';
    }
 if(isset($rst)){
            foreach ($rst as $value) {
                $price = [];
                $date_price = '';
                $dates= [];
                foreach ($value['tour_prices'] as  $v) {
                   $price[] = $v['price'];
                   if($v['price'] !='')$dates[] = date('Y/m/d', strtotime($v['depdt']));
                   if($v['depdt'] != NULL && $v['depdt'] !='' ){

                   }
                }
                $str_depdt = '';
                if($dates){
                    if(max($dates) == min($dates)){
                        $str_depdt = max($dates);
                    }else{
                        $str_depdt = min($dates).'～'.max($dates) ;
                    }
                }
                $str_price = '';
                if($price){
                     if(max($price) == min($price)){
                        $str_price = number_format(max($price)).'円';
                    }else{
                         $str_price = number_format(min($price)).'円 ~'.number_format(max($price)).'円' ;
                    }
                }
                $url =checkImageExist($value['mimage1'],$value['id'], $this->Url->build('/', true));

                $str_period = '';

                if((int)$value['priod'] == 1){
                    $str_period ='日帰り';
                }else if((int)$value['priod'] > 1){
                    $str_period =((int)$value['priod'] - 1).'泊'.(int)$value['priod'].'日';
                }

                $str_tourcon = '';
                if((int)$value['tourcon'] === 0){
                    $str_tourcon = 'なし';
                }else if($value['tourcon'] == 1){
                    $str_tourcon = '同行';
                }

                $str_busguid = '';
                if((int)$value['busguid'] === 0){
                    $str_busguid = 'なし';
                }else if($value['busguid'] == 1){
                    $str_busguid = '同行';
                }
                $TourPlatform = [];

                foreach ($value['tour_platforms'] as  $v) {
                   if(!in_array($v['platform'], $TourPlatform)) $TourPlatform[] = $v['platform'];
                }
                $platform =($TourPlatform)?  implode("、",$TourPlatform):'';
              
               
         ?>
        <li>
           
            
            <table>
                <tr>
                    <tr>
                        <th scope="row">
                            <p class="resultsCaption05 radius2">設定期間</p>
                        </th>
                        <td class="td_deft"><?=$str_depdt ?> <?php if(!empty($value['depdted'])){ ?><p class="resultsIcon01 margin-left-4">出発決定日あり</p> <?php }else{ ?>
               
            <?php } ?></td>
                    </tr>
                </tr>
            </table>
            <H3 class="resultsH301 pre_tag"><?= nl2br(($value['tournm'])) ?></H3>
            <div class="resultaBox01">
                <div class="left01"><?php if($url !='#'){ ?><img src="<?=$url ?>" > <?php } ?></div>
                <div class="right01">
                    <table class="resultsTable01">
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <p class="resultsCaption01 radius2">旅行代金</p>
                                </th>
                                <td colspan="3" class="resultsPrice"><?=$str_price ?></td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <p class="resultsCaption01 radius2">コースNo.</p>
                                </th>
                                <td class="resultsTd01"><?=$newstr = substr_replace($value['tourcd'], '-', strlen($value['tourcd'])-1, 0); ?></td>
                                <th>
                                    <p class="resultsCaption01 radius2">行き先</p>
                                </th>
                                <td><?=$value['destination'] ?></td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <p class="resultsCaption01 radius2">旅行日数</p>
                                </th>
                                <td class="resultsTd01"><?=$str_period ?></td>
                                <th>
                                    <p class="resultsCaption01 radius2">食事</p>
                                </th>
                                <td><?=$value['meal'] ?></td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <p class="resultsCaption01 radius2">添乗員</p>
                                </th>
                                <td class="resultsTd01"><?=$str_tourcon ?></td>
                                <th>
                                    <p class="resultsCaption01 radius2">バスガイド</p>
                                </th>
                                <td><?=$str_busguid ?></td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <p class="resultsCaption01 radius2">出発場所</p>
                                </th>
                                <td colspan="3"><?=$platform ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php $url = $this->Url->build("/", true)."tour/main01/" ?>
                    <div class="resultsBtn01 clearfix"><a class="detail_link" state='0' 
                            href="<?= $this->Url->build('/', true).'tour/detail/'.$value['id'] ?>"
                           ><span class="radius2">ツアー詳細を見る</span></a></div>
                </div>
            </div>
        </li>
    <?php }

        }
     ?>

