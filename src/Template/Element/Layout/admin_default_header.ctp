<?php $baseUrl = Cake\Core\Configure::read("App.fullBaseUrl"); ?>
<!--ヘッダー　ここから-->
<div>
    <div class="headerWrap01">
        <div class="header01">
            <div class="headerIn">
                <div class="logo01">
                    <h1><a href="<?php echo $this->Url->build('/admin/top'); ?>" class="imghover"><img src="<?php echo $baseUrl; ?>/admin/images/kanri_logo01.png"
                        width="436"
                        height="46" alt="西武グリーンツアー" /></a></h1>
                </div>
                <div class="text01">
                    <p>コース管理</p>
                </div>
                <div class="logout01">
                    <p><a href="<?php echo $this->Url->build('/admin/logout', true); ?>" class="imghover">ログアウト</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--ヘッダー　ここまで-->