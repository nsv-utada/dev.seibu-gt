<?php
    $imageCount = 5;
    $display    = "";
?>
<?php for ($i = 1; $i <= $imageCount ; $i++): ?>
    <?php
        // type is pimage or mimage, which is column in database to store image
        $imageField = $type . $i;
    ?>
    <?php if ($item->$imageField): ?>
        <!-- show image if there is image -->
        <?php $imageUrl = $item->getImageUrl($imageField); ?>
        <div id="delete-image-box-<?php echo $imageField; ?>" class="kanriImgBox01 delete-image-box">
            <span class="kanriImg01" id='image-name-<?php echo $imageField; ?>'><img src="<?php echo $imageUrl; ?>" alt="イメージ"/></span>
            <input type="button" value="画像を削除" class="adminbutton02 imghover btn-delete-image" data-image-field="<?php echo $imageField; ?>" />
        </div>
        <div id="upload-image-box-<?php echo $imageField; ?>" class="kanriImgBox01 upload-image-box has-image" style="display:none">
            <span class="upload-btn-comment">※最大5枚</span>
            <br />
            <div class='input-image-box' id="input-url-box-<?php echo $imageField; ?>">
                <input class="input-url-image loginInput02" 
                    id="input-url-<?php echo $imageField; ?>"
                    name="input-url-<?php echo $imageField; ?>"
                    type="text" placeholder="http://" 
                    data-image-field="<?php echo $imageField; ?>" maxlength="255" />
                <div class="add-url-button-box">
                    <div class="imgBtn01 boxShadowBtn01 imghover add-url-button" data-image-field="<?php echo $imageField; ?>"><span>＋行を追加</span></div>
                </div>
            </div>
        </div>
        <input class="input-delete-image" id="input-delete-<?php echo $imageField; ?>" type="hidden" data-image-field="<?php echo $imageField; ?>" name="delete-images[]" />
    <?php else: ?>
        <div id="delete-image-box-<?php echo $imageField; ?>" class="kanriImgBox01 delete-image-box" style="display:none">
            <span class="kanriImg01" id='image-name-<?php echo $imageField; ?>'></span>
            <input type="button" value="画像を削除" class="adminbutton02 imghover btn-delete-image" data-image-field="<?php echo $imageField; ?>" />
        </div>
        <div id="upload-image-box-<?php echo $imageField; ?>" class="kanriImgBox01 upload-image-box no-image" style="<?php echo $display; ?>">
            <span class="upload-btn-comment">※最大5枚</span>
            <br />
            <div class='input-image-box' id="input-url-box-<?php echo $imageField; ?>">
                <input class="input-url-image loginInput02" 
                    id="input-url-<?php echo $imageField; ?>"
                    name="input-url-<?php echo $imageField; ?>"
                    type="text" placeholder="http://" 
                    data-image-field="<?php echo $imageField; ?>" maxlength="255" />
                <div class="add-url-button-box">
                    <div class="imgBtn01 boxShadowBtn01 imghover add-url-button" data-image-field="<?php echo $imageField; ?>"><span>＋行を追加</span></div>
                </div>
            </div>
        </div>
        <?php
            // will hide the next row if it doesn't have value
            $display = "display: none;"
        ?>
    <?php endif; ?>
<?php endfor; ?>