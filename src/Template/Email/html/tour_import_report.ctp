ご担当者様
<br />
<br />
平素より大変お世話になっております。
<br />
ご入稿いただきましたツアーデータのインポート処理が完了いたしました。
<br />
以下の実行ログをご査収のほどよろしくお願い申し上げます。
<br />
<br />
-------
<br />
<?php if (!$response['success']): ?>
    更新処理に失敗しました。
    <br />
    <strong><?php echo $response['error']; ?></strong>
<br />
<?php else: ?>
    <?php $messages = $response['messages']; ?>
    <?php foreach ($messages as $tourcd => $message): ?>
        <?php echo $tourcd ?>: <?php echo $message; ?>
        <br />
    <?php endforeach; ?>
    <br />
<?php endif; ?>
-------
