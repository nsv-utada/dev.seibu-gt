<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'home';

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>

<?php
    $urlPath = $this->request->getAttribute('here');
    $adminPath = '/app/admin/';
?>

<?php if (substr($urlPath, 0, strlen($adminPath)) == $adminPath): ?>
    <!-- different layout for admin path -->
    <?php $this->layout = 'admin_default'; ?>

    <div class="kanriWrap01">
        <div class="kanriContWrap01">
            <div style="text-align:center;font-size:24px;font-weight:bold;padding-top:30px;height:435px">
                <h1>404: Not Found</h1>
                <p>お探しのページが見つかりません。</p>
            </div>
        </div>
    </div>
<?php else: ?>
    <div style="text-align:center;font-size:24px;font-weight:bold;padding-top:30px;padding-bottom:300px">
        <h1>404: Not Found</h1>
        <p>お探しのページが見つかりません。</p>
    </div>
<?php endif; ?>
   <script type="text/javascript">
        $('.searchAreaWrap02').hide()
    </script>