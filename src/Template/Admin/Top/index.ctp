<?php
    echo $this->Html->script("admin/top.js?timestamp=20190711", ['block' => 'body-end']);
?>
<div class="kanriWrap01">
    <div class="statusSpWrap01">
        <div class="kanriContWrap01">
            <div class="kanridetailBlock01 clearfix statusTitWrap">
                <h2 class="kanriTit02">掲載状況</h2>
            </div>
            <div class="statusContWrap01">
                <div class="statusBox01">
                    <div class="statusBoxIn01">
                        <div class="boxIn01">
                            <p class="statusTit01">更新日時</p>
                            <p class="statusBtn01"><input type="button" id='btn-import' value="手動更新"
                                    class="boxShadowBtn01 imghover adminbutton04"></p>
                        </div>
                        <div class="boxIn02">
                            <p class="statusText01">
                                <span id='last-update-date'></span>
                                <br class="pcBr">
                                <span id='last-update-time' class="span02"></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="statusBox01">
                    <div class="statusBoxIn01">
                        <div class="boxIn01">
                            <p class="statusTit01">公開コース</p>
                        </div>
                        <div class="boxIn02">
                            <p class="statusText01"><a data-status="2" href="#" class="btn-big-status statusBtn03 imghover"><span
                                id="big-total-1" class="statusBtn03In"><?php echo $countItems[1]; ?></span></a></p>
                        </div>
                    </div>
                </div>
                <div class="statusBox02">
                    <div class="statusBoxIn02 magB8">
                        <div class="boxIn01">
                            <p class="statusTit01">非公開コース</p>
                        </div>
                        <div class="boxIn02">
                            <p class="statusText01"><a href="#" data-status="1" class="btn-big-status statusBtn03 imghover"><span
                                id="big-total-0" class="statusBtn03In"><?php echo $countItems[0]; ?></span></a></p>
                        </div>
                    </div>
                    <div class="statusBoxIn02">
                        <div class="boxIn01">
                            <p class="statusTit01">終了コース</p>
                        </div>
                        <div class="boxIn02">
                            <p class="statusText01"><a href="#" data-status="10" class="btn-big-status statusBtn03 imghover"><span
                                id="big-total-9" class="statusBtn03In"><?php echo $countItems[9]; ?></span></a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="kanriContWrap01">

            <div class="kanridetailBlock01 statusTitWrap statusTitBoxWrap01">
                <div class="statusTitBox01">
                    <h2 class="kanriTit02">コース一覧</h2>
                </div>
                <div class="statusTitBox02"><p class="tabNaviKeyword"><input id="input-search" type="text" name="keyword" 
                            placeholder="キーワード" class="searchInputKey" /><input id='btn-search' type="submit" class="searchBtn01 imghover" /></p></div></div>

            <div class="clearfix alc">
                <ul class="statusUl01">
                    <li><a class='admin-top-counter' data-status="0" href="#">すべて</a> <span id='count-total-total'></span>｜</li>
                    <li><a class='admin-top-counter' data-status="2" href="#">公開</a> <span id='count-total-1'></span>｜</li>
                    <li><a class='admin-top-counter' data-status="1" href="#">非公開</a> <span id='count-total-0'></span>｜</li>
                    <li><a class='admin-top-counter' data-status="10" href="#">終了</a> <span id='count-total-9'></span></li>
                </ul>
                <div class='admin-top-paging' id="admin-top-paging-1"></div>
            </div>

            <div class="statusTableWrap">
                <table class="statusTable02">
                    <colgroup>
                        <col width="80">
                        <col width="60">
                        <col width="1060">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td class="td02">
                                <!-- value is +1 from database -->
                                <select id='select-apply-status' class="adminSelect01">
                                    <option value="0" selected="selected">一括操作</option>
                                    <option value="2">公開</option>
                                    <option value="1">非表示</option>
                                    <option value="10">終了</option>
                                    <option value="delete">削除</option>
                                </select>
                            </td>
                            <td class="td02">
                                <p>
                                    <input id='btn-apply-status' type="button" value="実行"
                                        class="boxShadowBtn01 imghover adminbutton04 adminbutton0402" />
                                </p>
                            </td>
                            <td colspan="5" class="td03"></td>
                        </tr>
                    </tbody>
                </table>
                <div id='admin-top-items'>
                </div>
                
                <!-- hidden inputs for grid -->
                <input type="hidden" id="input-page-number" class='input-paging-control' />
                <input type="hidden" id="input-status" class='input-paging-control' />
                <input type="hidden" id="input-sort-field" class='input-paging-control' />
                <input type="hidden" id="input-sort-order" class='input-paging-control' />
            </div>

            <div class="clearfix alc admin-top-paging" id="admin-top-paging-2">
            </div>
        </div>
    </div>
</div>