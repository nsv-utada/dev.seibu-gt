<h2 class="kanriTit01">コース管理</h2>
<div class="kanriLoginBox01">
    <?php echo $this->Form->create(null); ?>
        <table class="kanriLoginTable">
            <tbody>
                <tr id='logout-message' style='display:none'>
                    <td>
                        <p style="text-align:center">ログアウトしました</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="email" placeholder="ログインID" class="loginInput01"
                            value="<?php echo(isset($email) ? $email : ''); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="password" name="password" placeholder="パスワード" class="loginInput01"
                            value="<?php echo(isset($password) ? $password : ''); ?>" />
                    </td>
                </tr>
                <?php if (isset($error)): ?>
                    <tr>
                        <td>
                            <div class='red'><?php echo $error; ?></div>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td class="td01"><input type="submit" value="ログイン" class="loginSubmit01Login"></td>
                </tr>
            </tbody>
        </table>
    <?php echo $this->Form->end(); ?>
</div>