<?php if ($response): ?>
    <script type="text/javascript">
        var response = <?php echo json_encode($response); ?>;
    </script>
<?php else: ?>
    <script type="text/javascript">
        var response = false;
    </script>
<?php endif; ?>
<script type="text/javascript">
    var platforms  = <?php echo json_encode($platforms); ?>;
    var today      = "<?php echo date('Y/m/d'); ?>";
    var config     = <?php echo json_encode($config); ?>;

    var tourPrices    = <?php echo json_encode($item->tour_prices); ?>;
    var tourPlatforms = <?php echo json_encode($item->tour_platforms); ?>;
    var tourSchedules = <?php echo json_encode($item->tour_schedules); ?>;

</script>
<?php
    echo $this->Html->script("admin/detail.js?timestamp=201906271534", ['block' => 'body-end']);
?>
<style type="text/css">
    .display-hide{
        display: none
    }
</style>
<div class="detailBreadWrap">
    <ul>
        <li><a href="<?php echo $this->Url->build('/admin/top', true); ?>">コース一覧</a>&nbsp;&#62;&nbsp;</li>
        <li>コース詳細・編集</li>
    </ul>
</div>
<div class="kanriWrap01">
    <div class="kanriContWrap01">
        <?php
            // start form
            echo $this->Form->create(
                $item,
                [
                    'enctype' => 'multipart/form-data',
                    'url'     => '/admin/detail/save',
                    'id'      => 'detail-form'
                ]
            );
        ?>
            <input type="hidden" id='input-id' value="<?php echo $item->id; ?>" name="id" />
            <input type="hidden" id='input-is-preview' value="" name="is-preview" />
            <div class="kanridetailBlock01 clearfix">
                <div class="kanriBox01">
                    <h2 class="kanriTit02">コース詳細・編集</h2>
                </div>
                <div class="kanriBox02Sp">
                    <div class="kanriBox02">
                        <p>
                            <?php if ($response && $response['success']): ?>
                                更新しました
                            <?php endif; ?>
                            &nbsp;
                        </p>
                    </div>
                    
                    <div class="kanriBox03">
                        <p>
                            <input type="submit" value="プレビュー" class="btn-preview loginSubmit02 radius4 imghover mr20" />
                            <input type="submit" value="更新" class="btn-submit loginSubmit01 radius4 imghover" />
                        </p>
                        <p class="kanriP01"><a href="#" class="btn-delete">コースを削除</a></p>
                    </div>
                </div>
            </div>
            <div class="kanriAcContWrap" id="main-section">
                <table class="kanriAdminTable01">
                    <tbody>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">自動更新</p></th>
                            <td class="labelWrap01">
                                <span class="red">★のある項目がシステムの情報で毎日上書きされることを</span><br />
                                <label><input class="select-autoup" id='select-autoup-2' data-link='select-autoup-end-2' type="radio" value="2" name="autoup" <?php echo($item->autoup ? 'checked="checked"' : ''); ?>>&nbsp;許可</label>
                                <label><input class="select-autoup" id='select-autoup-1' data-link='select-autoup-end-1' type="radio" value="1" name="autoup" <?php echo(!$item->autoup ? 'checked="checked"' : ''); ?>>&nbsp;不可</label>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">コースNo.</p></th>
                            <td>
                                <p><?php echo $item->tourcd; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">タイトル</p>
                                <br />
                                <span class="red font12">★自動更新対象項目</span>
                            </th>
                            <td>
                                <textarea name="tournm" class="loginInput03"><?php echo $item->tournm; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">状態</p></th>
                            <td class="labelWrap01">
                                <label><input type="radio" name="status" value="2" <?php echo($item->status == 1 ? 'checked="checked"' : ''); ?>>&nbsp;公開</label>
                                <label><input type="radio" name="status" value="1" <?php echo($item->status == 0 ? 'checked="checked"' : ''); ?>>&nbsp;非公開</label>
                                <label><input type="radio" name="status" value="10" <?php echo($item->status == 9 ? 'checked="checked"' : ''); ?>>&nbsp;終了</label>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">出発決定日</p></th>
                            <?php
                                $depdted = "";
                                if ($item->depdted) {
                                    //$depdted = $item->depdted->format('Y/m/d');
                                    $depdted = $item->depdted;
                                }
                            ?>
                            <td><input type="text" name="depdted" maxlength="255" value="<?php echo htmlspecialchars($depdted); ?>" class="loginInput02 force-input-date" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">食事</p></th>
                            <td><input type="text" name="meal" maxlength="255" value="<?php echo htmlspecialchars($item->meal); ?>" class="loginInput02" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">添乗員</p></th>
                            <td class="labelWrap01">
                                <label><input type="radio" name="tourcon" value="2" <?php echo($item->tourcon ? 'checked="checked"' : ''); ?>>&nbsp;同行</label>
                                <label><input type="radio" name="tourcon" value="1" <?php echo(!$item->tourcon ? 'checked="checked"' : ''); ?>>&nbsp;なし</label>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">バスガイド</p></th>
                            <td class="labelWrap01">
                                <label><input type="radio" name="busguid" value="2" <?php echo($item->busguid ? 'checked="checked"' : ''); ?>>&nbsp;同行</label>
                                <label><input type="radio" name="busguid" value="1" <?php echo(!$item->busguid ? 'checked="checked"' : ''); ?>>&nbsp;なし</label>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">メイン写真</p></th>
                            <td>
                                <?php
                                    echo $this->element('/admin/detail_images', ['item' => $item, 'type' => 'mimage']);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">ツアーのポイント</p></th>
                            <td><textarea name="naiyo" class="loginInput03" placeholder=""><?php echo $item->naiyo; ?></textarea></td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">ツアーのポイント写真</p></th>
                            <td>
                                <?php
                                    echo $this->element('/admin/detail_images', ['item' => $item, 'type' => 'pimage']);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">行程表</p>
                                <br /><span class="red font12">★自動更新対象項目</span>
                            </th>
                            <td>
                                <div class="kouteihyou" id="tour-schedules">                                    
                                </div>
                                <div class="kouteihyou">
                                    <p class="imgBtn01 boxShadowBtn01 imghover" id='btn-add-schedule'><span>＋行を追加</span></p>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">行き先</p>
                                <br /><span class="red font12">★自動更新対象項目</span>
                            </th>
                            <td><?php echo $item->destination; ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01 ">目的</p></th>
                            <td class="labelWrap02">
                                <?php
                                    $selecteds = [];
                                    foreach ($item->purposes as $selectedPurpose) {
                                        $selecteds[] = $selectedPurpose->purpose_id;
                                    }
                                    
                                ?>
                                <?php foreach ($purposes as $purpose): ?>
                                    <?php
                                        $checked = '';
                                        if (in_array($purpose->id, $selecteds)) {
                                            $checked = 'checked="checked"';
                                        }
                                    ?>
                                    <div class="span"><input id='checkbox-purpose-<?php echo $purpose->id; ?>' type="checkbox" <?php echo $checked; ?> name="purposes[]" value="<?php echo $purpose->id; ?>">
                                    <label for="checkbox-purpose-<?php echo $purpose->id; ?>"><?php echo $purpose->purpose_name; ?></label></div>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">設定期間<br/>旅行代金 <br /></p>
                                <br /><span class="red font12">★自動更新対象項目</span>
                            </th>
                            <td>
                                <?php
                                    foreach ($item->tour_prices as $tourPrice) {
                                        if ($tourPrice->depdt) {
                                            $tourPrice->depdt = $tourPrice->depdt->format('Y/m/d');
                                        } else {
                                            $tourPrice->depdt = "";
                                        }

                                        if ($tourPrice->price) {
                                            $tourPrice->price = number_format($tourPrice->price);
                                        } else {
                                            $tourPrice->price = "";
                                        }
                                    }
                                ?>
                                <div id='tour-prices'></div>
                                <!-- add new row button -->
                                <p id='btn-add-tour-price-row' data-row-number="<?php echo count($item->tour_prices) +1 ; ?>" 
                                    class="imgBtn01 boxShadowBtn01 imghover m-t10"><span>＋行を追加</span></p>
                            </td>
                        </tr>
                        
                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">出発場所</p>
                                <br /><span class="red font12">★自動更新対象項目</span>
                            </th>
                            <td>
                                <?php
                                    foreach ($item->tour_platforms as $tourPlatform) {
                                        if ($tourPlatform->depdt) {
                                            $tourPlatform->depdt = $tourPlatform->depdt->format('Y/m/d');
                                        } else {
                                            $tourPlatform->depdt = "";
                                        }
                                    }
                                ?>
                                <div id='tour-platforms'></div>
                                <p id='btn-add-platform-row' class="imgBtn01 boxShadowBtn01 imghover m-t10 display-hide"><span>＋行を追加</span></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <p class="kanriAdminTableTit01">旅行日数</p>
                                <br /><span class="red font12">★自動更新対象項目</span>
                            </th>
                            <?php
                                $priod = "";
                                if ($item->priod) {
                                    if ($item->priod == 1) {
                                        $priod = "日帰り";
                                    } elseif ($item->priod > 1) {
                                        $priod = ($item->priod - 1) . "泊" . $item->priod . "日";
                                    }
                                }
                            ?>
                            <td><?php echo $priod; ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><p class="kanriAdminTableTit01">自動更新</p></th>
                            <td class="labelWrap01">
                                <label><input name="autoup-end" class="select-autoup" id='select-autoup-end-2' data-link='select-autoup-2' type="radio" value="2" <?php echo($item->autoup ? 'checked="checked"' : ''); ?>>&nbsp;許可</label>
                                <label><input name="autoup-end" class="select-autoup" id='select-autoup-end-1' data-link='select-autoup-1' type="radio" value="1" <?php echo(!$item->autoup ? 'checked="checked"' : ''); ?>>&nbsp;不可</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="kanriAdminBox01">
                <p>
                    <input type="submit" value="プレビュー" class="btn-preview loginSubmit02 radius4 imghover mr20" />
                    <input type="submit" value="更新" class="btn-submit loginSubmit01 radius4 imghover" />
                </p>
                <p class="kanriP01"><a href="#" class="btn-delete">コースを削除</a></p>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>