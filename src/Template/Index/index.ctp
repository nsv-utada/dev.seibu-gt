<div class="mainVisualWrap pc">
    <ul class="slickMainvisual main_slick_pc">
        <li>
            <a href="tour/main01.php"><img src="../../images/top_main_1904_01.jpg"
                    alt="メロン食べ放題／イメージ" /></a>
            <p class="main-catch">あなたの夢、叶えます！！夏の王様<span
                    class="main-catch__sub-catch">メロンを食べつくす！</span></p>
        </li>
        <li>
            <a href="tour/special_cherry.php"><img src="../../images/top_main_1904_02.jpg"
                    alt="さくらんぼ狩り" /></a>
            <p class="main-catch">赤く甘い宝石☆彡一口の幸せ…<span
                    class="main-catch__sub-catch">さくらんぼ狩り</span></p>
        </li>


        <li>
            <a href="tour/main03.php"><img src="../../images/top_main_1904_03.jpg"
                    alt="ひたち海浜公園のネモフィラ／イメージ" /></a>
            <p class="main-catch">花言葉は「可憐」　一度は見たい！<span
                    class="main-catch__sub-catch">美しすぎる青の絶景</span></p>
        </li>
        <li>
            <a href="tour/main04.php"><img src="../../images/top_main_1904_04.jpg"
                    alt="富士山と桜／イメージ" /></a>
            <p class="main-catch">2019新しい時代の<span class="main-catch__sub-catch">新しい春に出逢う</span>
            </p>
        </li>
        <li>
            <a href="tour/main05.php"><img src="../../images/top_main_1904_05.jpg"
                    alt="露天風呂／イメージ" /></a>
            <p class="main-catch">心の渇きに旅が効く！<span
                    class="main-catch__sub-catch">絶景、グルメ、そして温泉</span></p>
        </li>

    </ul>
</div>

<div class="mainVisualWrap sp">
    <ul class="slickMainvisual main_slick_sp">

        <li>
            <a href="tour/main01.php"><img src="../../images/top_main_1904_01.jpg"
                    alt="メロン食べ放題／イメージ" /></a>
            <p class="main-catch">あなたの夢、叶えます！！夏の王様<span
                    class="main-catch__sub-catch">メロンを食べつくす！</span></p>
        </li>
        <li>
            <a href="tour/special_cherry.php"><img src="../../images/top_main_1904_02.jpg"
                    alt="さくらんぼ狩り" /></a>
            <p class="main-catch">赤く甘い宝石☆彡一口の幸せ…<span
                    class="main-catch__sub-catch">さくらんぼ狩り</span></p>
        </li>


        <li>
            <a href="tour/main03.php"><img src="../../images/top_main_1904_03.jpg"
                    alt="ひたち海浜公園のネモフィラ／イメージ" /></a>
            <p class="main-catch">花言葉は「可憐」　一度は見たい！<span
                    class="main-catch__sub-catch">美しすぎる青の絶景</span></p>
        </li>
        <li>
            <a href="tour/main04.php"><img src="../../images/top_main_1904_04.jpg"
                    alt="富士山と桜／イメージ" /></a>
            <p class="main-catch">2019新しい時代の<span class="main-catch__sub-catch">新しい春に出逢う</span>
            </p>
        </li>
        <li>
            <a href="tour/main05.php"><img src="../../images/top_main_1904_05.jpg"
                    alt="露天風呂／イメージ" /></a>
            <p class="main-catch">心の渇きに旅が効く！<span
                    class="main-catch__sub-catch">絶景、グルメ、そして温泉</span></p>
        </li>

    </ul>
</div>

<div class="topNewsWrap contIn960">
    <dl>
        <dt><a href="info">お知らせ</a></dt>
        <dd>
            <ul class="slickNews">

                <li><span>2019/03/22</span><a
                        href="info/2019%e5%b9%b44%e6%9c%881%e6%97%a5%e5%8f%97%e4%bb%98%e3%82%88%e3%82%8a%e3%82%b3%e3%83%b3%e3%83%93%e3%83%8b%e6%b1%ba%e6%b8%88%e3%82%b9%e3%82%bf%e3%83%bc%e3%83%88%ef%bc%81%e3%81%95%e3%82%89%e3%81%ab/">2019年4月1日受付よりコンビニ決済スタート！さらに便利になります</a>
                </li>


                <li><span>2019/03/22</span><a
                        href="info/web%e3%83%91%e3%83%b3%e3%83%95%e3%83%ac%e3%83%83%e3%83%88%e3%80%8c%e9%99%bd%e6%98%a5-%e3%83%90%e3%82%b9%e6%97%85%e3%80%8d%e3%80%8c%e3%83%ac%e3%82%b0%e3%83%ab%e3%82%b9-%e3%83%90%e3%82%b9%e6%97%85/">WEBパンフレット「陽春
                        バス旅」「レグルス バス旅」を公開いたしました</a></li>


                <li><span>2019/03/22</span><a
                        href="info/%e8%a5%bf%e6%ad%a6%e3%82%b0%e3%83%aa%e3%83%bc%e3%83%b3%e3%83%84%e3%82%a2%e3%83%bc%e3%81%ae%e3%83%9b%e3%83%bc%e3%83%a0%e3%83%9a%e3%83%bc%e3%82%b8%e3%81%8c%e3%83%aa%e3%83%8b%e3%83%a5%e3%83%bc%e3%82%a2/">西武グリーンツアーのホームページがリニューアルしました！</a>
                </li>

            </ul>
        </dd>
    </dl>
</div>

<div class="searchAreaWrap01">
    
     <?= $this->Form->create(null, ['url' => ['controller' => 'tour', 'action' => 'main01'], 'novalidate' => true, 'class' => 'icon-person', 'escape' => false,  'autocomplete'=> 'off', 'id' => 'ApForm1','method'=>'GET']); ?>
        <div class="searchArea01">
            <ul class="tabNav clearfix">
                <li class="tabNaviLeft">
                    <div class="tabNavi01">
                        <a href="#tab1" class="noSmoothScroll">日帰りツアー</a>
                    </div>
                </li>
                <li class="tabNaviRight">
                    <div class="tabNavi01">
                        <a href="#tab2" class="noSmoothScroll" onclick="getDepdtDate(2)">宿泊ツアー</a>
                    </div>
                </li>
            </ul>

            <div id="tab1">
                <div class="tabNaviCont01">
                    <div class="tabNaviContIn01">

                        <div class="tabNaviContCell01">
                            <label>
                                <div class="tabNaviBox01 daySp01">
                                    <p class="text01">出発日</p>
                                    <p class="text02">
                                        <input type="text" name="tab1_datedepdted"  placeholder="日付未定"
                                            value="" size="10" class="searchInput01"
                                            id="datepicker" >
                                        <span class="iconCal01"><img
                                                src="../../images/search_icon_cal.png" width="18"
                                                height="18" alt="カレンダー" class="pcBr"><img
                                                src="../../images/search_icon_cal_sp.png" alt="カレンダー"
                                                class="spBr"></span>
                                    </p>
                                </div>
                            </label>
                        </div>

                        <div class="tabNaviContCell01 accordion radioWrap02">
                                <div class="tabNaviBox01 switch" onclick="InitPurpose(1)">
                                    <p class="text01">目的</p>
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01 pulldown">
                                    <ul class="clearfix searchInputWrap01">
                                       
                                       
                                    </ul>
                                </div>
                            </div>

                        <div class="tabNaviContCell01 accordion radioWrap01">
                            <div class="tabNaviBox01 switch" onclick="InitRegion(1)">
                                <p class="text01">行き先</p>
                                <p class="text02">指定しない</p>
                            </div>
                            <div class="tabNaviAcCont01 pulldown">
                                <ul class="clearfix searchInputWrap01">
                                    
                                </ul>
                                
                                <div id="boxregion1" class="click">
                                    <div class="clickWrap clearfix">
                                        <div><a class="modal-close imghover detailModalClose"
                                            href="#"><img src="../../images/common/detail_modal_close.png" width="22" height="22" alt="close" /></a></div>
                                            <div>
                                                <p class="clickP01">東北</p>
                                                <ul class="modalRadioWrap modal-close">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <div class="tabNaviContCell01 accordion radioWrap03">
                            <div class="tabNaviBox01 switch" onclick="InitRoute(1)">
                                <p class="text01">出発場所</p>
                                <p class="text02">指定しない</p>
                            </div>
                            <div class="tabNaviAcCont01 pulldown">
                                <ul class="clearfix searchInputWrap01" id='boxRoute1'>

                                </ul>
                            </div>
                            <div id="boxPlatform1" class="click">
                                        <div class="clickWrap clearfix">
                                            <div><a class="modal-close imghover detailModalClose"
                                                    href="#"><img
                                                        src="../../images/common/detail_modal_close.png"
                                                        width="22" height="22"
                                                        alt="close" /></a></div>
                                            <div>
                                                <p class="clickP01">西武池袋線</p>
                                                <ul class="modalRadioWrap modal-close">
                                            
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                        </div>

                        <div class="tabNaviKeyword"><input type="text" id="tab1_keyword"
                                placeholder="キーワード　観光やホテル名など"></div>

                        <div class="accordion tabNaviCont02">
                            <div class="switch02 tabNaviSwitch01">
                               
                                <?php $baseUrl = Cake\Core\Configure::read("App.fullBaseUrl"); ?>
                                <p class="p01"><span class="tabNaviSpan01"><span
                                            class="plus"><img src="<?=$baseUrl ?>/../../images/Plus_1.svg"></span></span>条件を追加</p>
                                <p class="p02"><span class="tabNaviSpan01"><span
                                            class="plus"><img src="<?=$baseUrl ?>/../../images/Plus_2.svg"></span></span>閉じる</p>
                            </div>
                            <div class=" tabNaviAcCont02">
                                <ul class="clearfix">
                                    <li class="li01">
                                        <div style="display: inline-block; height: auto;vertical-align: bottom;"> 
                                            
                                            <input type="checkbox" id='show_price1' name='show_price1' />
                                            <label for="show_price1">予算</label>
                                        </div>
                                        <div class="yosanWrap01">
                                            <p id="amount"></p>
                                            <div id="slider-range"></div>                                        
                                        </div>
                                    </li>
                                    <li> <label><input type="checkbox" name="tab1_isDepdted" value="1">出発決定日あり</label> </li>
                                    <li> <label><input type="checkbox" name="tab1_isTourBus" value="1">添乗員・ガイド同行</label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="tabNaviContCell02">
                            <input type="button" value="検索" name='submit1' id='submit1' class="submitBtn01 imghover">
                        </div>
                    </div>
                </div>
            </div>
            <!--#tab1ここまで-->

            <div id="tab2">
                <div class="tabNaviCont01">
                    <div class="tabNaviContIn01">
                        <div class="tabNaviContCell01">
                            <label>
                                <div class="tabNaviBox01 daySp01">
                                    <p class="text01">出発日</p>
                                    <p class="text02">
                                        <input type="text" name="tab2_datedepdted"  placeholder="日付未定"
                                            value="" size="10" class="searchInput01"
                                            id="datepicker02" >
                                        <span class="iconCal01"><img src="../../images/search_icon_cal.png" width="18" height="18" alt="カレンダー" class="pcBr"><img
                                                src="../../images/search_icon_cal_sp.png" alt="カレンダー" class="spBr"></span>
                                    </p>
                                </div>
                            </label>
                        </div>

                        <div class="tabNaviContCell01 accordion radioWrap02">
                            <div class="tabNaviBox01 switch" onclick="InitPurpose(2)">
                                <p class="text01">目的</p>
                                <p class="text02">指定しない</p>
                            </div>
                            <div class="tabNaviAcCont01 pulldown">
                                <ul class="clearfix searchInputWrap01">
                                   
                                </ul>
                            </div>
                        </div>

                        <div class="tabNaviContCell01 accordion radioWrap01">
                            <div class="tabNaviBox01 switch" onclick="InitRegion(2)">
                                <p class="text01">行き先</p>
                                <p class="text02">指定しない</p>
                            </div>
                            <div class="tabNaviAcCont01 pulldown">
                                <ul class="clearfix searchInputWrap01">
                                    
                                </ul>
                                 
                                  <div id="boxregion2" class="click">
                                            <div class="clickWrap clearfix">
                                                <div><a class="modal-close imghover detailModalClose"
                                                        href="#"><img  src="../../images/common/detail_modal_close.png"  width="22" height="22" alt="close" /></a></div>
                                                <div>
                                                    <p class="clickP01">東北</p>
                                                    <ul class="modalRadioWrap modal-close">
                                                    
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>      
                            </div>
                        </div>

                        <div class="tabNaviContCell01 accordion radioWrap03">
                            <div class="tabNaviBox01 switch" onclick="InitRoute(2)">
                                <p class="text01">出発場所</p>
                                <p class="text02">指定しない</p>
                            </div>
                            <div class="tabNaviAcCont01 pulldown">
                                <ul class="clearfix searchInputWrap01" id='boxRoute2'>
                                    
                                </ul>
                                <div id="boxPlatform2" class="click">
                                    <div class="clickWrap clearfix">
                                        <div><a class="modal-close imghover detailModalClose"
                                                href="#"><img
                                                    src="../../images/common/detail_modal_close.png"
                                                    width="22" height="22"
                                                    alt="close" /></a></div>
                                        <div>
                                            <p class="clickP01">西武池袋線</p>
                                            <ul class="modalRadioWrap modal-close">
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="tabNaviKeyword"><input type="text" id="tab2_keyword"
                                placeholder="キーワード　観光やホテル名など"></div>
                        <div class="accordion tabNaviCont02">
                            <div class="switch tabNaviSwitch01">
                                <p class="p01"><span class="tabNaviSpan01"><span
                                            class="plus"><img src="<?=$baseUrl ?>/../../images/Plus_1.svg"></span></span>条件を追加</p>
                                <p class="p02"><span class="tabNaviSpan01"><span
                                            class="plus"><img src="<?=$baseUrl ?>/../../images/Plus_2.svg"></span></span>閉じる</p>
                            </div>
                            <div class="tabNaviAcCont02 tabNaviAcCont01">
                                <ul class="clearfix" id='clear_tab'>
                                    <li class="li01">
                                        <div style="display: inline-block; height: auto;vertical-align: bottom;">
                                            
                                            <input type="checkbox" id='show_price2' name='show_price2' />
                                            <label for="show_price2">予算</label>
                                           
                                            
                                        </div>
                                        <div class="yosanWrap01 yosanWrap001">
                                            <p id="amount02"></p>
                                            <div id="slider-range02"></div>                                        
                                        </div>
                                    </li>
                                    <li>
                                        <label><input type="checkbox" name="tab2_isDepdted"
                                                value="出発決定日あり">出発決定日あり</label>
                                    </li>
                                    <li>
                                        <label><input type="checkbox" name="tab2_isTourBus"
                                                value="添乗員・ガイド同行">添乗員・ガイド同行</label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="tabNaviContCell02">
                            <input type="button" value="検索" id='submit2' name='submit2' class="submitBtn01 imghover">
                        </div>

                    </div>
                </div>
            </div>
            <!--#tab2ここまで-->
        </div>
       <div style="display: none">
         
            <input type="hidden" name="tab_date_depdted" id="tab_date_depdted" value='' >
            <input type="hidden" name="tab_purpose" id="tab_purpose" value='' >
            <input type="hidden" name="tab_purpose_title" id="tab_purpose_title" value='' >
            <input type="hidden" name="tab_destination" id="tab_destination" value='' >
            <input type="hidden" name="tab_destination_title" id="tab_destination_title" value='' >
            <input type="hidden" name="tab_platform" id="tab_platform" value='' >
            <input type="hidden" name="tab_platform_title" id="tab_platform_title" value='' >
            <input type="hidden" name="tab_price_from"  id="tab_price_from" value='' >
            <input type="hidden" name="tab_price_to" id="tab_price_to" value='' >
            <input type="hidden" name="tab_depdted"  id="tab_depdted" value='' >
            <input type="hidden" name="tab_tourbus"  id="tab_tourbus" value='' >
            <input type="hidden" name="tab_search_key"  id="tab_search_key" value='' >
            <input type="hidden" name="submit_name" id="submit_name" value='' >

            <input type="hidden" name="tab_date_from" id="tab_date_from" value='' >
            <input type="hidden" name="tab_date_from_title" id="tab_date_from_title" value='' >
            <input type="hidden" name="tab_date_to" id="tab_date_to" value='' >
            <input type="hidden" name="tab_date_to_title" id="tab_date_to_title" value='' >
            <input type="hidden" name="tab_dates" id="tab_dates" value='' >

            <input type="hidden" name="tab_depdted_from" id="tab_depdted_from" value='' >
            <input type="hidden" name="tab_depdted_to" id="tab_depdted_to" value='' >
            <input type="hidden" name="tab_depdted_title" id="tab_depdted_title" value='' >
            <input type="hidden" name="action" id="action" value='index' >
            <input type="hidden" name="sort" id="sort" value='' >
       </div>
    </form>
</div>

<div class="topNaviWrap01 contIn960">
    <ul>
        <li>
            <a href="#" class="imghover">
                <span class="spanCell01"><img src="../../images/top_btn_icon01.png" alt="" /></span>
                <span class="spanCell02">　マイページ<br><span class="span01">（予約内容確認など）</span></span>
            </a>
        </li>
        <li>
            <a href="#" class="imghover">
                <span class="spanCell01"><img src="../../images/top_btn_icon02.png" alt="" /></span>
                <span class="spanCell02">集合場所案内</span>
            </a>
        </li>
        <li class="li01">
            <a href="#" class="imghover">
                <span class="spanCell01"><img src="../../images/top_btn_icon03.png" alt="" /></span>
                <span class="spanCell02">ご予約からご出発まで</span>
            </a>
        </li>
        <li>
            <a href="#" class="imghover">
                <span class="spanCell01"><img src="../../images/top_btn_icon04.png" alt="" /></span>
                <span class="spanCell02">メール会員登録</span>
            </a>
        </li>
    </ul>
</div>

<div class="contIn960">
    <h2 class="topH201">担当者おすすめツアー</h2>
</div>
<div class="tourContWrap">
    <div class="slickTourWrap">
        <ul class="slickTour">

            <li>
                <a href="tour/19005.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821989"
                                width="306" alt="石切山脈／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">【石切山脈】東西８㎞、南北６㎞にもわたる採掘場 白く美しい採掘現場の景観はまるで壮大な石の屏風です
                            </p>
                            <p class="text02">9,480<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="tour/19011.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821987"
                                width="306" alt="国営ひたち海浜公園のネモフィラ／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">一面を青一色に染め上げる 空と海と丘の青が溶け合うパノラマシーンは圧巻です！！</p>
                            <p class="text02">7,990<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="tour/19012.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821991"
                                width="306" alt="あしかがフラワーパークの藤／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">見渡す限りの芝桜 600畳敷きの大藤 100余種約10,000株のつつじ</p>
                            <p class="text02">7,980<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>


            <li>
                <a href="tour/19009.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821990"
                                width="306" alt="雪の大谷／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">高さ20mに迫るダイナミックな雪の壁を見る事ができるかも・・・！？</p>
                            <p class="text02">39,800<span>円</span>～42,800<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="tour/19017.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821988"
                                width="306" alt="小田原城／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">難攻不落の城とうたわれた国の史跡「小田原城」戦国時代末の山城、天守閣と石垣のない「山中城」を巡る
                            </p>
                            <p class="text02">8,480<span>円</span>～8,980<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="tour/19018.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821986"
                                width="306" alt="東京湾アクアライン／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">普段は一般の方の立ち入りができない場所・・・車の通らない大きなトンネル探検！！</p>
                            <p class="text02">9,490<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="tour/19015.php" class="imghover">
                    <div class="slickTourCont matchHeight">
                        <div class="tourImg01"><img
                                src="https://api.bud-group.com/img.php?material_id=821992"
                                width="306" alt="富士芝桜まつり／イメージ"></div>
                        <div class="tourCont01">
                            <p class="text01">首都圏最大級80万株の芝桜が富士山麓の広大な敷地に咲き誇ります</p>
                            <p class="text02">7,990<span>円</span></p>
                        </div>
                    </div>
                </a>
            </li>

        </ul>
    </div>
</div>

<div class="topContWrap">
    <div class="moreLink"><a href="osusume/" class="imghover radius4">まだまだあります！</a></div>
    <h2 class="topH201">人気ツアーランキング<span class="topH201Span01">2019/04/09 更新</span></h2>
    <div class="rankingBlock01">
        <ul class="rankingUl01 clearfix">

            <li class="matchHeight">
                <a href="tour/19020.php" class="imghover">
                    <div class="rankingImg01"><img
                            src="https://api.bud-group.com/img.php?material_id=828179"
                            width="228" alt="九頭龍神社月次祭／イメージ" /></div>
                    <div class="rankingLabel01"><img src="../../images/top_label01.png" width="36"
                            height="36" alt="1" /></div>
                    <div class="rankingBox01">
                        <p class="text01">縁結び」＆開運祈願！ 九頭龍神社本宮月次祭参列と箱根のパワースポットめぐり</p>
                        <p class="text02">8,990<span>円</span>〜9,990<span>円</span></p>
                    </div>
                </a>
            </li>
            <li class="matchHeight">
                <a href="tour/19011.php" class="imghover">
                    <div class="rankingImg01"><img
                            src="https://api.bud-group.com/img.php?material_id=821996"
                            width="228" alt="国営ひたち海浜公園のネモフィラ／イメージ" /></div>
                    <div class="rankingLabel01"><img src="../../images/top_label02.png" width="36"
                            height="36" alt="2" /></div>
                    <div class="rankingBox01">
                        <p class="text01">国営ひたち海浜公園の春の絶景 ネモフィラハーモニー</p>
                        <p class="text02">7,990<span>円</span></p>
                    </div>
                </a>
            </li>


            <li class="matchHeight">
                <a href="tour/19012.php" class="imghover">
                    <div class="rankingImg01"><img
                            src="https://api.bud-group.com/img.php?material_id=821995"
                            width="228" alt="館林市つつじが岡公園／イメージ" /></div>
                    <div class="rankingLabel01"><img src="../../images/top_label03.png" width="36"
                            height="36" alt="3" /></div>
                    <div class="rankingBox01">
                        <p class="text01">一足早いGW企画「北関東三大花物語」</p>
                        <p class="text02">7,980<span>円～</span>15,980<span>円</span></p>
                    </div>
                </a>
            </li>
            <li class="matchHeight">
                <a href="tour/19014.php" class="imghover">
                    <div class="rankingImg01"><img
                            src="https://api.bud-group.com/img.php?material_id=821998"
                            width="228" alt="靖国神社／イメージ" /></div>
                    <div class="rankingLabel01"><img src="../../images/top_label04.png" width="36"
                            height="36" alt="4" /></div>
                    <div class="rankingBox01">
                        <p class="text01">改元の年 今こそ訪れたい 皇居東御苑と御創立150年 靖國神社</p>
                        <p class="text02">66,680<span>円</span></p>
                    </div>
                </a>
            </li>

        </ul>
    </div>
</div>

<div class="topContWrap">
    <h2 class="topH201">特集・目的別バナー</h2>
    <div class="topBannerWrap">
        <ul class="topBanner01 clearfix">

            <li><a href="tour/special_cherry.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827472"
                        alt="さくらんぼ狩り" /></a></li>
            <li><a href="tour/special_fruits.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827360"
                        alt="季節のフルーツ狩り" /></a></li>
            <li><a href="tour/special_stay.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827361"
                        alt="バスでゆったり宿泊の旅" /></a></li>
            <li><a href="tour/special_regulus.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827373"
                        alt="くつろぎのレグルス" /></a></li>


            <li><a href="tour/special_regulus.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827373"
                        alt="くつろぎのレグルス" /></a></li>
            <li><a href="tour/special_gw.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=827453"
                        alt="日帰り！遠出も！GW特集" /></a></li>
            <li><a href="tour/special_buffet.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=818488"
                        alt="食べ放題×バイキング" /></a></li>
            <li><a href="tour/special_flower.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=818487"
                        alt="季節の花を見に行こう！" /></a></li>
            <li><a href="tour/special_chichibu.php" class="imghover"><img
                        src="https://api.bud-group.com/img.php?material_id=833596"
                        alt="秩父発着ツアー特集" /></a></li>

        </ul>
    </div>
    <div class="topBannerWrap">
        <ul class="topBanner02 clearfix">
            <li><a href="ebook" class="imghover"><img src="../../images/top_banner01.jpg"
                        alt="WEBパンフレット" /></a></li>
            <li><a href="#" class="imghover"><img src="../../images/top_banner02.jpg"
                        alt="スマイルシート" /></a></li>
        </ul>
    </div>
</div>

<!-- お問い合わせ -->
<div class="topContWrap">
    <div class="topContactWrap">
        <div class="topContactBlock">
            <ul class="topContactUl01">
                <li class="li01">
                    <p class="text01"><span class="topContactIcon01"><img
                                src="../../images/contact_icon_tel.png"
                                alt="tel" /></span>お電話でのお問い合わせ</p>
                    <p class="text02 pc">0570-01-8139</p>
                    <p class="text02 sp"><a href="tel:0570-01-8139">0570-01-8139</a></p>
                    <p class="text03">受付時間　10:00～16:00</p>
                    <p class="text04">※土・日・祝は休業</p>
                </li>
                <li>
                    <p class="text01"><span class="topContactIcon01"><img
                                src="../../images/contact_icon_mail.png"
                                alt="tel" /></span>メールでのお問い合わせ</p>
                    <p class="contactBtn01"><a href="form/form.php"
                            class="imghover radius4">お問い合わせ</a></p>
                </li>
            </ul>
            <ul class="topContactUl01 mgt30">
                <li>
                    <p class="text01">西武バス株式会社</p>
                    <p class="text03">〒359-1180 埼玉県所沢市久米546-1</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div>
    <div class="footerContWrap">
        <div class="clearfix footerContIn">
            <div class="footerSnsWrap01">
                <div class="fb-page" data-href="https://www.facebook.com/seibubus/" data-tabs="timeline"
                        data-width="280" data-height="340" data-small-header="false" data-adapt-container-width="true"
                        data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/seibubus/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/seibubus/">西武バス</a></blockquote>
                </div>
            </div>
            <div class="footerNaviWrap01">
                <div class="naviCont01 naviContFirst">
                    <p class="naviTit01"><a href="">ツアーを探す</a></p>
                    <ul class="naviUl01">
                        <!-- <li><a href="#">日帰りツアー</a></li> -->
                        <!-- <li><a href="#">宿泊ツアー</a></li> -->
                        <li><a href="osusume/">おすすめツアー</a></li>
                        <li><a href="ebook/">WEBパンフレット</a></li>
                    </ul>
                </div>
                <div class="naviCont01">
                    <p class="naviTit01">西武グリーンツアーについて</p>
                    <ul class="naviUl01 clearfix">
                        <li class="borderRight"><a href="about/">西武グリーンツアーとは</a></li>
                        <li class="borderRight"><a href="about/smileseat/">スマイルシート</a></li>
                        <li><a href="about/antisocial_forces.php">反社会的勢力に対する基本方針</a></li>
                        <!--<li class="sp">&nbsp;</li>-->
                    </ul>
                </div>
                <div class="naviCont01">
                    <p class="naviTit01">ご利用案内</p>
                    <ul class="naviUl01 clearfix">
                        <li class="borderRight"><a href="guide/">お申し込みからご出発まで</a></li>
                        <li><a href="guide/meeting">集合場所案内</a></li>
			<li><a href="/guide/pdf/checksheet_covid19.pdf" target="_blank"><span class="naviIcon01">健康管理チェックシート</span></a></li>
			<li><a href="/guide/pdf/douisyo.pdf" target="_blank"><span class="naviIcon01">旅行申込同意書</span></a></li>
                        <li class="borderRight"><a href="yakkan/">旅行条件書・旅行業約款</a></li>
                        <li><a href="https://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AM01_TOP.PR_CHECK?HV_SYSTEM_CD=MYPAGE&HV_USER_CODE=SEIBUBUS&HV_NEXT_JOB_NO=AM11"
                                target="_blank"><span
                                    class="naviIcon01">マイページ（予約内容確認など）</span></a></li>
                        <li class="borderRight"><a
                                href="http://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AH61_TOP.PR_CHECK?HV_USER_CODE=SEIBUBUS"
                                target="_blank"><span class="naviIcon01">メール会員登録</span></a></li>
                        <li><a href="form/form.php">ツアーのお問い合わせ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--フッターここから-->
<?php
    echo $this->Html->script("user/index/index.js", ['block' => 'body-end']);
?>
