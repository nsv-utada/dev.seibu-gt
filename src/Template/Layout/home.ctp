<?php $baseUrl = Cake\Core\Configure::read("App.fullBaseUrl"); ?>
<!doctype html>
<html lang="ja-jp">
<head>
    <meta charset="utf-8">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
    <meta name="description"
        content="<?=(isset($data['platform'])&& $data['platform'] !='')?$data['platform'].'発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' : '埼玉県（さいたま）発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' ?>">
    <meta name="keywords" content="西武,乗合,高速,観光,バス">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:title" content="西武グリーンツアー｜国内旅行・バスツアー｜西武バス">
    <meta property="og:description"
        content="<?=(isset($data['platform'])&& $data['platform'] !='')?$data['platform'].'発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' : '埼玉県（さいたま）発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' ?>">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="西武グリーンツアー｜国内旅行・バスツアー｜西武バス">
    <meta name="twitter:description"
        content="<?=(isset($data['platform'])&& $data['platform'] !='')?$data['platform'].'発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' : '埼玉県（さいたま）発の国内旅行・日帰りバスツアーは西武バスの西武グリーンツアーに！果物狩りや食べ放題、絶景、温泉、テーマパークなど話題のスポットへのツアーが充実。ゆったりくつろげるハイグレードバス「レグルス」も人気です。' ?>">
    <meta name="twitter:image:src" content="">
    <meta name="format-detection" content="telephone=no, email=no, address=no">
    <!--<meta name="viewport" content="width=device-width,initial-scale=1.0">-->
    <link rel="canonical" href="">
     <?php
        if (mb_strtolower($this->request->getParam('action')) !== 'detail') {?>
        <title><?=(isset($data['platform']) && $data['platform'] !='')?$data['platform'].'発のバスツアー' : '検索結果' ?><?=(isset($title))?$title: '' ?>｜西武グリーンツアー｜国内旅行・バスツアー</title>
    <?php }else{ ?>

        <title><?=(isset($title))?$title: '' ?>｜西武グリーンツアー｜国内旅行・バスツアー</title>
    <?php } ?>
    <script type="text/javascript">
        if (navigator.userAgent.indexOf('iPad') > 0) {
            //iPadの設定
            document.write(
                '<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/local_ipad.css"><link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/common/styles.min_ipad.css">'
                );
        } else {
            //それ以外（PC、スマホなど）の設定
            document.write(
                '<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/local.css"><link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/common/styles.min.css" media="screen and (min-width: 768px)"><link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/sp/common/styles.min.css" media="screen and (max-width: 767px)">'
                );
        }

    </script>
    <script>
        var rootUrl = '<?php echo $this->Url->build('/', true); ?>';
    </script>
    <meta name="format-detection" content="telephone=no">
    <!--<meta name="viewport" content="width=750">-->
    <!--<meta name="viewport" content="width=375, user-scalable=no, target-densitydpi=device-dpi">-->
    <!--<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width, minimal-ui">-->
    <script type="text/javascript">
        if (navigator.userAgent.indexOf('iPad') > 0) {
            //iPadの設定
            document.write('');
        } else {
            //それ以外（PC、スマホなど）の設定
            document.write(
            '<meta name="viewport" content="width=375, user-scalable=no, target-densitydpi=device-dpi">');
        }

    </script>

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/reset.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/jquery-ui.min.css">
    <!--<link rel="stylesheet" type="text/css" href="css/layout.css">-->
    <script type="text/javascript">
        if (navigator.userAgent.indexOf('iPad') > 0) {
            //iPadの設定
            document.write('<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/layout_ipad.css">');
        } else {
            //それ以外（PC、スマホなど）の設定
            document.write('<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/layout.css">');
        }

    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/modules.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/jquery.mmenu.all.css">
    <?= $this->Html->css('user/top/top.css').PHP_EOL ?>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/smoothscroll.js" charset="utf-8"></script>
    <script type="text/javascript">
        var atscroll = new ATScroll({
            noScroll: 'noSmoothScroll',
            setHash: false,
            duration: 1500,
            interval: 10,
            animation: 'quinticOut'
        });
        atscroll.load();

    </script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/main.js"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PMKXLK4');</script>
    <!-- End Google Tag Manager -->
</head>
<!--テンプレートの記述-->
 <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMKXLK4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section id="primary" class="content-area">
    <main id="main" class="site-main">

       
            <div id="top">
                <div id="wrap">
                    <?php
                       require_once('../../inc/header_pc.php');
                       require_once('../../inc/header_sp.php');
                    ?>
                    <div id="container">
                      
                        <?php
                         if (mb_strtolower($this->request->getParam('controller')) !== 'index' && mb_strtolower($this->request->getParam('action')) !== 'index' && mb_strtolower($this->request->getParam('action')) !== 'detail' && mb_strtolower($this->request->getParam('action')) !== 'detail2') {
                             echo $this->element('/Layout/home_subheader');
                         }?>
                        <?php echo $this->fetch('content'); ?>

                        <div class="sp_footer">
                            <nav id="breadcrumb">
                                <ol>
                                    <li><a href="<?php echo $baseUrl; ?>/"><span>HOME</span></a></li>
                                    <li><a href="<?php echo $baseUrl; ?>/"><em>貸切バス・バスツアー（西武グリーンツアー）</em></a></li>
                                </ol>
                                <!-- /#breadcrumb -->
                            </nav>
                            <aside id="subContents" class="js_accordion">
                                <div class="category"><button class="button">路線バス</button>
                                    <div class="inner">
                                        <a href="https://www.seibubus.co.jp/sp/rosen/" target="_blank">路線バスTOP</a>
                                        <ul class="subContents">
                                            <li><a href="https://transfer.navitime.biz/seibubus-dia/pc/map/Top"
                                                    target="_blank">時刻表</a></li>
                                            <li><a href="https://transfer.navitime.biz/seibubus-dia/pc/map/Top"
                                                    target="_blank">経路検索</a></li>
                                            <li><a href="https://transfer.navitime.biz/seibubus/pc/fare/Top"
                                                    target="_blank">運賃検索（距離・運賃証明書）</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/teiki/"
                                                    target="_blank">定期券・乗車券</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/rosenzu/"
                                                    target="_blank">路線図</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/r_unkou/"
                                                    target="_blank">運行状況</a></li>
                                            <li><a href="https://transfer.navitime.biz/seibubus-dia/pc/map/Top"
                                                    target="_blank">バス位置情報</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/busnavi-annai/"
                                                    target="_blank">経路検索・バス位置情報<br>検索の使い方</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/norikata/"
                                                    target="_blank">西武バスの乗り方</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/nightex/"
                                                    target="_blank">深夜急行バス</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/chichibu/"
                                                    target="_blank">秩父エリア</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/karuizawa/"
                                                    target="_blank">軽井沢・草津エリア</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/community/"
                                                    target="_blank">コミュニティバス</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/mitsumine/"
                                                    target="_blank">三峯神社線</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/dome/"
                                                    target="_blank">メットライフドーム線</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/rosen/outlet/"
                                                    target="_blank">三井アウトレットパーク線</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="category"><button class="button">高速バス</button>
                                    <div class="inner">
                                        <a href="https://www.seibubus.co.jp/sp/kousoku/" target="_blank">高速バスTOP</a>
                                        <ul class="subContents">
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/line/"
                                                    target="_blank">路線一覧</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/purchase/purchase.html"
                                                    target="_blank">乗車券購入方法</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/other/equipment.html"
                                                    target="_blank">車内設備</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/unkou/"
                                                    target="_blank">運行状況</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/other/ticket_ikebukuro.html"
                                                    target="_blank">池袋チケットセンター</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/other/"
                                                    target="_blank">その他のご案内</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="category"><button class="button">空港連絡バス</button>
                                    <div class="inner">
                                        <a href="https://www.seibubus.co.jp/sp/airport/" target="_blank">空港連絡バスTOP</a>
                                        <ul class="subContents">
                                            <li><a href="https://www.seibubus.co.jp/sp/airport/line/"
                                                    target="_blank">路線一覧</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/purchase/purchase.html"
                                                    target="_blank">乗車券購入方法</a></li>
                                            <li><a href="https://www.seibubus.co.jp/sp/kousoku/unkou/"
                                                    target="_blank">運行状況</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="category"><button class="button">貸切バス・バスツアー</button>
                                    <div class="inner">
                                        <a href="<?php echo $baseUrl; ?>/">貸切バス・バスツアーTOP</a>
                                        <ul class="subContents">
                                            <li><a href="<?php echo $baseUrl; ?>/">ツアーを探す</a></li>
                                            <li><a href="<?php echo $baseUrl; ?>/osusume/">おすすめツアー</a></li>
                                            <li><a href="<?php echo $baseUrl; ?>/guide/meeting/">集合場所案内</a></li>
                                            <li><a href="https://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AM01_TOP.PR_CHECK?HV_SYSTEM_CD=MYPAGE&HV_USER_CODE=SEIBUBUS&HV_NEXT_JOB_NO=AM11"
                                                    target="_blank">予約内容確認</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="category"><button class="button">採用情報</button>
                                    <div class="inner">
                                        <a href="https://www.seibubus.co.jp/sp/reserved/" target="_blank">採用情報TOP</a>
                                        <ul class="subContents">
                                            <li><a href="https://www.seibubus.co.jp/jump/jump_shinsotsu.html"
                                                    target="_blank">新卒採用</a></li>
                                            <li><a href="https://www.seibubus.co.jp/jump/jump_syakai.html"
                                                    target="_blank">社会人採用</a></li>
                                            <li><a href="https://www.seibubus.co.jp/jump/jump_arbeit.html"
                                                    target="_blank">アルバイト採用</a></li>
                                            <li><a href="https://www.seibubus.co.jp/jump/jump_group.html"
                                                    target="_blank">グループ会社採用</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="otherContents">
                                    <ul>
                                        <li><a href="https://www.seibubus.co.jp/sp/company/" target="_blank">企業情報</a>
                                        </li>
                                        <li><a href="https://www.seibubus.co.jp/sp/eigyou/" target="_blank">営業所一覧</a>
                                        </li>
                                        <li><a href="https://www.seibubus.co.jp/sp/angeroom/"
                                                target="_blank">エンジェの部屋</a></li>
                                        <li><a href="https://www.seibubus.co.jp/sp/contact/faq/"
                                                target="_blank">よくあるご質問</a></li>
                                        <li><a href="https://www.seibubus.co.jp/sp/contact/" target="_blank">お問い合わせ</a>
                                        </li>
                                        <!-- <li><a href="https://www.seibubus.co.jp/sp/whatsNew/" target="_blank">ダイヤ改正バックナンバー</a></li> -->
                                        <li><a href="https://www.seibubus.co.jp/sp/koukoku/"
                                                target="_blank">バス広告のご案内</a></li>
                                        <li><a href="https://www.seibubus.co.jp/sp/specific/"
                                                target="_blank">特定輸送のご案内</a></li>
                                        <li><a href="http://www.seibuhire.co.jp/" target="_blank">タクシー・ハイヤーのご用命</a></li>
                                        <li><a href="https://www.seibubus.co.jp/sp/news/" target="_blank">トピックス一覧</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /#subContents ==========-->
                            </aside>
                            <footer id="gfooter">
                                <div class="contentBottom">
                                    <p class="privacy"><a href="https://www.seibubus.co.jp/sp/company/csr/"
                                            target="_blank">CSRマネジメント</a></p>
                                    <p class="facebook"><a href="https://www.facebook.com/seibubus/"
                                            target="_blank"><span>西武バスfacebookページ</span></a></p>
                                    <p class="copyright"><small>Copyright &copy; Seibu Bus Co.,Ltd. all Right
                                            Reserved.</small></p>
                                </div>
                                <p class="group"><small>西武グループ</small></p>
                                <!-- /#gfooter ==========-->
                            </footer>
                        </div>
                    </div><!-- /#container ==========-->
                    
                    <?php
                       // echo $this->element('/Layout/home_footer');
                        require_once('../../inc/footer_pc.php');
                    ?>

                <!--/ id wrap-->
            </div>

            <div class="loading-spinner"><!-- Place at bottom of page --></div>
            <!--/ id top-->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.js"></script>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.matchHeight.js"></script>
            <script>
                $(function () {
                    $('.matchHeight').matchHeight();
                });

            </script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
            <?= $this->Html->script('datepicker-ja.js')?>            
            <script src="<?php echo $baseUrl; ?>/js/searchIFree.js"></script>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/yuga.js"></script>
            <?= $this->Html->script('user/common.js'); ?>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.mmenu.all.js"></script>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.bxslider.js"></script>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/top-slide.js"></script>
            <script src="<?php echo $baseUrl; ?>/scripts/common/jquery.cookie.min.js"></script>
            <!--<script src="scripts/common/common.min.js"></script>-->
            <script src="<?php echo $baseUrl; ?>/scripts/common/jquery.easing.1.3.min.js"></script>
            <script src="<?php echo $baseUrl; ?>/js/local.js"></script>
            <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.simplemodalwindow.js"></script>
            <?php echo $this->fetch('body-end'); ?>
       

    </main><!-- .site-main -->
</section>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- .content-area -->
 </body>
</html>
