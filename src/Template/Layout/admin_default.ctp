<?php $baseUrl = Cake\Core\Configure::read("App.fullBaseUrl"); ?>
<!doctype html>
<html lang="ja">

<head>

    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title><?php echo(isset($title) ? $title : "") ?>｜西武グリーンツアー管理画面｜国内旅行・バスツアー</title>

    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=1200">
    <!--<meta name="viewport" content="width=375, user-scalable=no, target-densitydpi=device-dpi">-->
    <!--<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width, minimal-ui">-->

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/admin/css/reset.css">

    <?php echo $this->Html->css('/css/jquery-ui.css?ver=4.9.17'); ?>
    
    <?php echo $this->Html->css('/css/layout.css'); ?>
    <?php echo $this->Html->css('/css/modules.css'); ?>

    <?php echo $this->Html->css('/css/custom.css'); ?>

    <?php echo $this->fetch('head-end'); ?>
    <script>
        var baseUrl   = '<?php echo $this->Url->build('/', true); ?>';
        var csrfToken = '<?php echo $this->request->getParam('_csrfToken') ?>';
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PMKXLK4');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMKXLK4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->



    <div id="top">
        <div id="wrap">
			<?php
                echo $this->element('/Layout/admin_default_header');
            ?>

			<?php echo $this->fetch('content'); ?>

			<?php
                echo $this->element('/Layout/admin_default_footer');
            ?>
        </div>
        <!--/ id wrap-->
    </div>
    <!--/ id top-->
    
    <?php echo $this->Html->script('/js/jquery-3.4.0.min.js'); ?>
    <?php echo $this->Html->script('/js/jquery-ui.js'); ?>
    <?php echo $this->Html->script('/js/jquery.ui.datepicker-ja.min.js'); ?>
    <?php echo $this->Html->script('/js/sweetalert.min.js'); ?>
    <?php echo $this->Html->script('/js/common_module.js'); ?>
    <?php echo $this->Html->script('/js/custom.js'); ?>

    <!-- this script is for admin after login only -->
    <?php echo $this->Html->script('/js/admin/admin.js?timestamp=20190711'); ?>
    <?php echo $this->fetch('body-end'); ?>
</body>

</html>
