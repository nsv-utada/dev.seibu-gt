<!doctype html>
<html lang="ja">
<head>

<meta charset="utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>

<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=1200">
<!--<meta name="viewport" content="width=375, user-scalable=no, target-densitydpi=device-dpi">-->
<!--<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width, minimal-ui">-->

<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="../../admin/css/reset.css">
<link rel="stylesheet" type="text/css" href="../../admin/css/layout.css">
<link rel="stylesheet" type="text/css" href="../../admin/css/modules.css">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PMKXLK4');</script>
<!-- End Google Tag Manager -->


</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMKXLK4"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="loginTop">
		<div id="wrap">
			<div class="kanriWrap01">
				<div class="kanriLoginWrap">
					<div class="kanriLoginBlock01">
						<h1 class="loginLogo01"><img src="../../admin/images/kanri_logo01.png" width="436" height="46" alt="西武グリーンツアー"/></h1>
						
						<?php echo $this->fetch('content'); ?>

					</div>
				</div>
			</div>
		</div><!--/ id wrap-->
	</div>
	<!--/ id top-->
	<?php echo $this->Html->script('/js/jquery-3.4.0.min.js'); ?>
	<?php echo $this->Html->script('/js/admin/login.js?timestamp=20190711'); ?>
</body>
</html>
