<ul class="statusPagerWrap">
    <!-- link to previous page, which is current page -1 unless current page is 1 -->
    <?php if ($paging["page"] == 1): ?>
        <!--
        <li class="disabled"><p class="statusPager" data-page-number="1" href="#1"> 最初 </p></li>
        -->
        <li class="disabled"><p class="statusPager" data-page-number="<?php echo $paging["page"]; ?>" href="#<?php echo $paging["page"]; ?>">&#60;</p></li>
    <?php else: ?>
        <!--
        <li><p class="statusPager page-link" data-page-number="1" href="#1">最初</p></li>
        -->
        <li><p class="statusPager page-link" data-page-number="<?php echo($paging["page"] - 1); ?>" href="#<?php echo($paging["page"] - 1); ?>">&#60;</p></li>
    <?php endif; ?>

    <!-- link to all pages from 1 to pageCount -->
    <?php for ($i = 1; $i <= $paging["pageCount"]; $i++): ?>
        <?php if ($i == $paging["page"]): ?>
            <li class="active"><p class="statusPager" data-page-number="<?php echo $i; ?>" href="#<?php echo $i; ?>"><?php echo $i; ?></p></li>
        <?php else: ?>
            <li><p class="statusPager page-link" data-page-number="<?php echo $i; ?>" href="#<?php echo $i; ?>"><?php echo $i; ?></p></li>
        <?php endif; ?>
    <?php endfor; ?>
    
    <!-- link to next page, which is current page + 1 unless current page is last page -->
    <?php if ($paging["page"] == $paging["pageCount"]): ?>
        <li class="disabled"><p class="statusPager" data-page-number="<?php echo $paging["page"]; ?>" href="#<?php echo $paging["page"]; ?>" rel="next">&#62;</p></li>
        <!--
        <li class="disabled"><p class="statusPager" data-page-number="<?php echo $paging['pageCount']; ?>" href="#<?php echo $paging['pageCount']; ?>">最後</p></li>
        -->
    <?php else: ?>
        <li><p class="statusPager page-link" data-page-number="<?php echo($paging["page"] + 1); ?>" href="#<?php echo($paging["page"] + 1); ?>" rel="next">&#62;</p></li>
        <!--
        <li><p class="statusPager page-link" data-page-number="<?php echo $paging['pageCount']; ?>" href="#<?php echo $paging['pageCount']; ?>">最後</p></li>
        -->
    <?php endif; ?>
</ul>