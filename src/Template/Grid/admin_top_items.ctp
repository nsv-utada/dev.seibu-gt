<table class="statusTable01">
    <colgroup>
        <col width="90">
        <col width="100">
        <col width="120">
        <col width="400">
        <col width="240">
        <col width="180">
        <col width="110">
    </colgroup>
    <tbody>
        <tr>
            <th>
                <p>
                    <input id="mass-item-select" type="checkbox" name="sakujyo" class="checkBox01 category" /> 選択
                </p>
            </th>
            <?php foreach ($cols as $col): ?>
                <th scope="col">
                    <?php
                        $sortClass = "";
                        if (isset($col['sortable']) && $col['sortable']) {
                            $sortClass = "sortable-col";
                        }
                    ?>
                    <div class="<?php echo $sortClass; ?>" data-sort-order="<?php echo $sortOrder; ?>" data-col="<?php echo $col['key']; ?>">
                        <?php echo $col["label"]; ?>
                        <?php if (isset($col['sortable']) && $col['sortable']): ?>
                            <?php if ($sortByField == $col['key']): ?>
                                <span class="f-r direction-char">
                                    <?php if ($sortOrder == "DESC"): ?>
                                        ▼
                                    <?php else: ?>
                                        ▲
                                    <?php endif; ?>
                                </span>
                            <?php else: ?>
                                <span class="f-r direction-img">
                                    <img src="<?php echo $this->Url->build("/img/up-down.svg", true); ?>" style="width:12px" alt="Sorttable" />
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </th>    
            <?php endforeach; ?>
        </tr>
        <?php foreach ($rows as $row): ?>
            <?php
                $status = $row['status'];
                $class  = "";
                if ($status == 1) {
                    $class = "koukai01";
                }
                if ($status == 9) {
                    $class = "syuryo01";
                }
                if (isset($statuses[$status])) {
                    $status = $statuses[$status];
                }

                $depdted = $row['depdted'];
                if ($depdted) {
                    //$depdted = $depdted->format('Y/m/d');
                }
            ?>
            <tr class="<?php echo $class; ?>">
                <td class="categories">
                    <p><input class="item-checkbox" data-item-id="<?php echo $row['id']; ?>" type="checkbox" name="sakujyo" class="checkBox01 category" /></p>
                </td>
                <td>
                    <p>
                        <a href="<?php echo $this->Url->build("/", true); ?>tour/detail/<?php echo $row['id']; ?>"
                            class="tour_link"><?php echo $status; ?>
                        </a>                        
                    </p>
                </td>
                <td>
                    <a class='admin-detail-link' href="<?php echo $this->Url->build('/admin/detail?tourId=' . $row['id']); ?>"><?php echo $row['tourcd']; ?></a>
                </td>
                <td class="td01">
                    <a class='admin-detail-link' href="<?php echo $this->Url->build('/admin/detail?tourId=' . $row['id']); ?>"><?php echo htmlspecialchars($row['tournm']); ?></a>
                </td>
                <td>
                    <?php if ($row['tour_prices'] && count($row['tour_prices'])): ?>
                        <?php $firstTourPrice = $row['tour_prices'][0]; ?>
                        <?php if (count($row['tour_prices']) == 1): ?>
                            <?=$firstTourPrice->depdt !='' ? $firstTourPrice->depdt->format('Y/m/d') :''; ?>
                        <?php else: ?>
                            <?php $lastTourPirce = end($row['tour_prices']); ?>
                            <?=$firstTourPrice->depdt != '' ? $firstTourPrice->depdt->format('Y/m/d') : ''; ?>
                            ~
                            <?=$lastTourPirce->depdt != '' ?  $lastTourPirce->depdt->format('Y/m/d') : ''; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td class="">
                    <?php echo $depdted; ?>
                </td>
                <td>
                    <?php if ($row['autoup']): ?>
                        <span class="colPink01">&#9679;</span>許可
                    <?php else: ?>
                        &#10006;不可
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (!count($rows)): ?>
            <tr>
                <td colspan="<?php echo count($cols) + 1 ?>">
                    データが見つかりません。
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>