<style type="text/css">
.detailBlock01 .ul01 li {
    margin-top: 10px
}
.pre_tag{
    white-space: pre-wrap;       /* Since CSS 2.1 */
    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    word-wrap: break-word; 
}
.scdl_value pre{
    font-family: 'Noto Sans JP', sans-serif;
    font-size: 13px;
}
.scdl_value_even2 , .scdl_value_even2 pre{
    height: 18px;
    letter-spacing: 0.5pt;
}
.scdl_value_even3,.scdl_value_even3 pre  {
    height: 18px;
    letter-spacing: 1.1pt
}
</style>
<?php
    use Cake\Routing\Router;
    $price = [];
    $dates= [];
    foreach ($item['tour_prices'] as  $v) {
       $price[] = $v['price'];
       if($v['price'] !='')$dates[] = date('Y/m/d', strtotime($v['depdt']));
    }
    $str_depdt = '';
    if($dates){
        if(max($dates) == min($dates)){
            $str_depdt = max($dates);
        }else{
            $str_depdt = min($dates).'～'.max($dates) ;
        }
    }
    $str_price = '';
    if($price){
        if(max($price) == min($price)){
            $str_price = number_format(max($price)).'円';
        }else{
            $str_price = number_format(min($price)).'円 ~'.number_format(max($price)).'円' ;
        }
    }
    
    $url1 =checkImageExist($item['mimage1'],$item['id'],$this->Url->build('/', true));
    $url2 =checkImageExist($item['mimage2'],$item['id'],$this->Url->build('/', true));
    $url3 =checkImageExist($item['mimage3'],$item['id'],$this->Url->build('/', true));
    $url4 =checkImageExist($item['mimage4'],$item['id'],$this->Url->build('/', true));
    $url5 =checkImageExist($item['mimage5'],$item['id'],$this->Url->build('/', true));

    $url_p1 =checkImageExist($item['pimage1'],$item['id'],$this->Url->build('/', true));
    $url_p2 =checkImageExist($item['pimage2'],$item['id'],$this->Url->build('/', true));
    $url_p3 =checkImageExist($item['pimage3'],$item['id'],$this->Url->build('/', true));
    $url_p4 =checkImageExist($item['pimage4'],$item['id'],$this->Url->build('/', true));
    $url_p5 =checkImageExist($item['pimage5'],$item['id'],$this->Url->build('/', true));
    $str_period = '';
    $text_link = '001';
    if((int)$item['priod'] == 1){
        $str_period ='日帰り';

    }else if((int)$item['priod'] > 1){
        $str_period =((int)$item['priod'] - 1).'泊'.(int)$item['priod'].'日';
        $text_link = '002';
    }

    $str_tourcon = '';
    if((int)$item['tourcon'] === 0){
        $str_tourcon = 'なし';
    }else if($item['tourcon'] ==1){
        $str_tourcon = '同行';
    }

    $str_busguid = '';
    if((int)$item['busguid'] === 0){
        $str_busguid = 'なし';
    }else if($item['busguid'] == 1){
        $str_busguid = '同行';
    }
    $TourPlatform = [];
    foreach ($item['tour_platforms'] as  $v) {
        if(!in_array($v['platform'], $TourPlatform)) $TourPlatform[] = $v['platform'];
    }
    $platform =($TourPlatform)?  implode("、",$TourPlatform):'';
    $courid =  str_replace('-', '',  $item['tourcd']);
    $link="https://hplink.we-can.co.jp/hplink/PR_FRAME?HV_USER_CODE=SEIBUBUS&HV_TOUR_CD=".$courid."&HV_S_DEP_FROM=&HV_S_DEP_TO=&HV_PTN_EDA=".$text_link."&HV_PTN_NO=";
    
    $tour_schedules_gyono = [];
     foreach ($item['tour_schedules'] as $v) {
  
        $tour_schedules_gyono[$v['nichiji']][] = $v['gyono'];
        
    }
    $tour_schedules = [];
    $old_naiyo2 ='';
    foreach ($item['tour_schedules'] as $v) {

        $v['naiyo2'] = filtSpaceBySpace($v['naiyo2'] );
        
        $url_img = $this->Url->build('/', true).'webroot/img/img_gaiji_l_sen003.gif';
        $img = "<span style='display: inline-block;'><img src=".$url_img." border='0' style='width: 52px;height: 5px;vertical-align: middle;zoom:85%;'></span>";
        $old_naiyo2 = $v['naiyo2'];
        if(strpos($old_naiyo2. $v['naiyo2'], '  ') !== false){
           $v['naiyo2'] =   ltrim( $v['naiyo2'], ' ');
        }
        if($v['gyono'] == max($tour_schedules_gyono[$v['nichiji']])){
            $v['naiyo2'] = str_replace(' ',$img.'&nbsp;', rtrim($v['naiyo2']));
        }else{
            $v['naiyo2'] = str_replace(' ',$img.'&nbsp;', ($v['naiyo2']));
        }
        
        
        
        

        $tour_schedules[$v['nichiji']][$v['gyono']] = ['naiyo1' =>  $v['naiyo1'],'naiyo2' =>  $v['naiyo2'],'naiyo3' =>  $v['naiyo3']];
        
    }
    function filtSpaceBySpace($naiyo2){
        $string = $naiyo2;
        for ($i=20; $i >=0 ; $i--) { 
            $new_space = '';
            for ($j=0; $j < $i ; $j++) { 
                $new_space .=' ';
            }            
            $string = str_replace($new_space,' ', $string);
        }
        return $string;
    }
    function checkImageExist($url,$id,$path){
       if(@file_get_contents($url,0,NULL,0,1)){
            return $url;
        }
        $url_new = WWW_ROOT .'tours'.DS.'images'.DS. $id.DS.$url;
        if(file_exists($url_new) && $url !='') return  $path.'webroot/tours/images/'. $id.'/'.$url;
        return '#';
    }
     $baseUrl = Cake\Core\Configure::read("App.fullBaseUrl"); 
?>
<nav id="breadcrumb" class="pc_breadcrumb">
<ol>
<li class="odd"><a  href="<?php echo $baseUrl; ?>"><span>西武グリーンツアー（貸切バス・バスツアー）</span></a></li>
<li class="even"><em><?= $item['tournm'] ?></em></li>
<!-- /.breadcrumb --></ol>
</nav>
<div class="detailContWrap01">

    <div class="pc">
        <div class="detailBox02">
            <table class="detailTable02">
                <tbody>
                    <tr>
                        <th scope="row">
                            <p class="resultsCaption01 radius2">コースNo.</p>
                        </th>
                        <td class="resultsTd01"><?=$newstr = substr_replace($item['tourcd'], '-', strlen($item['tourcd'])-1, 0); ?></td>
                        <th>
                            <p class="resultsCaption01 radius2">行き先</p>
                        </th>
                        <td><?=$item['destination'] ?></td>
                        <th>
                            <p class="resultsCaption01 radius2">設定期間</p>
                        </th>
                        <td><?=$str_depdt ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="sp">
        <div class="detailBox02">
            <table class="detailTable02">
                <tbody>
                    <tr>
                        <th scope="row">
                            <p class="resultsCaption01 radius2">コースNo.</p>
                        </th>
                        <td class="resultsTd01"><?=$newstr = substr_replace($item['tourcd'], '-', strlen($item['tourcd'])-1, 0); ?></td>
                        <th>
                            <p class="resultsCaption01 radius2">行き先</p>
                        </th>
                        <td><?=$item['destination'] ?></td>
                    </tr>
                    <tr class="period">
                        <th>
                            <p class="resultsCaption01 radius2">設定期間</p>
                        </th>
                        <td colspan="3"><?=$str_depdt?> </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="detailBlock01">
        <H2  class="detailH201 pre_tag"><?= $item['tournm'] ?></H2>
    </div>


    <div class="detailSlickWrap">
        <div class="detailSlickWrapIn">
            <ul class="slickDtail">
                <?php if($url1 && $url1!='#' ){ ?> <li><img src="<?=$url1 ?>" alt="イメージ" /></li> <?php } ?>
                <?php if($url2 && $url2!='#' ){ ?> <li><img src="<?=$url2 ?>" alt="イメージ" /></li> <?php } ?>
                <?php if($url3 && $url3!='#' ){ ?> <li><img src="<?=$url3 ?>" alt="イメージ" /></li> <?php } ?>
                <?php if($url4 && $url4!='#' ){ ?> <li><img src="<?=$url4 ?>" alt="イメージ" /></li> <?php } ?>
                <?php if($url5 && $url5!='#' ){ ?> <li><img src="<?=$url5 ?>" alt="イメージ" /></li> <?php } ?>
            </ul>
        </div>
    </div>


    <div class="detailBlock01">
        <div class="detailBoxWrap01">
            <div class="detailBox01">
                <div class="left01">
                    <p class="resultsCaption01 radius2">旅行代金</p>
                </div>
                <div class="right01">
                    <p class="resultsPrice"><span class="span01"><?=$str_price ?></span>
                    </p>
                </div>
            </div>

            <div class="detailBox01">
                <div class="left01">
                    <p class="resultsCaption01 radius2">出発地</p>
                </div>
                <div class="right01"><?=$platform ?></div>
            </div>

            <div class="clearfix">
                <div class="detailBox01 floatLeft01">
                    <div class="left01">
                        <p class="resultsCaption01 radius2">旅行日数</p>
                    </div>
                    <div class="right01"><?=$str_period ?></div>
                </div>
                <div class="detailBox01 floatLeft02">
                    <div class="left01">
                        <p class="resultsCaption01 radius2">食事</p>
                    </div>
                    <div class="right01"><?=$item['meal'] ?></div>
                </div>
                <div class="detailBox01 floatLeft02">
                    <div class="left01">
                        <p class="resultsCaption01 radius2">添乗員</p>
                    </div>
                    <div class="right01"><?=$str_tourcon ?></div>
                </div>
                <div class="detailBox01 floatLeft02">
                    <div class="left01">
                        <p class="resultsCaption01 radius2">バスガイド</p>
                    </div>
                    <div class="right01"><?=$str_busguid ?></div>
                </div>
            </div>
            <?php if($item['depdted']!=''): ?>
            <div class="detailBox01">
                <div class="left01">
                    <p class="resultsCaption01 radius2">出発決定日</p>
                </div>
                <div class="right01"><?=$item['depdted']; ?></div>
            </div>
            <?php endif;?>
        </div>
        <div class="detailBtn01 clearfix"><a href="<?=$link ?>" target="_blank"><span class="radius2">空席確認・予約</span></a></div>
    </div>
    <?php if($item['naiyo']!=''): ?>
    <div class="detailBlock01">
        <h3 class="detailH301">ツアーのポイント</h3>
        <?=nl2br($item['naiyo'] ) ?>
        <ul class="ul01 clearfix">
            <?php if($url_p1 && $url_p1!='#' ){ ?><li><img src="<?=$url_p1 ?>" alt="イメージ" /></li> <?php } ?>
            <?php if($url_p2 &&$url_p2!='#' ){ ?><li><img  src="<?=$url_p2 ?>" alt="イメージ" /></li> <?php } ?>
            <?php if($url_p3&&$url_p3!='#'){ ?><li><img  src="<?=$url_p3 ?>" alt="イメージ" /></li> <?php } ?>
            <?php if($url_p4&&$url_p4!='#'){ ?><li><img  src="<?=$url_p4 ?>" alt="イメージ" /></li> <?php } ?>
            <?php if($url_p5&&$url_p5!='#'){ ?><li ><img  src="<?=$url_p5 ?>" alt="イメージ" /></li> <?php } ?>
        </ul>
    </div>
    <?php endif;  ?>
    <div class="detailBlock01"><h3 class="detailBtn01 detailH301">行程表</h3></div>
    <div class="schedule">
        <div class="detailBlock01 " style="overflow-x: auto">
            
                        <?php if($tour_schedules){ ?>
                            <TABLE border="0" cellspacing="0" width="100%">
                                <TR>
                                    <TD>

                                        <TABLE border="0" cellspacing="0" cellpadding="2" width="100%">
                                            <?php foreach ($tour_schedules as $key => $value) { 

                                             ?>
                                            <TR>
                                                <TD align="center" class="scdl_value_odd" nowrap valign="top"><?=$key ?>日目</TD>
                                                <TD align="center" class="scdl_value_odd" nowrap valign="top"><br></TD>
                                                <TD align="center" class="scdl_value_odd" valign="top" width="600"><br></TD>
                                            </TR>
                                            
                                                <?php foreach ($value as $k1=> $v1) {  ?>
                                                  
                                               
                                               <TR>
                                                <TD class="scdl_value" colspan="3" nowrap> 
                                                    <TABLE align="center" cellspacing="0" cellpadding="1" width="100%">
                                                        <tr>
                                                            <td nowrap>
                                                                <TABLE align="center" cellspacing="0" cellpadding="2" width="100%">
                                                                    <TR> 
                                                                    <td align="left" class="scdl_value_even2" nowrap style=" height: 15px;">
                                                                      <pre>  <?=$v1['naiyo1'] ?></pre>
                                                                    </td>              
                                                                        
                                                                       
                                                                    </TR>
                                                                    <TR>
                                                                         <td align="left" class="scdl_value_even2" nowrap style=" height: 15px;">
                                                                       <?=$v1['naiyo2'] ?>
                                                                    </td>  
                                                                         
                                                                            
                                                                    </TR>
                                                                    <TR>
                                                                         <td align="left" class="scdl_value_even3" nowrap style=" height: 15px;">
                                                                           <pre> <?=$v1['naiyo3'] ?></pre>
                                                                    </td>  
                                                                         
                                                                    </TR>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </TABLE>
                                                </TD>
                                                </TR>
                                            <?php  } ?>
                                            
                                            
                                            <TR>
                                                <TD align="center" class="scdl_value" colspan="3" nowrap valign="top"><hr color="#ff66ff"size="1" noshade></TD>
                                            </TR>
                                            
                                            <?php } ?>
                                        </TABLE>
                                    </TD>
                                </TR>
                            </TABLE>
                        <?php } ?>
        </div>
        
    </div>
    <div class="detailBtn01 clearfix"><a href="<?=$link ?>" target="_blank"><span class="radius2">空席確認・予約</span></a></div>
</div>

<?php
use Cake\ORM\TableRegistry;
 if(isset($list_tour) && count($list_tour) !=0){ ?>
        <div class="contIn960 detailOsusumeWrap">
            <h2 class="topH201">おすすめツアー</h2>
            <div class="rankingBlock01">
                <ul class="rankingUl01 clearfix">
                    <?php
                    $tour = [];
                    foreach ($list_tour as $value) {
                        $row = [];
                        $price = [];
                        foreach ($value['tour_prices'] as  $v) {
                         $price[] = $v['price'];
                     }
                     $str_price = '';
                     $row['price'] = (min($price));
                     $str_price =$value['price'].'円'  ;
                     $row['url'] =checkImageExist($value['mimage1'],$value['id'],$this->Url->build('/', true));
                     $row['tournm'] = $value['tournm'];
                     $row['id'] = $value['id'];
                     $tour[] = $row;

                 }
                 $sortArray = array();
                 foreach($tour as $person){
                    foreach($person as $key=>$value){
                        if(!isset($sortArray[$key])){
                            $sortArray[$key] = array();
                        }
                        $sortArray[$key][] = $value;
                    }
                }             
                
                $orderby = "price";
                array_multisort($sortArray[$orderby],SORT_ASC,$tour);
                foreach ($tour as $value) {

                ?>
                <li class="matchHeight">
                    <a href="<?= $this->Url->build('/', true).'tour/detail/'.$value['id'] ?>" class="imghover">
                       <?php if($value['url'] !='#'){ ?> <div class="rankingImg01" > <?php if($value['url'] !='#'){ ?><img  src="<?=$value['url'] ?>"/><?php } ?></div>
                       <?php }else{?>
                         <div class="rankingImg01" style="height: 199px" ></div>
                      <?php  } ?>
                        <div class="rankingBox01">
                            <p class="text01"><?=$value['tournm'] ?></p>
                            <p class="text02"><?=number_format($value['price']) ?><span>円</span> </p>
                        </div>
                    </a>
                </li>





            <?php } ?>
        </ul>
    </div>
</div>
<?php } ?> 

</div>
<!-- お問い合わせ -->
<div class="topContWrap">
    <div class="topContactWrap">
        <div class="topContactBlock">
            <ul class="topContactUl01">
                <li class="li01">
                    <p class="text01"><span class="topContactIcon01"><img src="/images/contact_icon_tel.png"
                        alt="tel" /></span>お電話でのお問い合わせ</p>
                        <p class="text02 pc">0570-025-258</p>
                        <p class="text02 sp"><a href="tel:0570-025-258">0570-025-258</a></p>
                        <p class="text03">営業時間：平日 10:00～17:30</p>
                        <p class="text04">※土日祝は休業</p>
                    </li>
                    <li>
                        <p class="text01"><span class="topContactIcon01"><img src="/images/contact_icon_mail.png"
                            alt="tel" /></span>メールでのお問い合わせ</p>
                            <p class="contactBtn01"><a href="/form/form.php" class="imghover radius4">お問い合わせ</a></p>
                        </li>
                    </ul>
                    <ul class="topContactUl01 mgt30">
                        <li>
                            <p class="text01">西武バス株式会社</p>
                            <p class="text03">〒359-1180 埼玉県所沢市久米546-1</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--<div class="anc"><div id="anc01" class="ancIn"></div></div>-->
        <div>
            <div class="footerContWrap">
                <div class="clearfix footerContIn">
                    <div class="footerSnsWrap01">
                        <div class="fb-page" data-href="https://www.facebook.com/seibubus/" data-tabs="timeline"
                        data-width="280" data-height="340" data-small-header="false" data-adapt-container-width="true"
                        data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/seibubus/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/seibubus/">西武バス</a></blockquote>
                        </div>
                    </div>
                    <div class="footerNaviWrap01">
                        <div class="naviCont01 naviContFirst">
                            <p class="naviTit01"><a href="/">ツアーを探す</a></p>
                            <ul class="naviUl01">
                                <!-- <li><a href="#">日帰りツアー</a></li> -->
                                <!-- <li><a href="#">宿泊ツアー</a></li> -->
                                <li><a href="/osusume/">おすすめツアー</a></li>
                                <li><a href="/ebook/">WEBパンフレット</a></li>
                            </ul>
                        </div>
                        <div class="naviCont01">
                            <p class="naviTit01">西武グリーンツアーについて</p>
                            <ul class="naviUl01 clearfix">
                                <li class="borderRight"><a href="/about/">西武グリーンツアーとは</a></li>
                                <li class="borderRight"><a href="/about/smileseat/">スマイルシート</a></li>
                                <li><a href="/about/antisocial_forces.php">反社会的勢力に対する基本方針</a></li>
                                <!--<li class="sp">&nbsp;</li>-->
                            </ul>
                        </div>
                        <div class="naviCont01">
                            <p class="naviTit01">ご利用案内</p>
                            <ul class="naviUl01 clearfix">
                                <li class="borderRight"><a href="/guide/">お申し込みからご出発まで</a></li>
                                <li><a href="/guide/meeting">集合場所案内</a></li>
				<li><a href="/guide/pdf/checksheet_covid19.pdf" target="_blank"><span class="naviIcon01">健康管理チェックシート</span></a></li>
				<li><a href="/guide/pdf/douisyo.pdf" target="_blank"><span class="naviIcon01">旅行申込同意書</span></a></li>
                                <li class="borderRight"><a href="/yakkan/">旅行条件書・旅行業約款</a></li>
                                <li><a href="https://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AM01_TOP.PR_CHECK?HV_SYSTEM_CD=MYPAGE&HV_USER_CODE=SEIBUBUS&HV_NEXT_JOB_NO=AM11"
                                    target="_blank"><span class="naviIcon01">マイページ（予約内容確認など）</span></a></li>
                                    <li class="borderRight"><a
                                        href="http://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AH61_TOP.PR_CHECK?HV_USER_CODE=SEIBUBUS"
                                        target="_blank"><span class="naviIcon01">メール会員登録</span></a></li>
                                        <li><a href="/form/form.php">ツアーのお問い合わせ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ((isset($data['submit_name'])) || !isset($data['submit_name'])) {      ?>
                <input type="hidden" name="tab_date_depdted" id="tab_date_depdted" value='<?=(isset($data['tab_date_depdted']))?$data['tab_date_depdted'] : '' ?>' >
                <input type="hidden" name="tab_purpose"  id="tab_purpose" value='<?=(isset($data['purpose']))?$data['purpose'] : '' ?>' >
                <input type="hidden" name="tab_destination"  id="tab_destination" value='<?=(isset($data['destination']))?$data['destination'] : '' ?>' >
                <input type="hidden" name="tab_platform"  id="tab_platform" value='<?=(isset($data['platform']))?$data['platform'] : '' ?>' >

                <input type="hidden" name="tab_purpose_title"  id="tab_purpose_title" value='<?=(isset($data['purpose_title']))?$data['purpose_title'] : '' ?>' >
                <input type="hidden" name="tab_destination_title"  id="tab_destination_title" value='<?=(isset($data['destination_title']))?$data['destination_title'] : '' ?>' >
                <input type="hidden" name="tab_platform_title" id="tab_platform_title" value='<?=(isset($data['platform_title']))?$data['platform_title'] : '' ?>' >            
                
                <input type="hidden" name="tab_price_from"  id="tab_price_from" value='<?=(isset($data['from_price']))?$data['from_price'] : '' ?>' >
                <input type="hidden" name="tab_price_to" id="tab_price_to" value='<?=(isset($data['to_price']))?$data['to_price'] : '' ?>' >
                <input type="hidden" name="tab_depdted" id="tab_depdted" value='<?=(isset($data['isDepdted']))?$data['isDepdted'] : '' ?>' >
                <input type="hidden" name="tab_tourbus" id="tab_tourbus" value='<?=(isset($data['isTourBus']))?$data['isTourBus'] : '' ?>' >
                <input type="hidden" name="tab_search_key" id="tab_search_key" value='<?=(isset($data['search_key']))?$data['search_key'] : '' ?>' >
                <input type="hidden" name="submit_name" id="submit_name" value='<?=(isset($data['submit_name']))?$data['submit_name'] : '' ?>' >
                
                <input type="hidden" name="tab_date_from" id="tab_date_from" value='<?=(isset($data['tab_date_from']))?$data['tab_date_from'] : '' ?>' >
                <input type="hidden" name="tab_date_from_title" id="tab_date_from_title" value='<?=(isset($data['tab_date_from_title']))?$data['tab_date_from_title'] : '' ?>' >
                <input type="hidden" name="tab_date_to" id="tab_date_to" value='<?=(isset($data['tab_date_to']))?$data['tab_date_to'] : '' ?>' >
                <input type="hidden" name="tab_date_to_title" id="tab_date_to_title" value='<?=(isset($data['tab_date_to_title']))?$data['tab_date_to_title'] : '' ?>' >
                
                <input type="hidden" name="tab_dates" id="tab_dates" value='<?=(isset($data['tab_dates']))?$data['tab_dates'] : '' ?>' >

                <input type="hidden" name="tab_depdted_from" id="tab_depdted_from"  value='<?=(isset($data['tab_depdted_from']))?$data['tab_depdted_from'] : '' ?>' >
                <input type="hidden" name="tab_depdted_to" id="tab_depdted_to"  value='<?=(isset($data['tab_depdted_to']))?$data['tab_depdted_to'] : '' ?>' >
                <input type="hidden" name="tab_depdted_title" id="tab_depdted_title"  value='<?=(isset($data['tab_depdted_title']))?$data['tab_depdted_title'] : '' ?>' >
                <input type="hidden" name="action" id="action"  value='<?=(isset($data['action']))?$data['action'] : '' ?>' >

                <input type="hidden" name="sort" id="sort" value='<?=(isset($data['sort']))?$data['sort'] : 'asc' ?>' >
        <?php
                                    } else {
                                        ?>
                <input type="hidden" name="tab_date_depdted" id="tab_date_depdted" value='' >
                <input type="hidden" name="tab_purpose" id="tab_purpose" value='' >
                <input type="hidden" name="tab_purpose_title" id="tab_purpose_title" value='' >
                <input type="hidden" name="tab_destination" id="tab_destination" value='' >
                <input type="hidden" name="tab_destination_title" id="tab_destination_title" value='' >
                <input type="hidden" name="tab_platform" id="tab_platform" value='' >
                <input type="hidden" name="tab_platform_title" id="tab_platform_title" value='' >
                <input type="hidden" name="tab_price_from"  id="tab_price_from" value='' >
                <input type="hidden" name="tab_price_to" id="tab_price_to" value='' >
                <input type="hidden" name="tab_depdted"  id="tab_depdted" value='' >
                <input type="hidden" name="tab_tourbus"  id="tab_tourbus" value='' >
                <input type="hidden" name="tab_search_key"  id="tab_search_key" value='' >
                <input type="hidden" name="submit_name" id="submit_name" value='' >

                <input type="hidden" name="tab_date_from" id="tab_date_from" value='' >
                <input type="hidden" name="tab_date_from_title" id="tab_date_from_title" value='' >
                <input type="hidden" name="tab_date_to" id="tab_date_to" value='' >
                <input type="hidden" name="tab_date_to_title" id="tab_date_to_title" value='' >
                <input type="hidden" name="tab_dates" id="tab_dates" value='' >

                <input type="hidden" name="tab_depdted_from" id="tab_depdted_from" value='' >
                <input type="hidden" name="tab_depdted_to" id="tab_depdted_to" value='' >
                <input type="hidden" name="tab_depdted_title" id="tab_depdted_title" value='' >

                <input type="hidden" name="sort" id="sort" value='' >
        <?php
                                    } ?>
                <?php
                echo $this->Html->script("user/index/index.js", ['block' => 'body-end']);
                ?>
                <!--フッターここから-->
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        window.onhashchange = function() {
                                javascript:history.back();
                            }
                        })
                </script>
