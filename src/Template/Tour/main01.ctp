
<?= $this->Html->css("user/pagination.css"); ?>
<div class="sp">
    <div class="resultsJyouken01">
        <p class="resultsH201"><span class="span01">検索条件</span></p>
        <ul class="resultsUl01">
            <!-- <li><span class="jyoukenSpan01">日数</span>指定しない～日帰り</li>
            <li><span class="jyoukenSpan01">キーワード</span>食べ放題</li> -->
        </ul>
        <div class="resultsBtn01 clearfix resultsBtn0102"><a href="#anc01"><span class="radius2">条件を変えて再検索</span></a></div>
    </div>
</div>
<div class="contIn960 resultsContWrap01">
    <div class="resultsBlock01">
        <div class="resultsLeft">
            <h2 class="resultsH201"><span class="span01">検索結果：</span><span class="span02"><?=(isset($count))?$count:0 ?></span><span
                    class="span01">件</span></h2>
        </div>
        <div class="resultsRight">
            <p class="resultsText01">並び順 ： <a href="#" class='sort_button' attr='asc' >料金が安い順</a> ｜ <a href="#" class='sort_button' attr='desc' >料金が高い順</a></p>
        </div>
    </div>
    <ul class="resultsListWrap">
        <!--商品ここから-->
        <!--19005-1ここから-->
         <?= $this->element('user/tour_main01')?>
        <!--19005-1ここまで-->
        <!--19005-2ここから-->
       
        <!--19005-5ここまで-->
    </ul>
    
       
        <div class="customer-list-paging" id="tablePaging"></div>
        <!-- <li><a href="#">2</a></li> -->
   

</div>
<?php  if (isset($mobile)){ ?>
            <div class="sp" id="anc01">
        <div class="searchAreaWrap02">
            <?= $this->Form->create(null, ['url' => ['controller' => 'tour', 'action' => 'main01'], 'novalidate' => true, 'class' => 'icon-person', 'escape' => false,  'autocomplete'=> 'off', 'id' => 'ApForm1']); ?>
                
                <div class="searchArea01">
                    <div class="resultsBox01">
                        <div class="resultsCell01">
                            <p>日数</p>
                        </div>
                        <div class="resultsCell02">
                            <div class="tabNaviContCell01 accordion radioWrap01">
                                <div class="tabNaviBox01 switch">
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01">
                                    <ul class="clearfix searchInputWrap01">
                                        <li class="li01 odd"><label class="over"><input type="radio" name="cmsDay01"
                                                    value="0" data-title="指定しない" checked="">指定しない</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay01" value="1"
                                                    data-title="日帰り">日帰り</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay01" value="2"
                                                    data-title="2日間">2日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay01" value="3"
                                                    data-title="3日間">3日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay01" value="4"
                                                    data-title="4日間">4日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay01" value="5"
                                                    data-title="5日間">5日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay01" value="6"
                                                    data-title="6日間">6日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay01" value="7"
                                                    data-title="7日間">7日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay01" value="8"
                                                    data-title="8日間">8日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay01" value="9"
                                                    data-title="9日間">9日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay01" value="10"
                                                    data-title="10日間以上">10日間以上</label></li>
                                    </ul>
                                </div>
                            </div>

                            <p class="text02">～</p>

                            <div class="tabNaviContCell01 accordion radioWrap02">
                                <div class="tabNaviBox01 switch">
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01">
                                    <ul class="clearfix searchInputWrap01">
                                        <li class="li01 odd"><label class="over"><input type="radio" name="cmsDay02"
                                                    value="0" data-title="指定しない" checked="">指定しない</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay02" value="1"
                                                    data-title="日帰り">日帰り</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay02" value="2"
                                                    data-title="2日間">2日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay02" value="3"
                                                    data-title="3日間">3日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay02" value="4"
                                                    data-title="4日間">4日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay02" value="5"
                                                    data-title="5日間">5日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay02" value="6"
                                                    data-title="6日間">6日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay02" value="7"
                                                    data-title="7日間">7日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay02" value="8"
                                                    data-title="8日間">8日間</label></li>
                                        <li class="even"><label><input type="radio" name="cmsDay02" value="9"
                                                    data-title="9日間">9日間</label></li>
                                        <li class="odd"><label><input type="radio" name="cmsDay02" value="10"
                                                    data-title="10日間以上">10日間以上</label></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="tab1">
                    <div class="tabNaviCont01">
                        <div class="tabNaviContIn01">

                            <div class="tabNaviContCell01">
                                <label>
                                    <div class="tabNaviBox01 daySp01">
                                        <p class="text01">出発日</p>
                                        <p class="text02">
                                            <input type="text" name="tab1_datedepdted" <?=(isset($data['depdted']))?$data['depdted']: '' ?> onchange="$('#tab1_depdted').val($(this).val())" placeholder="日付未定"
                                                value="" size="10" class="searchInput01"
                                                id="datepicker" >
                                            <span class="iconCal01"><img
                                                    src="../../images/search_icon_cal.png" width="18"
                                                    height="18" alt="カレンダー" class="pcBr"><img
                                                    src="../../images/search_icon_cal_sp.png" alt="カレンダー"
                                                    class="spBr"></span>
                                        </p>
                                    </div>
                                </label>
                            </div>

                            <div class="tabNaviContCell01 accordion radioWrap02">
                                <div class="tabNaviBox01 switch" onclick="InitPurpose(1)">
                                    <p class="text01">目的</p>
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01">
                                    <ul class="clearfix searchInputWrap01">
                                       
                                       
                                    </ul>
                                </div>
                            </div>

                            <div class="tabNaviContCell01 accordion radioWrap01">
                                <div class="tabNaviBox01 switch" onclick="InitRegion(1)">
                                    <p class="text01">行き先</p>
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01">
                                    <ul class="clearfix searchInputWrap01">
                                       
                                        
                                    </ul>

                                            <div id="boxregion1" class="click">
                                                <div class="clickWrap clearfix">
                                                    <div><a class="modal-close imghover detailModalClose"
                                                            href="#"><img src="../../images/common/detail_modal_close.png" width="22" height="22" alt="close" /></a></div>
                                                    <div>
                                                        <p class="clickP01">東北</p>
                                                        <ul class="modalRadioWrap modal-close">
                                                    
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>

                            <div class="tabNaviContCell01 accordion radioWrap03">
                                <div class="tabNaviBox01 switch"  onclick="InitRoute(1)">
                                    <p class="text01">出発場所</p>
                                    <p class="text02">指定しない</p>
                                </div>
                                <div class="tabNaviAcCont01">
                                    <ul class="clearfix searchInputWrap01" id='boxRoute1'>
                                    </ul>
                                    <div id="boxPlatform1" class="click">
                                                <div class="clickWrap clearfix">
                                                    <div><a class="modal-close imghover detailModalClose"
                                                            href="#"><img
                                                                src="../../images/common/detail_modal_close.png"
                                                                width="22" height="22"
                                                                alt="close" /></a></div>
                                                    <div>
                                                        <p class="clickP01">西武池袋線</p>
                                                        <ul class="modalRadioWrap modal-close">
                                                      
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>

                            <div class="tabNaviKeyword"><input type="text" name="tab1_keyword" id="tab1_keyword"
                                    placeholder="キーワード　観光やホテル名など"></div>

                            <div class="accordion tabNaviCont02">
                                <div class="switch02 tabNaviSwitch01">
                                    <p class="p01"><span class="tabNaviSpan01"><span
                                                class="plus">＋</span></span>条件を追加</p>
                                    <p class="p02"><span class="tabNaviSpan01"><span
                                                class="plus">ー</span></span>閉じる</p>
                                </div>
                                <div class=" tabNaviAcCont02">
                                    <ul class="clearfix">
                                        <li class="li01">
                                            <span>予算　</span>
                                            <div class="yosanWrap01">
                                                <p id="amount"></p>
                                                <!--<p>
                                    <input type="text" id="amount">
                                </p>-->
                                                <div id="slider-range"></div>
                                            </div>
                                        </li>
                                        <li> <label><input type="checkbox" name="tab1_isDepdted" class="tab1_isDepdted" value="1">出発決定日あり</label> </li>
                                        <li> <label><input type="checkbox" name="tab1_isTourBus" class='tab1_isTourBus' value="1">添乗員・ガイド同行</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tabNaviContCell02">
                                <input type="button" value="検索" name='submit1' id='submit1' onclick="$('#submit_name').val('submit1');$('#action').val(''); " class="submitBtn01 imghover">
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div style="display: none">

        <?php if ((isset($data['submit_name'])) || !isset($data['submit_name'])) {      ?>
                <input type="hidden" name="tab_date_depdted" id="tab_date_depdted" value='<?=(isset($data['tab_date_depdted']))?$data['tab_date_depdted'] : '' ?>' >
                <input type="hidden" name="tab_purpose"  id="tab_purpose" value='<?=(isset($data['purpose']))?$data['purpose'] : '' ?>' >
                <input type="hidden" name="tab_destination"  id="tab_destination" value='<?=(isset($data['destination']))?$data['destination'] : '' ?>' >
                <input type="hidden" name="tab_platform"  id="tab_platform" value='<?=(isset($data['platform']))?$data['platform'] : '' ?>' >

                <input type="hidden" name="tab_purpose_title"  id="tab_purpose_title" value='<?=(isset($data['purpose_title']))?$data['purpose_title'] : '' ?>' >
                <input type="hidden" name="tab_destination_title"  id="tab_destination_title" value='<?=(isset($data['destination_title']))?$data['destination_title'] : '' ?>' >
                <input type="hidden" name="tab_platform_title" id="tab_platform_title" value='<?=(isset($data['platform_title']))?$data['platform_title'] : '' ?>' >            
                
                <input type="hidden" name="tab_Price_From"  id="tab_Price_From" value='<?=(isset($data['from_price']))?$data['from_price'] : '' ?>' >
                <input type="hidden" name="tab_Price_To" id="tab_Price_To" value='<?=(isset($data['to_price']))?$data['to_price'] : '' ?>' >
                <input type="hidden" name="tab_Depdted" id="tab_Depdted" value='<?=(isset($data['isDepdted']))?$data['isDepdted'] : '' ?>' >
                <input type="hidden" name="tab_TourBus" id="tab_TourBus" value='<?=(isset($data['isTourBus']))?$data['isTourBus'] : '' ?>' >
                <input type="hidden" name="tab_search_key" id="tab_search_key" value='<?=(isset($data['search_key']))?$data['search_key'] : '' ?>' >
                <input type="hidden" name="submit_name" id="submit_name" value='<?=(isset($data['submit_name']))?$data['submit_name'] : '' ?>' >
                
                <input type="hidden" name="tab_date_from" id="tab_date_from" value='<?=(isset($data['tab_date_from']))?$data['tab_date_from'] : '' ?>' >
                <input type="hidden" name="tab_date_from_title" id="tab_date_from_title" value='<?=(isset($data['tab_date_from_title']))?$data['tab_date_from_title'] : '' ?>' >
                <input type="hidden" name="tab_date_to" id="tab_date_to" value='<?=(isset($data['tab_date_to']))?$data['tab_date_to'] : '' ?>' >
                <input type="hidden" name="tab_date_to_title" id="tab_date_to_title" value='<?=(isset($data['tab_date_to_title']))?$data['tab_date_to_title'] : '' ?>' >
                
                <input type="hidden" name="tab_dates" id="tab_dates" value='<?=(isset($data['tab_dates']))?$data['tab_dates'] : '' ?>' >

                <input type="hidden" name="tab_depdted_from" id="tab_depdted_from"  value='<?=(isset($data['tab_depdted_from']))?$data['tab_depdted_from'] : '' ?>' >
                <input type="hidden" name="tab_depdted_to" id="tab_depdted_to"  value='<?=(isset($data['tab_depdted_to']))?$data['tab_depdted_to'] : '' ?>' >
                <input type="hidden" name="tab_depdted_title" id="tab_depdted_title"  value='<?=(isset($data['tab_depdted_title']))?$data['tab_depdted_title'] : '' ?>' >
                <input type="hidden" name="action" id="action"  value='<?=(isset($data['action']))?$data['action'] : '' ?>' >
                <input type="hidden" name="sort" id="sort" value='<?=(isset($data['sort']))?$data['sort'] : 'asc' ?>' >
        <?php
                                    } else {
                                        ?>
                <input type="hidden" name="tab_date_depdted" id="tab_date_depdted" value='' >
                <input type="hidden" name="tab_purpose" id="tab_purpose" value='' >
                <input type="hidden" name="tab_purpose_title" id="tab_purpose_title" value='' >
                <input type="hidden" name="tab_destination" id="tab_destination" value='' >
                <input type="hidden" name="tab_destination_title" id="tab_destination_title" value='' >
                <input type="hidden" name="tab_platform" id="tab_platform" value='' >
                <input type="hidden" name="tab_platform_title" id="tab_platform_title" value='' >
                <input type="hidden" name="tab_Price_From"  id="tab_Price_From" value='' >
                <input type="hidden" name="tab_Price_To" id="tab_Price_To" value='' >
                <input type="hidden" name="tab_Depdted"  id="tab_Depdted" value='' >
                <input type="hidden" name="tab_TourBus"  id="tab_TourBus" value='' >
                <input type="hidden" name="tab_search_key"  id="tab_search_key" value='' >
                <input type="hidden" name="submit_name" id="submit_name" value='' >

                <input type="hidden" name="tab_date_from" id="tab_date_from" value='' >
                <input type="hidden" name="tab_date_from_title" id="tab_date_from_title" value='' >
                <input type="hidden" name="tab_date_to" id="tab_date_to" value='' >
                <input type="hidden" name="tab_date_to_title" id="tab_date_to_title" value='' >
                <input type="hidden" name="tab_dates" id="tab_dates" value='' >

                <input type="hidden" name="tab_depdted_from" id="tab_depdted_from" value='' >
                <input type="hidden" name="tab_depdted_to" id="tab_depdted_to" value='' >
                <input type="hidden" name="tab_depdted_title" id="tab_depdted_title" value='' >
                <input type="hidden" name="sort" id="sort" value='' >
        <?php
                                    } ?>
    </div>
            </form>
        </div>
                    </div>
 <?php } ?>

<!-- お問い合わせ -->
<div class="topContWrap">
    <div class="topContactWrap">
        <div class="topContactBlock">
            <ul class="topContactUl01">
                <li class="li01">
                    <p class="text01"><span class="topContactIcon01"><img src="/../../images/contact_icon_tel.png"
                                alt="tel" /></span>お電話でのお問い合わせ</p>
                    <p class="text02 pc">0570-025-258</p>
                    <p class="text02 sp"><a href="tel:0570-025-258">0570-025-258</a></p>
                    <p class="text03">営業時間：平日 10:00～17:30</p>
                    <p class="text04">※土日祝は休業</p>
                </li>
                <li>
                    <p class="text01"><span class="topContactIcon01"><img src="/../../images/contact_icon_mail.png"
                                alt="tel" /></span>メールでのお問い合わせ</p>
                    <p class="contactBtn01"><a href="/form/form.php" class="imghover radius4">お問い合わせ</a></p>
                </li>
            </ul>
            <ul class="topContactUl01 mgt30">
                <li>
                    <p class="text01">西武バス株式会社</p>
                    <p class="text03">〒359-1180 埼玉県所沢市久米546-1</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div>
    <div class="footerContWrap">
        <div class="clearfix footerContIn">
            <div class="footerSnsWrap01">
                <div class="fb-page" data-href="https://www.facebook.com/seibubus/" data-tabs="timeline"
                        data-width="280" data-height="340" data-small-header="false" data-adapt-container-width="true"
                        data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/seibubus/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/seibubus/">西武バス</a></blockquote>
                        </div>
            </div>
            <div class="footerNaviWrap01">
                <div class="naviCont01 naviContFirst">
                    <p class="naviTit01"><a href="/">ツアーを探す</a></p>
                    <ul class="naviUl01">
                        <!-- <li><a href="#">日帰りツアー</a></li> -->
                        <!-- <li><a href="#">宿泊ツアー</a></li> -->
                        <li><a href="/osusume/">おすすめツアー</a></li>
                        <li><a href="/ebook/">WEBパンフレット</a></li>
                    </ul>
                </div>
                <div class="naviCont01">
                    <p class="naviTit01">西武グリーンツアーについて</p>
                    <ul class="naviUl01 clearfix">
                        <li class="borderRight"><a href="/about/">西武グリーンツアーとは</a></li>
                        <li class="borderRight"><a href="/about/smileseat/">スマイルシート</a></li>
                        <li><a href="/about/antisocial_forces.php">反社会的勢力に対する基本方針</a></li>
                        <!--<li class="sp">&nbsp;</li>-->
                    </ul>
                </div>
                <div class="naviCont01">
                    <p class="naviTit01">ご利用案内</p>
                    <ul class="naviUl01 clearfix">
                        <li class="borderRight"><a href="/guide/">お申し込みからご出発まで</a></li>
                        <li><a href="/guide/meeting">集合場所案内</a></li>
			<li><a href="/guide/pdf/checksheet_covid19.pdf" target="_blank"><span class="naviIcon01">健康管理チェックシート</span></a></li>
			<li><a href="/guide/pdf/douisyo.pdf" target="_blank"><span class="naviIcon01">旅行申込同意書</span></a></li>
                        <li class="borderRight"><a href="/yakkan/">旅行条件書・旅行業約款</a></li>
                        <li><a href="https://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AM01_TOP.PR_CHECK?HV_SYSTEM_CD=MYPAGE&HV_USER_CODE=SEIBUBUS&HV_NEXT_JOB_NO=AM11"
                                target="_blank"><span class="naviIcon01">マイページ（予約内容確認など）</span></a></li>
                        <li class="borderRight"><a
                                href="http://www4sv.we-can.co.jp/pls/WECAN/ST_PK_AH61_TOP.PR_CHECK?HV_USER_CODE=SEIBUBUS"
                                target="_blank"><span class="naviIcon01">メール会員登録</span></a></li>
                        <li><a href="/form/form.php">ツアーのお問い合わせ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

       
       </div>
    

<!--フッターここから-->
 <input type="hidden" name="count" id="count" value="<?=$count ?>">
<input type="hidden" name="size" id="page-size" value="<?=$page_size?>">
<?= $this->Html->script('user/pagi/pagination.js').PHP_EOL ?>
<?php
    echo $this->Html->script("user/index/index.js?date=".date('YmdHis'), ['block' => 'body-end']); //vi add to update lib
?>
