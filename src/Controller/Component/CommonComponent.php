<?php
namespace App\Controller\Component;

use Constants;
use Cake\Utility\Security;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Created by PhpStorm.
 * User: Vi
 * Date: 2019/05/06
 * Time: 10:23
 */
class CommonComponent extends Component
{
	
    public function getPurpose($conds =false)
    {
        $list = TableRegistry::get('MstPurpose')->find()
        ->select([
            'key'   =>'MstPurpose.id',
            'value' =>'MstPurpose.purpose_name',
            // 'count' => '(SELECT count(`tour_id`) FROM `mst_tour_purpose` 
            //             INNER JOIN mst_tour ON mst_tour.id = mst_tour_purpose.tour_id 
            //             where mst_tour_purpose.purpose_id = MstPurpose.id and mst_tour.status = 1 
            //             GROUP by mst_tour_purpose.`purpose_id`)'
            
        ])
        ->where(['is_disp' =>1])
        ->order(['sort' => 'ASC']);
       
        $list_new = [];
        foreach ($list as $v) {
        	$new_conds = $conds;
        	$new_conds[] = ['MstTourPurpose.purpose_id' => $v['key']];
        	$row = $v;
        	$row['count'] = $this->searchByConds($new_conds);
        	$list_new[] = $row;
        }
        return $list_new;
    }

    public function getPlatform($conds=false, $route_sort = false)
    {
        $cond_private = ($route_sort)? ['MstPlatform.route_name' =>$route_sort,'is_disp' =>1]:['is_disp' =>1];
        $list = TableRegistry::get('MstPlatform')->find()
        ->select([
            'key'   =>'MstPlatform.id',
            'value' =>'MstPlatform.platform',
        ])
        ->where($cond_private)
        ->order(['station_sort' => 'ASC']);
        $arrayPlatform = [];        
        foreach ($list as $value) {
        	$new_conds = $conds;
        	$new_conds[] = ['TourPlatforms.platform LIKE' => '%'.$value['value'].'%'];
        	$value['count'] = $this->searchByConds($new_conds);
            $arrayPlatform[] = $value;           
        }
        return $arrayPlatform;
    }
    public function getCountPlatformByRoute($conds=false, $route_sort){
        $cond_private = ['MstPlatform.route_name' =>$route_sort,'is_disp' =>1];
        $list = TableRegistry::get('MstPlatform')->find()
        ->select([
            'value' =>'MstPlatform.platform',
        ])->where($cond_private);
        $new_conds = [];        
        foreach ($list as $value) {
            $new_conds[] = ['TourPlatforms.platform LIKE' => '%'.$value['value'].'%'];
        }
        $new_conds = ['OR'=> $new_conds];
        $conds[] = $new_conds;
        $count = $this->searchByConds($conds);
        return $count;
    }
    public function getRoutePlatform($conds =false)
    {
        $list = TableRegistry::get('MstPlatform')->find()
        ->select(
            [
                'key'   => 'DISTINCT MstPlatform.route_sort',
                'value' => 'MstPlatform.route_name',
            ]
        )
        ->where(['is_disp' =>1])
        ->order(['route_sort' => 'ASC']);
        $arrayRoute = [];
        foreach ($list as $value) {
            $row = $value;
            $count = 0;            
            $row['count'] = $this->getCountPlatformByRoute($conds,$value['value']);
            $arrayRoute[] = $row;
        }
        return $arrayRoute;
    }

    public function getRegion($conds =false)
    {
        $list = TableRegistry::get('MstRegion')->find()
        ->select(
            [
                'key'   => 'MstRegion.id',
                'value' => 'MstRegion.name',
                // 'count' => '(SELECT count(`id`) FROM `mst_tour` where mst_tour.destination = MstRegion.name and (MstRegion.id = 1 OR MstRegion.id = 11) and mst_tour.status = 1)'
            ]
        );
        $list_new = [];
        
        foreach ($list as $v) {
            $count = 0;
        	$row = $v;
        	if($v['key'] == 1 || $v['key'] == 11){
        		$new_conds = $conds;
	        	$new_conds[] = ['MstTour.destination' => $v['value']];
	        	$row = $v;

	        	$row['count'] = $this->searchByConds($new_conds);
	        	
        	}else{
                $count = 0;
                $list_city = $this->getCity($conds, $v['key'] );
                foreach ($list_city as $value) {
                   $count += $value['count'];
                }
                $row['count'] = $count;
            }
        	$list_new[] = $row;
        }
        return $list_new;
    }

    public function getCity($conds =false, $region_id = false)
    {
        $cond_private = ($region_id)? ['MstPrefecture.region_id' =>$region_id]:[];
        $list = TableRegistry::get('MstPrefecture')->find()
        ->select(
            [
                'key'    =>'MstPrefecture.id',
                'value'  =>'MstPrefecture.name',
                // 'count'  =>'(SELECT count(`id`) FROM `mst_tour` where mst_tour.destination = MstPrefecture.name and mst_tour.status = 1)'
            ]
        )
        ->where($cond_private);
        $arrayCity = [];
        foreach ($list as $value) {
        	$new_conds = $conds;
        	$new_conds[] = ['MstTour.destination' => $value['value']];
        	$value['count'] = $this->searchByConds($new_conds);
            $arrayCity[] = $value;
        }
        return $arrayCity;
    }
    public function searchByConds($conds){
    	$rst = TableRegistry::get('MstTour')->find()
	            ->join([
	                'MstTourPurpose' => [
	                    'table'      => 'mst_tour_purpose',
	                    'type'       => 'LEFT',
	                    'conditions' => [
	                        'MstTourPurpose.tour_id = MstTour.id'
	                    ]
	                ],
	                'TourPlatforms'  => [
	                    'table'      => 'mst_tour_platform',
	                    'type'       => 'LEFT',
	                    'conditions' => [
	                        'TourPlatforms.tour_id =MstTour.id'
	                    ]
	                ],
	                'MstTourPrice'   => [
	                    'table'      => 'mst_tour_price',
	                    'type'       => 'LEFT',
	                    'conditions' => [
	                        'MstTourPrice.tour_id =MstTour.id'
	                    ]
	                ],
                    'MstTourSchedule'   => [
                        'table'      => 'mst_tour_schedule',
                        'type'       => 'INNER',
                        'conditions' => [
                            'MstTourSchedule.tour_id =MstTour.id'
                        ]
                    ]
	            ])
	            ->select(['id' =>'MstTour.id'])
	            ->where($conds)
                ->group(['MstTour.id']);
               // ->having(['max(MstTourPrice.depdt) IS NOT' =>NULL]);
         return $rst->count();
    }
    public function getDepdtDate($conds, $select =['value' =>'DISTINCT(MstTourPrice.depdt)']){
        $rst = TableRegistry::get('MstTour')->find()
                ->join([
                    'MstTourPurpose' => [
                        'table'      => 'mst_tour_purpose',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'MstTourPurpose.tour_id = MstTour.id'
                        ]
                    ],
                    'TourPlatforms'  => [
                        'table'      => 'mst_tour_platform',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'TourPlatforms.tour_id =MstTour.id'
                        ]
                    ],
                    'MstTourPrice'   => [
                        'table'      => 'mst_tour_price',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'MstTourPrice.tour_id =MstTour.id'
                        ]
                    ],
                    'MstTourSchedule'   => [
                        'table'      => 'mst_tour_schedule',
                        'type'       => 'INNER',
                        'conditions' => [
                            'MstTourSchedule.tour_id =MstTour.id'
                        ]
                    ]
                ])
                ->select($select)
                ->where($conds);
        
        return $rst;
    }
}