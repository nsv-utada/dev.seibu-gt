<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;

class LoginController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('LogAdminAccess');
        $this->viewBuilder()->setLayout('admin_login');
    }

    public function index()
    {
        $request      = $this->getRequest();
        $logoutAction = $request->getSession()->consume("logout_action");
        $this->set('logoutAction', $logoutAction);
        if ($request->is("post")) {
            $email    = $request->getData('email');
            $admins   = Configure::read('admins');
            $password = $request->getData("password");
            $hasher   = new DefaultPasswordHasher();
            foreach ($admins as $admin) {
                if ($admin['email'] == $email) {
                    if ($hasher->check($password, $admin['password'])) {
                        $request->getSession()->write('admin_authed', 1);

                        $log                  = $this->LogAdminAccess->newEntity();
                        $log->account         = $email;
                        $log->datetime        = date("Y-m-d H:i:s");
                        $log->remote_address  = $_SERVER['REMOTE_ADDR'];
                        if (isset($_SERVER['HTTP_USER_AGENT'])) {
                            $log->http_user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }

                        $this->LogAdminAccess->save($log);

                        $redirect = $request->getSession()->consume("url_before_login");
                        if ($redirect) {
                            return $this->redirect($redirect);
                        } else {
                            return $this->redirect('/admin/top');
                        }
                    }
                }
            }
            $this->set('error', "ログインIDまたはパスワードが違います");
            $this->set('email', $email);
            $this->set('password', $password);
        }
    }
}
