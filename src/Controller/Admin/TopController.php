<?php
namespace App\Controller\Admin;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

use App\Controller\AppController;
use App\Helper\TourImport;
use App\Model\Entity\MstTour;

class TopController extends AppController
{
    public $itemCols = [
        ['key' => 'MstTour.status',    'label' => '状態',       'class' => ''],
        ['key' => 'MstTour.tourcd',    'label' => 'コースNo.',   'class' => '', 'sortable' => 1],
        ['key' => 'tournm',    'label' => 'タイトル',    'class' => ''],
        ['key' => 'depdt_values',    'label' => '設定期間',    'class' => '', 'sortable' => 1],
        ['key' => 'depdted',   'label' => '出発決定日',   'class' => ''],
        ['key' => 'autoup',    'label' => '自動更新',    'class' => ''],
    ];

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin_default');

        $this->loadModel('MstTour');
    }

    public function index()
    {
        $this->set("title", "コース管理");
        $countItems = $this->countItems([]);
        $this->set("countItems", $countItems);
    }

    public function grid()
    {
        $response = ['success' => 0, 'error' => ''];
        $items    = [];

        try {
            $request  = $this->getRequest();
            $data     = $this->getItems($request);

            $builder  = $this->viewBuilder();
            $builder->setLayout(false);
            // main table
            $view     = $builder->build();
            $view->set("cols", $this->itemCols);
            $view->set("rows", $data['rows']);
            $view->set("sortByField", $data['sortByField']);
            $view->set("sortOrder", $data['sortOrder']);
            $view->set("statuses", MstTour::STATUSES);
            $grid   = $view->render('Grid/admin_top_items');

            // paging
            $view   = $builder->build();
            $view->set("paging", $data['paging']);
            $paging = $view->render('Grid/admin_top_paging');

            $response['success']    = 1;
            $response['html']       = $grid;
            $response['paging']     = $paging;
            $response['count']      = $data['paging']['count'];
            $response['counts']     = $data['counts'];
            $response['lastUpdate'] = $data['lastUpdate'];
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }

    /**
     * Get items from database
     *
     * @param object $request a request object
     *
     * @return array
     */
    public function getItems($request)
    {
        $pageNumber    = $request->getData('pageNumber');
        $items         = $this->MstTour->find()
            ->select(
                [
                    'MstTour.id', 'MstTour.status', 'MstTour.tourcd',
                    'MstTour.tournm', 'MstTour.depdted', 'MstTour.autoup',
                    'MstTour.destination', 'MstTour.naiyo',
                    'schedule_values' => '(GROUP_CONCAT' .
                        '(' .
                            'CONCAT_WS(' .
                                '" ", TourSchedules.naiyo1, TourSchedules.naiyo2, TourSchedules.naiyo3' .
                            ') ' .
                            'ORDER BY TourSchedules.nichiji,TourSchedules.gyono ASC '.
                            'SEPARATOR "," ' .
                        ')' .
                    ')',
                    'purpose_values' => '(GROUP_CONCAT' .
                        '(' .
                            'MstPurposes.purpose_name ORDER BY MstPurposes.sort ASC SEPARATOR ","' .
                        ')' .
                    ')',
                    'depdt_values' => '(GROUP_CONCAT(TourPrices.depdt ORDER BY TourPrices.depdt DESC SEPARATOR ","))',
                    'platform_values' => '(GROUP_CONCAT(TourPlatforms.platform ORDER BY TourPlatforms.depdt DESC SEPARATOR ","))'
                ]
            )
            ->contain(['TourPrices', 'TourPlatforms', 'TourSchedules', 'Purposes'])
            ->join(
                [
                    'TourPurposes' => [
                        'table'      => 'mst_tour_purpose',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'MstTour.id = TourPurposes.tour_id'
                        ]
                    ]
                ]
            )
            ->join(
                [
                    'MstPurposes' => [
                        'table'      => 'mst_purpose',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'TourPurposes.purpose_id = MstPurposes.id'
                        ]
                    ]
                ]
            )
            ->join(
                [
                    'TourPlatforms' => [
                        'table'      => 'mst_tour_platform',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'TourPlatforms.tour_id = MstTour.id'
                        ]
                    ]
                ]
            )
            ->join(
                [
                    'TourSchedules' => [
                        'table'      => 'mst_tour_schedule',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'TourSchedules.tour_id = MstTour.id'
                        ]
                    ]
                ]
            )
            ->join(
                [
                    'TourPrices' => [
                        'table'      => 'mst_tour_price',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'TourPrices.tour_id = MstTour.id'
                        ]
                    ]
                ]
            );

        $whereQueries  = [];
        $havingQueries = [];
        if (!$pageNumber) {
            $pageNumber = 1;
        }

        $status = $request->getData("status");
        if ($status) {
            // value is +1 in the template so -1 here
            $status                 = $status - 1;
            $whereQueries['MstTour.status'] = $status;
        }
        $search  = trim($request->getData("search"));
        $search  = str_replace("'", "", $search);
        $pattern = '/ +|　+|\xc2\xa0+/u';
        $search  = preg_replace($pattern, '%', $search);
        $parts   = explode("%", $search);
        $searchr = implode("%", array_reverse($parts));
        $having  = "CONCAT_WS(' ', tournm, MstTour.tourcd, schedule_values, destination, naiyo, purpose_values, platform_values) collate utf8_unicode_ci LIKE ";
        if ($search) {
            if ($search != $searchr) {
                $havingQueries[] = "(" .
                    "(" . $having . "'%" . $search . "%')" .
                    " OR " .
                    "(" . $having . "'%" . $searchr . "%')" .
                ")";
            } else {
                $havingQueries[] = $having . "'%" . $search . "%'";
            }
        }

        if (count($whereQueries)) {
            $items = $items->where($whereQueries);
        }

        $pageConfig = ["limit" => 20, "page" => $pageNumber];

        $sortByField = $request->getData('sortByField');
        $sortOrder   = $request->getData('sortOrder');
        if (!$sortOrder) {
            $sortOrder = "ASC";
        }

        if ($sortByField) {
            $items = $items->order([$sortByField => $sortOrder]);
        } else {
            $items = $items->order(["update_date" => "DESC"]);
        }
        
        $items      = $items->group(['MstTour.id', 'MstTour.tourcd']);
        if (count($havingQueries)) {
            $items  = $items->having($havingQueries);
        }
 
        $items      = $this->paginate($items, $pageConfig);

        $paging     = $this->getRequest()->getParam("paging.MstTour");

        $rows = [];
        foreach ($items as $item) {
            $row                = [];
            $row["id"]          = $item->id;
            $row['tour_prices'] = $item->tour_prices;
            $row['tourcd']      = $item->tourcd;
            $row['status']      = $item->status;
            foreach ($this->itemCols as $col) {
                $key       = $col['key'];
                $value     = $item->$key;
                $row[$key] = $value;
            }
            $rows[] = $row;
        }
        
        $lastUpdate = $this->MstTour->find()
            ->select(['last_update' => "MAX(update_date)"])
            ->first()
            ->last_update;
        $lastUpdate = str_replace("-", "/", $lastUpdate);
        
        return [
            'rows'        => $rows,
            'paging'      => $paging,
            'sortByField' => $sortByField,
            'sortOrder'   => $sortOrder,
            'counts'      => $this->countItems($whereQueries),
            'lastUpdate'  => $lastUpdate
        ];
    }

    // count item for each status
    public function countItems($whereQueries)
    {
        if (isset($whereQueries['status'])) {
            unset($whereQueries['status']);
        }
        $counts   = [];
        // count based on status value
        $statuses = array_keys(MstTour::STATUSES);
        foreach ($statuses as $status) {
            $count = $this->MstTour->find()
                ->select(['count' => 'COUNT(*)'])
                //->where($whereQueries)
                ->where(['status' => $status])
                ->first()
                ->count;
            $counts[$status] = $count;
        }

        $counts['total'] = $this->MstTour->find()
            ->select(['count' => 'COUNT(*)'])
            ->first()
            ->count;

        return $counts;
    }

    public function massUpdate()
    {
        $itemIds = $this->getRequest()->getData('itemIds');
        $status  = $this->getRequest()->getData('status');
        $response = ['success' => 0, 'error' => ''];
        try {
            if ($status == "delete") {
                // delete items when delete is selected
                $conn      = ConnectionManager::get('default');
                try {
                    $conn->begin();
                    
                    TableRegistry::get("MstTourPurpose")->deleteAll(['tour_id IN ' => $itemIds]);
                    TableRegistry::get("MstTourPrice")->deleteAll(['tour_id IN ' => $itemIds]);
                    TableRegistry::get("MstTourPlatform")->deleteAll(['tour_id IN ' => $itemIds]);
                    TableRegistry::get("MstTourSchedule")->deleteAll(['tour_id IN ' => $itemIds]);
                    TableRegistry::get("MstTour")->deleteAll(['id IN ' => $itemIds]);
                    
                    $conn->commit();
                    $response['success'] = 1;
                } catch (\PDOException $e) {
                    $this->log($e->getMessage());
                    $this->log($e->getTraceAsString());
                    $response['error'] = $e->getMessage();
                    $conn->rollback();
                }
            } else {
                $now   = date("Y-m-d H:i:s");
                $query = $this->MstTour->query();
                $query->update()
                    ->set([
                        'status' => $status,
                        'update_date' => $now
                    ])
                    ->where(['id IN' => $itemIds])
                    ->execute();
                $response['success'] = 1;
            }
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }

    public function import()
    {
        $response = ['success' => 0, 'error' => ''];
        try {
            $import   = new TourImport();
            $response = $import->main();
            $this->log($response);
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }
}
