<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class LogoutController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $request = $this->getRequest();
        $request->getSession()->destroy();
        $request->getSession()->write("logout_action", 1);
        return $this->redirect('/admin/login');
    }
}
