<?php
namespace App\Controller\Admin;

use stdClass;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class DetailController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('MstTour');
        $this->loadModel('MstPurpose');
        $this->loadModel('MstPlatform');
        $this->viewBuilder()->setLayout('admin_default');
    }

    public function index()
    {
        $this->set("title", "コース詳細・編集");
        $tourId  = $this->getRequest()->getQuery('tourId');
        if (!is_numeric($tourId)) {
            throw new NotFoundException();
        }
        $item    = $this->MstTour->get(
            $tourId,
            ['contain' => ['Purposes', 'TourPrices', 'TourPlatforms', 'TourSchedules']]
        );
        $session = $this->getRequest()->getSession();
            
        if (!$item) {
            throw new NotFoundException();
        }

        $savedData = $session->consume("admin_detail_save_data");
        if ($savedData) {
            $this->MstTour->patchEntity($item, $savedData);
        }
        
        $fields = ['naiyo1', 'naiyo2', 'naiyo3'];
        foreach ($item->tour_schedules as $schedule) {
            foreach ($fields as $field) {
                $schedule->$field = str_replace("→", " ", $schedule->$field);
            }
        }
        $this->set("item", $item);

        $purposes = $this->MstPurpose->find()
            ->where(['is_disp' => 1])
            ->order(['sort' => 'ASC'])
            ->all();
        $this->set("purposes", $purposes);

        $platforms = $this->MstPlatform->find()
            ->where(['is_disp' => 1])
            ->order(['route_sort' => 'ASC', 'station_sort' => 'ASC'])
            ->all();
        $this->set("platforms", $platforms);
            
        $response = $session->consume('admin_detail_save_response');
        $this->set("response", $response);

        $config = [
            'max_image_width'  => Configure::read("max_image_width"),
            'max_image_height' => Configure::read("max_image_height"),
        ];
        $this->set('config', $config);
    }

    public function save()
    {
        if (!$this->getRequest()->is(['post', 'put'])) {
            return $this->redirect("/admin/top");
        }

        $tourId    = $this->getRequest()->getData("id");
        $isPreview = $this->getRequest()->getData("is-preview");
        $item      = $this->MstTour->get(
            $tourId,
            [
                'contain' => [
                    'Purposes', 'TourPrices', 'TourPlatforms', 'TourSchedules'
                ]
            ]
        );

        if ($isPreview) {
            $item->isPreview = true;
        }

        $session  = $this->getRequest()->getSession();
        $response = ['success' => 0, 'error' => '', 'errors' => []];

        if (!$item) {
            return $this->redirect("/admin/top");
        }

        try {
            $fields = [
                'autoup', 'tournm', 'status', 'depdted', 'meal',
                'tourcon', 'busguid', 'naiyo',
            ];

            $selects = ['autoup', 'busguid', 'tourcon', 'status'];
            // set item fields
            foreach ($fields as $field) {
                $value = $this->getRequest()->getData($field);

                // select input has value +1 in the form to avoid send 0
                if (in_array($field, $selects)) {
                    $value = $value - 1;
                }

                $item->$field = $value;
            }

            // set purposes - relationship table
            $purposes     = $this->getRequest()->getData('purposes');
            $savePurposes = [];
            if ($purposes && is_array($purposes) && count($purposes)) {
                foreach ($purposes as $purposeId) {
                    $savePurposes[$purposeId] = [
                        'purpose_id' => $purposeId,
                        'tourcd'     => $item->tourcd
                    ];
                }
            }
            foreach ($item->purposes as $pivotPurpose) {
                $pivotId   = $pivotPurpose->id;
                $purposeId = $pivotPurpose->purpose_id;
                if (isset($savePurposes[$purposeId])) {
                    $savePurposes[$purposeId]['id'] = $pivotId;
                }
            }
            
            $this->MstTour->patchEntity($item, ['purposes' => $savePurposes]);

            // set tour prices - relationship table
            $tourPrices     = $this->getRequest()->getData('tour_prices');
            if (!$tourPrices) {
                $tourPrices = [];
            }
            foreach ($tourPrices as $index => $tourPriceData) {
                // remove the item if there is no data
                if (!$tourPriceData['depdt'] || !$tourPriceData['price']) {
                    unset($tourPrices[$index]);
                    continue;
                }
                $tourPrices[$index]['depdt']  = str_replace("/", "-", $tourPriceData['depdt']);
                $tourPrices[$index]['tourcd'] = $item->tourcd;
                $price = str_replace(",", "", $tourPriceData['price']);
                if (!is_numeric($price)) {
                    throw new \Exception("金額を数字で入力してください");
                }
                $tourPrices[$index]['price'] = (int)$price;
            }
            $this->MstTour->patchEntity($item, ['tour_prices' => $tourPrices]);
            
            // set platforms - relationship table
            $platforms = $this->getRequest()->getData('platforms');
            if (!$platforms) {
                $platforms = [];
            }
            foreach ($platforms as $index => $platformData) {
                if (!$platformData['depdt'] || !$platformData['platform']) {
                    unset($platforms[$index]);
                    continue;
                }
                $platforms[$index]['depdt']  = str_replace("/", "-", $platformData['depdt']);
                $platforms[$index]['tourcd'] = $item->tourcd;
            }
            $this->MstTour->patchEntity($item, ['tour_platforms' => $platforms]);

            // set schedules - relationship table
            $schedules = $this->getRequest()->getData('schedules');
            if (!$schedules) {
                $schedules = [];
            }
            foreach ($schedules as $index => $scheduleData) {
                if (!$scheduleData['nichiji'] && !$scheduleData['gyono'] &&
                    !$scheduleData['naiyo1'] && !$scheduleData['naiyo2'] && !$scheduleData['naiyo3']
                ) {
                    unset($schedules[$index]);
                    continue;
                }
                $stringFields = ['naiyo1', 'naiyo2', 'naiyo3'];
                foreach ($stringFields as $field) {
                    //$string = str_replace(" ", "→", $scheduleData[$field]);
                    $string = $scheduleData[$field];
                    $schedules[$index][$field] = $string;
                }
                $schedules[$index]['tourcd'] = $item->tourcd;
            }
            
            $this->MstTour->patchEntity($item, ['tour_schedules' => $schedules]);
            
            $session->write('admin_detail_save_data', $item->toArray());

            // delete images if there is delete-images in request
            $deleteImages = $this->getRequest()->getData("delete-images");
            if ($deleteImages && is_array($deleteImages) && count($deleteImages)) {
                foreach ($deleteImages as $imageField) {
                    if (!$imageField || !$item->$imageField) {
                        continue;
                    }
                    $file = WWW_ROOT . "/tours/images/" . $item->id . "/" . $item->$imageField;
                    if (file_exists($file)) {
                        @unlink($file);
                    }
                    $item->$imageField = "";
                }
            }

            $now = date("Y-m-d H:i:s");
            if (!$item->register_date) {
                $item->register_date = $now;
            }

            if (!$item->tourcd) {
                $item->tourcd = "";
            }

            // save if not on preview
            if (!$isPreview) {
                $table = TableRegistry::get('MstTour');
                $table->save($item);
            }

            $this->saveUrlImages($item, $this->getRequest(), $response);
            $this->saveUploadImages($item, $this->getRequest(), $response);

            // save if not on preview
            if (!$isPreview) {
                $table->save($item);
                $session->delete('admin_detail_save_data');
            }

            if ($isPreview) {
                $session->write("admin_detail_preview_item", $item);
            }

            $response['success'] = 1;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }

        // return json when preview
        if ($isPreview) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $session->write('admin_detail_save_response', $response);
        
        return $this->redirect("/admin/detail?tourId=" . $tourId);
    }

    public function preview()
    {
        $response  = ['success' => 0, 'error' => ''];
        try {
            $item     = new stdClass();
            $item->id = "preview-" . uniqid();
            $this->saveImages($item, $this->getRequest(), $response);
            $response['item']    = $item;
            $response['success'] = 1;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            $response['error'] = $e->getMessage();
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }

    public function saveUrlImages($item, $request, &$response)
    {
        $imageFields  = [
            'mimage1', 'mimage2', 'mimage3', 'mimage4', 'mimage5',
            'pimage1', 'pimage2', 'pimage3', 'pimage4', 'pimage5'
        ];
        foreach ($imageFields as $imageField) {
            $imageUrlField = "input-url-" . $imageField;
            if ($imageUrl = $request->getData($imageUrlField)) {
                $item->$imageField = $imageUrl;
            }
        }
    }

    // hande URL in input field
    public function downloadUrlImages($item, $request, &$response)
    {
        $imageFields  = [
            'mimage1', 'mimage2', 'mimage3', 'mimage4', 'mimage5',
            'pimage1', 'pimage2', 'pimage3', 'pimage4', 'pimage5'
        ];
        $allowFormats = ['png', 'jpeg', 'jpg'];
        foreach ($imageFields as $imageField) {
            $imageUrlField = "input-url-" . $imageField;
            if ($imageUrl = $request->getData($imageUrlField)) {
                // extract image name from URL
                $parts     = explode("/", $imageUrl);
                $imageName = end($parts);
                // extract if there is ? in name part
                $parts     = explode("?", $imageName);
                $imageName = array_shift($parts);
                // check image extension
                $parts     = explode(".", $imageName);
                $ext       = end($parts);
                if (!in_array(strtolower($ext), $allowFormats)) {
                    $message = "URL: " . $imageUrl . " is not valid image";
                    $response['errors'][$imageUrlField] = $message;
                    throw new \Exception($message);
                }
                $dir = WWW_ROOT . "/tours/images/" . $item->id . "/download";
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                $fileName = preg_replace('/[^a-z0-9_\\-\\.]+/i', '_', $imageName);
                $path = $dir . "/" . $imageName;
                
                $isCopy = @copy($imageUrl, $path);
                if (!$isCopy) {
                    $error = error_get_last();
                    $message = "URL: " . $imageUrl . " error: " . $error['message'];
                    $response['errors'][$imageUrlField] = $message;
                    throw new \Exception($message);
                    continue;
                }

                
                $fileExtension = pathinfo($path, PATHINFO_EXTENSION);
                $fileExtension = strtolower($fileExtension);
                $imageSize     = @getimagesize($path);
                if (!in_array($fileExtension, $allowFormats) || !$imageSize) {
                    $message = "URL: " . $imageUrl . " is not image file";
                    $response['errors'][$imageUrlField] = $message;
                    throw new \Exception($message);
                }

                if ($imageSize[0] > Configure::read("max_image_width") || $imageSize[1] > Configure::read("max_image_height")) {
                    $message = "URL: " . $imageUrl . " has size bigger than limit " .
                        Configure::read("max_image_width") . "x" .
                        Configure::read("max_image_height") . "px";
                    $response['errors'][$imageUrlField] = $message;
                    throw new \Exception($message);
                }

                if ($item->isPreview) {
                    $fileName = $imageField . "_preview_" . $fileName;
                } else {
                    $fileName = $imageField . "_" . $fileName;
                }
                $dir     = WWW_ROOT . "/tours/images/" . $item->id;
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                rename($path, $dir . "/" . $fileName);
                $item->$imageField = $fileName;
            }
        }
    }

    // handle image upload by browser
    public function saveUploadImages($item, $request, &$response)
    {
        $imageFields  = [
            'mimage1', 'mimage2', 'mimage3', 'mimage4', 'mimage5',
            'pimage1', 'pimage2', 'pimage3', 'pimage4', 'pimage5'
        ];
        $allowFormats = ['png', 'jpeg', 'jpg'];
        
        foreach ($imageFields as $imageField) {
            $name = $request->getData($imageField . ".name");
            $file = $request->getData($imageField . ".tmp_name");
            if ($name) {
                $fileExtension = pathinfo($name, PATHINFO_EXTENSION);
                $fileExtension = strtolower($fileExtension);
                $imageSize     = @getimagesize($file);
                if (!in_array($fileExtension, $allowFormats) || !$imageSize) {
                    $message = "File: " . $name . " is not image file";
                    $response['errors'][$imageField] = $message;
                    throw new \Exception($message);
                }
                
                if ($imageSize[0] > Configure::read("max_image_width") || $imageSize[1] > Configure::read("max_image_height")) {
                    $message = "File: " . $name . " has size bigger than limit " .
                        Configure::read("max_image_width") . "x" .
                        Configure::read("max_image_height") . "px";
                    $response['errors'][$imageField] = $message;
                    throw new \Exception($message);
                }

                // remove special charracters from file name
                $fileName = preg_replace('/[^a-z0-9_\\-\\.]+/i', '_', $name);

                if ($item->isPreview) {
                    $fileName = $imageField . "_preview_" . $fileName;
                } else {
                    $fileName = $imageField . "_" . $fileName;
                }
                $path     = WWW_ROOT . "/tours/images/" . $item->id;
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                move_uploaded_file($file, $path . "/" . $fileName);
                $item->$imageField = $fileName;
            }
        }
    }

    public function delete($itemId)
    {
        $response  = ['success' => 0, 'error' => ''];
        $conn      = ConnectionManager::get('default');
        try {
            $conn->begin();
            TableRegistry::get("MstTourPurpose")
                ->deleteAll(['tour_id' => $itemId]);
            TableRegistry::get("MstTourPrice")
                ->deleteAll(['tour_id' => $itemId]);
            TableRegistry::get("MstTourPlatform")
                ->deleteAll(['tour_id' => $itemId]);
            TableRegistry::get("MstTourSchedule")
                ->deleteAll(['tour_id' => $itemId]);

            $table = TableRegistry::get('MstTour');
            $item  = $this->MstTour->get($itemId);
            $table->delete($item);
            
            $conn->commit();

            // delete image files also
            try {
                $imageDir = WWW_ROOT . "/tours/images/" . $itemId;
                $folder   = new Folder($imageDir);
                $folder->delete();
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }

            $response['success'] = 1;
        } catch (\PDOException $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            $response['error'] = $e->getMessage();
            $conn->rollback();
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }
}
