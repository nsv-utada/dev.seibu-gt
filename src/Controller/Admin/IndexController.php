<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class IndexController extends AppController
{
    public function index()
    {
        return $this->redirect("/admin/top");
    }
}
