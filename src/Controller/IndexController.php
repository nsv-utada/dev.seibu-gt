<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Controller\Component;

class IndexController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('home');
    }

    public function index($string = false)
    {
      $this->set('title','西武グリーンツアー');
      if ($string !='') {
            $this->redirect(
                array('controller' => 'errors', 'action' => 'error404')
            );
        }
         ($this->Common->getPurpose());
    }
}
