<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Controller\Component;
use Cake\Event\Event;

class TourController extends AppController
{
    var $other = false;
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('home');

        $this->loadModel('MstTour');
        $this->loadModel('MstPurpose');
        $this->loadModel('MstPrefecture');
        $this->loadComponent('Paginator');
    }

    public function load($conds)
    {

        //2
        $listPurpose = $this->Common->getPurpose($conds);
        $this->set('listPurpose', $listPurpose);
        //3
        $listRegion = $this->Common->getRegion();
        $this->set('listRegion', $listRegion);
        $listCity = $this->Common->getCity();
        $this->set('listCity', $listCity);
        //4
        $listRoutePlatform = $this->Common->getRoutePlatform();
        $this->set('listRoutePlatform', $listRoutePlatform);
        $listPlatform = $this->Common->getPlatform();
        $this->set('listPlatform', $listPlatform);
    }

    public function main01($string = false)
    {
        $this->viewBuilder()->setLayout('home');
        session_cache_limiter('private_no_expire');
        session_start();
        if ($this->RequestHandler->isMobile()) {
            $this->set('mobile', 'true');
        }        
        $conds = [];
        $data1 = [];
        $sort = 'asc';
        if ($string !='') {
            $this->redirect(
                array('controller' => 'errors', 'action' => 'error404')
            );
        }
        if ($this->request->is(['POST', 'PUT','GET'])) {
            if ($this->request->is('POST')) {
                $_POST = $_POST;
            } else {
                $_POST = $_GET;
            }
            $data1 = $this->processData($_POST);
            $conds = $this->build_cond($data1);
            $sort =  $data1['sort'];
        }
        
       
        $query = $this->MstTour->mkSearchBy($conds, ['MstTourPrice.price'=>$sort,'MstTour.id'=> 'asc'], false);
        $this->paginate = [
            'limit' => 10
        ];
        $rst = $this->paginate($query)->toArray();
        
        if($data1['action'] == 'index' && $this->other == false && $data1['submit_name'] != 'submit2'){
            unset($data1['tab_dates']);
            unset($data1['tab_date_to']);
            unset($data1['tab_date_to_title']);
            unset($data1['tab_date_from']);
            unset($data1['tab_date_from_title']);
            unset($data1['tab_dates']);
        }
        $data1['action'] = '';
        $this->set('data', $data1);

        $this->request->session()->write('data_search', $data1);
        $this->set('count', $query->count());
        $this->set('page_size', 10);
        $this->set('rst', $rst);
        //$this->load($conds); 20200629 vi comment for load
        if ($this->request->is('ajax')) {

            $this->viewBuilder()->setLayout('');
            $this->render('/Element/user/tour_main01');
        }
    }

    public function detail($id = false, $string = false)
    {
        $this->viewBuilder()->setLayout('detail'); //20200629 vi change to new layout
        $conds = [];
        $this->set('title', '名勝「姨捨」の夜景鑑賞と伝統の町並み散策 一軒宿の稲荷山温泉 2日間');
        if (!is_numeric($id) || $string !='') {
            $this->redirect(
                array('controller' => 'errors', 'action' => 'error404')
            );
        }
        if ($id) {
            try {
                $item   = $this->MstTour->get($id, ['contain' => ['Purposes', 'TourPrices','TourPlatforms', 'TourSchedules']]);

                if (!$item) {                    
                    return $this->redirect("/tour/main01");
                } else {
                    if ($item['status'] != 1) {
                        // allow view page as admin
                        $isAdmin = $this->getRequest()->getSession()->read('admin_authed');
                        if (!$isAdmin) {
                            // only redirect if not admin
                            return $this->redirect("/tour/main01");
                        }
                    }
                }
            } catch (\Exception $e) {
                return $this->redirect("/tour/main01");
            }
        } else {
            return $this->redirect("/tour/main01");
        }
        $TourPlatform = [];
        $list_tour= [];
        if (isset($item['tour_platforms']) && count($item['tour_platforms']) > 0) {
            foreach ($item['tour_platforms'] as $v) {
                $TourPlatform[] = $v['platform'];
            }
            $list_tour = $this->MstTour->mkSearchBy(['TourPlatforms.platform IN' => $TourPlatform, 'MstTour.status' => 1,'MstTour.id !=' => $id], ['MstTourPrice.price'=>'DESC'], 4);
            if (count($list_tour->toArray()) == 0) {
                $list_tour= [];
            }
        } else {
            $list_tour= [];
        }
        $data1 = $this->request->session()->read('data_search');
        $this->set('data', $data1);
        
        //$this->load($conds); //20200629 vi comment for not loading search bar
        $this->set('list_tour', $list_tour);
        $this->set('title', $item['tournm']);
        $this->set("item", $item);
    }


    public function preview()
    {
        $session = $this->getRequest()->getSession();
        $item    = $session->consume("admin_detail_preview_item");
        $this->set("item", $item);
        $this->render('/Tour/detail');
    }
    
    public function build_cond($data, $name = '')
    {
        $rst = [];
        $rst[] = ['MstTour.status' =>1];
        if ($data) {

            $this->other = false;
            if ($data['purpose'] != '' && !empty($data['purpose']) && ($name !== 'purpose')) {
                $rst[] = ['MstTourPurpose.purpose_id' =>(int)$data['purpose'] ];
                $this->other = true;
            }
            if ($data['destination'] != '' && !empty($data['destination'])&& ($name !== 'destination')) {
                $rst[] = ['MstTour.destination' => $data['destination']];
                $this->other = true;
            }
            
           
            if ($data['isDepdted'] != '' && !empty($data['isDepdted'])) {
                $rst[] = ['MstTour.depdted IS NOT ' => null,
                                'MstTour.depdted != ' => '0000-00-00',
                                'MstTour.depdted NOT LIKE' => '%0000%',
                                'MstTour.depdted !=' =>''


                 ];
                 $this->other = true;
            }

            if ($data['isTourBus'] != '' && !empty($data['isTourBus'])) {
                $rst[]['OR'] = ['MstTour.tourcon'=> 1,  'MstTour.busguid' =>1];
                $this->other = true;
            }
            
            
                
                // $rst[]['OR']  = ['MstTour.tourcd LIKE'=> '%'.$data['search_key'].'%',
                //                 'MstTour.tournm collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'MstTour.naiyo collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'MstTour.destination collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'TourPlatforms.platform collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'MstTourSchedule.naiyo1 collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'MstTourSchedule.naiyo2 collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 'MstTourSchedule.naiyo3 collate utf8_unicode_ci LIKE ' =>'%'.$data['search_key'].'%',
                //                 $array_or
                //             ];
                //==========================platform
            

            if ($data['platform'] != '' ||  !empty($data['platform'])) {
                
                if ($data['tab_date_depdted'] != '' && !empty($data['tab_date_depdted'])) {
                    $this->other = true;
                    $rst[] = ['TourPlatforms.depdt' => date('Y-m-d', strtotime($data['tab_date_depdted']))];
                } else {
                    if (isset($data['tab_depdted_from']) && $data['tab_depdted_from'] !='') {
                        $this->other = true;
                        $rst[] = ['TourPlatforms.depdt >=' => date('Y-m-d', strtotime($data['tab_depdted_from']))];
                    }
                    if (isset($data['tab_depdted_to']) && $data['tab_depdted_to'] !='') {
                        $this->other = true;
                        $rst[] = ['TourPlatforms.depdt <=' => date('Y-m-d', strtotime($data['tab_depdted_to']))];
                    }
                }
            } else {
                if ($data['tab_date_depdted'] != '' && !empty($data['tab_date_depdted'])) {
                    $this->other = true;
                    $rst[] = ['MstTourPrice.depdt' => date('Y-m-d', strtotime($data['tab_date_depdted']))];
                } else {
                    if (isset($data['tab_depdted_from']) && $data['tab_depdted_from'] !='') {
                        $this->other = true;
                        $rst[] = ['MstTourPrice.depdt >=' => date('Y-m-d', strtotime($data['tab_depdted_from']))];
                    }
                    if (isset($data['tab_depdted_to']) && $data['tab_depdted_to'] !='') {
                        $this->other = true;
                        $rst[] = ['MstTourPrice.depdt <=' => date('Y-m-d', strtotime($data['tab_depdted_to']))];
                    }
                }
            }
 
            if ($data['platform'] != '' && !empty($data['platform'])&& ($name !== 'platform')) {
                $this->other = true;
                $rst[] = ['TourPlatforms.platform LIKE' => '%'.$data['platform']. '%'];
            }
            if ($data['from_price'] && !empty($data['from_price']) && (int)($data['from_price']) > 1000) {
                $this->other = true;
                $rst[] = ['MstTourPrice.price >=' => $data['from_price']];
            }
            if ($data['to_price'] && !empty($data['to_price'])&& (int)($data['to_price']) > 1000) {
                $this->other = true;
                if ((int)($data['to_price']) != 50000) {
                    $rst[] = ['MstTourPrice.price <= ' => $data['to_price']];
                }
            }
            if ($data['search_key'] != '' && !empty($data['search_key'])) {
                $data['search_key'] = trim($data['search_key'], "   ");
                $data['search_key'] = trim($data['search_key'], "  ");
                $data['search_key'] = trim($data['search_key']);
                $array_or = [];
                $rst[]['OR']  = [];
                mb_internal_encoding('UTF-8');
                mb_regex_encoding('UTF-8');
                $string =mb_split('[[:space:]]', $data['search_key']);
                //==========================purpose
                $purpose_id_array = [];
                foreach ($string as $value) {
                    $conds = ['MstPurpose.purpose_name collate utf8_unicode_ci LIKE '=> '%'.$value.'%'];
                    $purpose_list = $this->MstPurpose->mkSearchBy($conds, false, false);
                    foreach ($purpose_list as $key => $value) {
                        $purpose_id_array[] = $value['id'];
                    }
                }      
                
                if (count($purpose_id_array) > 0) {
                    $array_or= ['MstTourPurpose.purpose_id IN'=> $purpose_id_array ];
                }
                foreach ($string as $key => $value) {
                    if (count($purpose_id_array) > 0 && $key == 0) {
                    $conds = ['OR'=>['MstTour.tourcd LIKE'=> '%'. $value.'%',
                                    'MstTour.tournm collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTour.naiyo collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTour.destination collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'TourPlatforms.platform collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo1 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo2 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo3 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourPurpose.purpose_id IN'=> $purpose_id_array

                            ]];
                    }else{ $conds = ['OR'=>['MstTour.tourcd LIKE'=> '%'. $value.'%',
                                    'MstTour.tournm collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTour.naiyo collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTour.destination collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'TourPlatforms.platform collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo1 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo2 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%',
                                    'MstTourSchedule.naiyo3 collate utf8_unicode_ci LIKE ' =>'%'. $value.'%']
                            ];
                    }
                    $rst[] = $conds;
                }
                
            
            }
            if(($name =='platform' || $name =='destination' || $name =='purpose')){
                $this->other =true;
            }

            if (isset($data['submit_name']) && $data['submit_name'] == 'submit2') {
                $rst[] = ['MstTour.priod > ' =>1 ];
            } elseif (isset($data['submit_name']) && $data['submit_name'] == 'submit1') {

                if ($data['tab_dates'] != '' && !empty($data['tab_dates'])) {
                    if ($data['action'] == 'index') {
                        if($this->other == false){
                            return $rst;
                        }
                    }

                    $dateArray = array_map('intval', explode(",", $data['tab_dates']));                    
                    $rst[] = ['MstTour.priod IN'=> $dateArray ];
                } elseif ($data['action'] == 'index' ) {
                    if($this->other == false){
                        return $rst;
                    }else{
                        $rst[] = ['MstTour.priod ' => 1 ];
                    }
                    
                }
            }
        }
         $rst[] = ['MstTour.status' =>1];
        return  $rst;
    }
    public function processData($params)
    {
        if (isset($params['submit_name']) && $params['submit_name'] == 'submit2') {
            $data['submit_name'] ='submit2';
        } elseif (isset($params['submit_name']) && $params['submit_name'] == 'submit1') {
            $data['submit_name'] ='submit1';
        } else {
            $data['submit_name'] ='submit1';
        }
        
       
        $data['destination'] = (isset($params['tab_destination']))?$params['tab_destination']:'';
        $data['platform'] = (isset($params['tab_platform']))?$params['tab_platform']:'';
        $data['from_price'] = (isset($params['tab_price_from']))? $params['tab_price_from']:'';
        $data['to_price'] = (isset($params['tab_price_to']))? $params['tab_price_to']:'';
        ;
        $data['isDepdted'] = (isset($params['tab_depdted']))?$params['tab_depdted']:'';
        $data['isTourBus'] = (isset($params['tab_tourbus']))?$params['tab_tourbus']:'';
        $data['search_key'] = (isset($params['tab_search_key']))?$params['tab_search_key']:'';

        $params = $this->getTitle($params);
        $data['tab_date_depdted'] = (isset($params['tab_date_depdted']))?$params['tab_date_depdted']:'';
        $data['purpose'] = (isset($params['tab_purpose']))?$params['tab_purpose']:'';
        $data['platform_title'] = (isset($params['tab_platform_title']))?$params['tab_platform_title']:'';
        $data['purpose_title'] = (isset($params['tab_purpose_title']))?$params['tab_purpose_title']:'';
        $data['destination_title'] = (isset($params['tab_destination_title']))?$params['tab_destination_title']:'';
        $data['tab_date_from'] =  (isset($params['tab_date_from']))?$params['tab_date_from']:'';
        $data['tab_date_from_title'] =  (isset($params['tab_date_from_title']))?$params['tab_date_from_title']:'';
        $data['tab_date_to'] =  (isset($params['tab_date_to']))?$params['tab_date_to']:'';
        $data['tab_date_to_title'] =  (isset($params['tab_date_to_title']))?$params['tab_date_to_title']:'';
        $data['tab_dates'] =  (isset($params['tab_dates']))?$params['tab_dates']:'';

        $data['tab_depdted_from'] =  (isset($params['tab_depdted_from']))?$params['tab_depdted_from']:'';
        $data['tab_depdted_to'] =  (isset($params['tab_depdted_to']))?$params['tab_depdted_to']:'';
        $data['tab_depdted_title'] =  (isset($params['tab_depdted_title']))?$params['tab_depdted_title']:'';

        $data['action'] =  (isset($params['action']))?$params['action']:'';
        if ($data['action'] == 'index') {
            if (isset($params['submit_name']) && $params['submit_name'] == 'submit2') {
                $data['tab_date_from'] =  2;
                $data['tab_date_from_title'] = '2日間';
                for ($i=2; $i <= 30; $i++) {
                    $dateArray[] = $i;
                }
                $data['tab_dates'] = implode(',', $dateArray);
            } elseif (isset($params['submit_name']) && $params['submit_name'] == 'submit1') {
                $data['tab_date_to'] = 1;
                $data['tab_date_to_title'] =  '日帰り';
                $data['tab_date_from'] = 1;
                $data['tab_date_from_title'] =  '日帰り';
                $data['tab_dates'] = "0,1";
            }
        }

        $data['sort'] = (isset($params['sort']) && $params['sort'] == 'desc')?$params['sort']:'asc';
        $data1 = $data;

        return $data1;
    }
    public function getTitle($params)
    {
        if (isset($params['tab_purpose']) && $params['tab_purpose']!='') {

            $this->loadModel('MstPurpose');
            $purpose =  $this->MstPurpose->find()
            ->where(['MstPurpose.purpose_name' => $params['tab_purpose']]);
            if(isset($purpose->toArray()[0])){
                $params['tab_purpose'] = $purpose->toArray()[0]['id'];
                $params['tab_purpose_title'] =  $purpose->toArray()[0]['purpose_name'];
            }else{
               // $this->redirect(
               // array('controller' => 'errors', 'action' => 'notfound')
               //  );
            }
           
            
        }
        if (isset($params['tab_destination']) && $params['tab_destination']!='') {
            $params['tab_destination_title'] = $params['tab_destination'];
        }
        if (isset($params['tab_platform']) && $params['tab_platform']!='') {
            $params['tab_platform_title'] = $params['tab_platform'];
        }
        $list_date = [
            0 =>'指定しない',
            1 =>'日帰り',
            2 =>'2日間',
            3 =>'3日間',
            4 =>'4日間',
            5 =>'5日間',
            6 =>'6日間',
            7 =>'7日間',
            8 =>'8日間',
            9 =>'9日間',
            10=>'10日間以上',
            30 =>'指定しない'
        ];
        if (isset($params['tab_dates']) && $params['tab_dates'] != '') {
            $dateArray = array_map('intval', explode(",", $params['tab_dates']));
            $params['tab_date_from'] = min($dateArray);
            $params['tab_date_from_title'] =(isset($params['tab_date_from_title']))?$params['tab_date_from_title'] : $list_date[(int)$params['tab_date_from']];
            $params['tab_date_to'] = max($dateArray);
            $params[''] =  isset($params['tab_date_to_title'])?$params['tab_date_to_title']: $list_date[(int)$params['tab_date_to']];
        } else {
            if (isset($params['tab_date_from']) && $params['tab_date_from'] !='') {
                $index = (int)$params['tab_date_from'];
                $params['tab_date_from_title'] =(isset($list_date[$index]))?$list_date[$index]:'' ;
                $dateArray = [];
                for ($i=$index; $i <= 30; $i++) {
                    $dateArray[] = $i;
                }
                $params['tab_dates'] = implode(',', $dateArray);
            }
            if (isset($params['tab_date_to']) && $params['tab_date_to'] !='') {
                $index = (int)$params['tab_date_to'];
                $params['tab_date_to_title'] = (isset($list_date[$index]))?$list_date[$index]:'' ;
                for ($i=0; $i <= $index; $i++) {
                    $dateArray[] = $i;
                }
                $params['tab_dates'] = implode(',', $dateArray);
            }
            
            if (isset($params['tab_date_from']) && $params['tab_date_from'] !=='' && isset($params['tab_date_to']) && $params['tab_date_to'] !=='') {
                $dateArray = [];
                for ($i=(int)$params['tab_date_from']; $i <= (int)$params['tab_date_to']; $i++) {
                    $dateArray[] = $i;
                }
                $params['tab_dates'] = implode(',', $dateArray);
            }

        }
        // if (isset($params['tab_month']) && $params['tab_month'] !='') {
        //     $params['tab_month'] = $params['tab_month'] ; 
        //     $params['tab_depdted_from'] = date('Y/m/1', strtotime($params['tab_month'].'/01'));
        //     $params['tab_depdted_to'] = date('Y/m/t', strtotime($params['tab_month'].'/01'));
        //     $params['tab_depdted_title'] = date('m', strtotime($params['tab_month'].'/01')).'月未定';
        // } else {
        //     if (isset($params['tab_depdted_to']) && $params['tab_depdted_to'] !='') {
        //         $params['tab_depdted_to'] = date('Y/m/d', strtotime($params['tab_depdted_to']));
        //     }
        //     if (isset($params['tab_depdted_from']) && $params['tab_depdted_from'] !='') {
        //         $params['tab_depdted_title'] = date('Y/m/d', strtotime($params['tab_depdted_from']));
        //     }
        // }
        if (isset($params['tab_date_depdted']) && $params['tab_date_depdted'] != '') {
            if(strlen($params['tab_date_depdted'])<8){
                $params['tab_month'] = $params['tab_date_depdted'] ; 
                $params['tab_depdted_from'] = date('Y/m/1', strtotime($params['tab_month'].'/01'));
                $params['tab_depdted_to'] = date('Y/m/t', strtotime($params['tab_month'].'/01'));
                $params['tab_depdted_title'] =(int)date('m', strtotime($params['tab_month'].'/01')).'月未定';
                unset($params['tab_date_depdted']);
                unset($params['tab_month']);
            }elseif(strlen($params['tab_date_depdted'])>8){
                unset($params['tab_month']);
                unset($params['tab_depdted_from']);
                unset($params['tab_depdted_to']);
                unset($params['tab_depdted_title']);
            }
        }
        if(isset($params['submit_name']) && $params['submit_name'] == 'submit2'){
            for ($i=2; $i <= 30; $i++) {
                    $dateArray[] = $i;
                }
            $params['tab_dates'] = implode(',', $dateArray);
        }
        $params['tab_date_depdted'] = (isset($params['tab_date_depdted']))?$params['tab_date_depdted']:'';
        return $params;
    }

    public function getPullDownPurposeByCondition()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;
        $data = $this->processData($_GET);
        $conds = $this->build_cond($data, 'purpose');
        $list = $this->Common->getPurpose($conds);
        echo json_encode($list);
        die();
    }
    public function getPullDownRegionByCondition()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;
        $data = $this->processData($_GET);
        $conds = $this->build_cond($data, 'destination');
        $list = $this->Common->getRegion($conds);
        echo json_encode($list);
        die();
    }
    public function getPullDownCityByCondition()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;

        $data = $this->processData($_GET);
        $conds = $this->build_cond($data, 'destination');
        $list = $this->Common->getCity($conds, $_GET['region_id']);
        echo json_encode($list);
        die();
    }
    public function getPullDownPlatformByCondition()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;

        $data = $this->processData($_GET);
        $conds = $this->build_cond($data, 'platform');
        $list = $this->Common->getPlatform($conds, $_GET['route_sort']);
        echo json_encode($list);
        die();
    }
    public function getPullDownRouteByCondition()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;

        $data = $this->processData($_GET);
        $conds = $this->build_cond($data, 'platform');
        $list = $this->Common->getRoutePlatform($conds);
        echo json_encode($list);
        die();
    }
    public function getDepdtDate()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;
        $data = $this->processData($_GET);
        $select =['value' =>'DISTINCT(MstTourPrice.depdt)'];
        if ($data['platform'] !='') {
            $select =['value' =>'DISTINCT(TourPlatforms.depdt)'];
        }
        $conds = $this->build_cond($data);
        $list = $this->Common->getDepdtDate($conds, $select);
        echo json_encode($list);
        die();
    }
}
