<?php

return [
    // simple admin accounts
    'admins' => [
        [
            'email' => 'admin@dev.seibu-gt.com',
            'password' => '$2y$10$FJmW46alGtqlwD4G6igmWeXh.h.tXyu4nqrcjJ6Ia8uOWMMw.ObcO'
        ],
        [
            'email' => 'seibu@dev.seibu-gt.com',
            'password' => '$2y$10$Ooa.ketTESaxvGMMS8JZt.gs6aAHWUTgt4aLg1lnfnJgzwae4TuqS'
        ],
        [
            'email' => 'nguyentuongvi92@gmail.com',
            'password' => '$2y$10$y3DgdogmcYll9PH5RiRmJeAwKAMVKyzpcKYmTftkh.wv.TqYjNRnW'
        ]
    ]
];
