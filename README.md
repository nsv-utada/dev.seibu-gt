Set up website with `public_html` folder:

    cd /path/to/your/public_html

Clone the site into the `app` folder:

    git clone git@bitbucket.org:nsv-utada/dev.seibu-gt.git app

To install cakephp lib run command:
    
    composer install

Add database details in `config/app.php` in to `Datasources => Default`, line #251 - #260

You should able access to `http://webiste/app` now