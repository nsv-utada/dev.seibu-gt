jQuery(document).ready(function() {
    // response is set in template
    if (response) {
        if (response.success) {
            // don't show as FB 64
            //swal("登録しました。");
        } else {
            swal(response.error);
        }
    }

    jQuery(document).ajaxError(function(event, request, setting) {
        if (request.status == 403) {
            location.reload(true);
        }
    });

    // handle radio button set for status as there are two of them
    jQuery(".select-autoup").click(function() {
        if (jQuery(this).is(":checked")) {
            var link = jQuery(this).data("link");
            jQuery("#" + link).prop("checked", true);
        }
    });

    jQuery("#btn-add-tour-price-row").click(function() {
        var priceData   = {};
        priceData.id    = "";
        priceData.depdt = "";
        priceData.price = "";
        addPriceRow(countPriceRows, priceData);
    });

    jQuery("#tour-prices").on("click", ".btn-delete-price-row", function() {
        jQuery(this).parent().remove();
    });

    jQuery("#btn-add-platform-row").click(function() {
        var platformData   = {};
        platformData.id    = "";
        platformData.depdt = "";
        platformData.platform = "";
        addPlatformRow(countPlaformRows, platformData);
    });

    jQuery("#tour-platforms").on("click", ".btn-delete-platform-row", function() {
        jQuery(this).parent().remove();
    });

    jQuery("#tour-schedules").on("click", ".btn-delete-schedule-row", function() {
        jQuery(this).parent().parent().remove();
    });
    
    jQuery(".input-datepicker").datepicker({
        showOn: "both",
        buttonImage: baseUrl + "/img/calendar.svg",
        buttonImageOnly: true,
        buttonText: "Select date"
    });

    jQuery(".btn-upload-image").click(function(event) {
        event.preventDefault();
        var imageField = jQuery(this).data("image-field");
        jQuery("#input-upload-" + imageField).trigger("click");
    });

    // show image using url in input, show delete button, show next input
    jQuery(".add-url-button").click(function (event) {
        var imageField = jQuery(this).data("image-field");
        var input      = jQuery("#input-url-" + imageField).val();
        if (!input) {
            swal("この項目が必須です。");
            return false;
        }

        if (input) {
            var newImage    = new Image();
            newImage.id     = 'image-' + imageField;

            // define action when image load first
            newImage.onload = function() {
                utilsModule().hideLoadingModal();
                // check image
                jQuery("#image-name-" + imageField).html(newImage);
                var width  = this.width;
                var height = this.height;
                if (width > config.max_image_width || height > config.max_image_height) {
                    var message = "Image has size bigger than limit " +
                        config.max_image_width + "x" +
                        config.max_image_height + "px";
                    swal(message);
                    return false;
                }

                // show delete
                jQuery("#delete-image-box-" + imageField).show();

                // hide current input
                jQuery("#upload-image-box-" + imageField).hide();
                jQuery("#upload-image-box-" + imageField).addClass('has-image').removeClass('no-image');

                // show next input
                var nextUploadButtons = jQuery("#upload-image-box-" + imageField).nextAll('.upload-image-box.no-image');
                if (nextUploadButtons.length) {
                    jQuery(nextUploadButtons[0]).show();
                }
            }

            newImage.onerror = function() {
                swal('Invalid Image: ' + input);
                return false;
            }

            // assign src to call action above for image
            newImage.src = input;
            utilsModule().displayLoadingModal();
        }
    });

    // click delete image button function: set delete value, hide current image, clear input, show input and add button
    jQuery(".btn-delete-image").click(function (event) {
        event.preventDefault();
        var imageField = jQuery(this).data("image-field");

        // set delete value
        jQuery("#input-delete-" + imageField).val(imageField);

        // hide current image
        jQuery("#delete-image-box-" + imageField).hide();

        // clear input
        jQuery("#input-url-" + imageField).val("");

        // show input and add button
        jQuery("#upload-image-box-" + imageField).show();
        jQuery("#upload-image-box-" + imageField).removeClass('has-image').addClass('no-image');
    });

    jQuery("#btn-add-schedule").click(function() {
        var scheduleData     = {};
        scheduleData.nichiji = "";
        scheduleData.gyono   = "";
        scheduleData.naiyo1  = "";
        scheduleData.naiyo2  = "";
        scheduleData.naiyo3  = "";
        addScheduleRow(countScheduleRows, scheduleData);
    });

    jQuery(".btn-delete").click(function(event) {
        event.preventDefault();
        var itemId = jQuery('#input-id').val();
        swal("このコースを削除します。" + "\n" + "よろしいですか？", {
            buttons: {
                cancel: "いいえ",
                catch: {
                    text: "はい",
                    value: "catch",
                }
            }
        })
        .then(function(value) {
            if (value == "catch") {
                utilsModule().displayLoadingModal();
                
                jQuery.ajax({
                    url:     baseUrl + 'admin/detail/delete/' + itemId,
                    type:    'GET',
                    error:   function() {},
                    success: function(resposne) {
                        if (resposne.success) {
                            swal('削除完了しました。');
                            setTimeout(function() {
                                window.location.href = baseUrl + 'admin/top';
                            }, 1000);
                        } else {
                            swal(response.error);
                        }
                    }
                });
            }
        });
    });

    jQuery(".btn-submit").click(function(event) {
        event.preventDefault();
        
        if (!validateForm()) {
            return false;
        }

        jQuery("#input-is-preview").val("");

        swal("登録しますか？", {
            buttons: {
                cancel: "いいえ",
                catch: {
                    text: "はい",
                    value: "catch",
                }
            }
        })
        .then(function(value) {
            if (value == "catch") {
                utilsModule().displayLoadingModal();
                jQuery("#detail-form").submit();
            }
        });

        return false;
    });

    jQuery(".btn-preview").click(function (event) {
        event.preventDefault();

        if (!validateForm()) {
            return false;
        }

        jQuery("#input-is-preview").val(1);

        utilsModule().displayLoadingModal();

        var formData = new FormData(jQuery("#detail-form")[0]);
        var tourId   = jQuery("#input-id").val();
        jQuery.ajax({
            url: baseUrl + "/admin/detail/save",
            type: 'POST',
            data: formData,
            headers: {'X-CSRF-TOKEN': csrfToken},
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function (response) {
                utilsModule().hideLoadingModal();
                if (response.success) {
                    showPreviewPoup(tourId, response);
                } else {
                    swal(response.error);
                    return;
                }
            }
        });
    });

    jQuery("#tour-platforms").on("click", ".train-icon", function(event) {
        event.preventDefault();
        var trainIcon = this;
        var wrapper   = document.createElement('div');
        var innerHTML = "<div>";

        var index;
        var item;
        var routeName = "";
        for (index = 0; index < platforms.length; index++) {
            item = platforms[index];
            if (item.route_name != routeName) {
                innerHTML = innerHTML + '<div style="clear:both;"></div>' +
                    "<p style='font-weight:bold;text-align:left'><br />" + item.route_name + "</p>";
                    
                routeName = item.route_name;
            }
            innerHTML = innerHTML +
                '<div class="platform-item">' +
                    '<input class="input-platform-item" type="radio" name="platform" ' +
                        'value="' + item.id + '" id="platform-item-' + item.id + '" />' +
                    '<label for="platform-item-' + item.id + '">' + item.platform + '<label>' +
                '</div>';
        }
        innerHTML = innerHTML + '<div style="clear:both;"></div>';

        wrapper.innerHTML = innerHTML;
        swal({
            title: "出発場所",
            content: wrapper,
            className: 'platform-items-popup'
        }).then(function(value) {
            var text = jQuery('.platform-items-popup .input-platform-item:checked').next('label').text();
            jQuery(trainIcon).prev("input").val(text);
        });
    });

    // add relationship data
    addPriceRows();
    addPlatformRows();
    addScheduleRows();

    jQuery('body').on('propertychange input', '.force-number', forceNumber);
});

function forceNumber()
{
    var inputElement = jQuery(this);
    inputElement.val(inputElement.val().replace(/[^\d]+/g,''));
}

function validateForm()
{
    var inputs = jQuery(".schedule-row-required");
    if (inputs.length) {
        var index;
        var input;
        var value;
        var hasError = false;
        for (index = 0; index < inputs.length; index++) {
            input = inputs[index];
            value = jQuery(input).val();
            if (value.length == 0) {
                jQuery(input).css("border-color", "red");
                hasError = input;
            }
        }

        if (hasError) {
            swal("この項目が必須です。");
            window.scrollTo(0, jQuery(hasError).offset().top - 100);
            return false;
        }
    }
    
    var files = jQuery("#detail-form").find("input:file");
    if (files.length) {
        var index;
        var file;
        for (index = 0; index < files.length; index++) {
            file = files[index].files[0];
            if (file) {
                // limit is 4MB
                if (file.size > 4*1024*1024) {
                    swal("したファイルが大きすぎます");
                    return false;
                }
            }
        }
    }
    return true;
}

function showPreviewPoup(tourId, response)
{
    var wrapper = document.createElement('div');
    var innerHTML = "<div class='iframe-wrapper' " +
        "style=''>" +
            "<iframe style='" +
                "width:1100px;height:1000px' " +
                "src='" + baseUrl + "tour/preview'>" +
            "</iframe>" +
        "</div>";
    wrapper.innerHTML = innerHTML;

    swal({
        title: "プレビュー",
        content: wrapper,
        className: 'item-preview-popup',
        onClose: onClosePreview
    }).then(function() {
        onClosePreview();
    });

    jQuery(".swal-overlay").scrollTop(0);
}

function onClosePreview()
{
    setTimeout(function() {
        jQuery('.swal-overlay').remove();
    }, 50);
    
    jQuery('#detail-form').focus();
}

var countPriceRows = 0;
function addPriceRow(index, priceData)
{
    var priceRow = '<div class="tour-price-row';
    if (jQuery("#tour-prices .tour-price-row").length) {
        priceRow = priceRow + ' m-t10';
    }
    var depdt = '';
    if (priceData.depdt) {
        // little format date time from mysql to Y/m/d (yyyy/mm/dd)
        depdt = priceData.depdt;
        depdt = depdt.split("T");
        depdt = depdt[0];
        depdt = depdt.replace(/-/g, "/");
    }

    if (priceData.price === null) {
        priceData.price = "";
    }
    priceRow = priceRow + '">出発日&nbsp;' +
        '<input type="text"' +
            'class="input-datepicker loginInput02 w-100px" ' +
            'id="input-price-date-' + index + '" ' +
            'name="tour_prices[' + index + '][depdt]" ' +
            'placeholder="' + today + '" ' +
            'value="' + depdt + '" />' +
        '<span class="m-250">料金</span>' +
        '<input type="text" name="tour_prices[' + index + '][price]" ' +
            'class="loginInput02 w-100px text-right force-number" ' + 
            'placeholder="5000" ' +
            'value="' + priceData.price + '" />' +
        '&nbsp;円' +
        '<span class="m-250">&nbsp;</span>' +
        '<input type="button" value="削除" ' +
            'class="adminbutton02 imghover btn-delete-price-row" />';
    
    if (priceData.id) {
        priceRow = priceRow +
            '<input type="hidden" value="' + priceData.id + '" ' +
                'name="tour_prices[' + index + '][id]" />';
    }

    priceRow = priceRow + '</div>';
    jQuery("#tour-prices").append(priceRow);
    jQuery("#input-price-date-" + index).datepicker({
        showOn: "both",
        buttonImage: baseUrl + "/img/calendar.svg",
        buttonImageOnly: true,
        buttonText: "Select date"			
    });
    countPriceRows++;
}
function addPriceRows()
{
    var priceData = {};
    if (tourPrices.length) {
        var index;
        for (index = 0; index < tourPrices.length; index++) {
            priceData = tourPrices[index];
            addPriceRow(index, priceData);
        }
    } else {
        priceData.id = "";
        priceData.depdt = "";
        priceData.price = "";
        addPriceRow(countPriceRows, priceData);
    }
}

var countPlaformRows = 0;
function addPlatformRow(index, platformData)
{
    var platformRow = '<div class="tour-platform-row';
    if (jQuery("#tour-platforms .tour-platform-row").length) {
        platformRow = platformRow + ' m-t10';
    }

    var depdt = '';
    if (platformData.depdt) {
        // little format date time
        depdt = platformData.depdt;
        depdt = depdt.split("T");
        depdt = depdt[0];
        depdt = depdt.replace(/-/g, "/");
    }
    hide = ''
    readonly = ""
    edit = ($('#input-id').val() !='')?true:false
    if(edit){
        readonly = "readonly"
        hide = 'display-hide'
    }
    platformRow = platformRow + '">出発日:&nbsp;' +
        '<input type="hidden"' +
            'class="input-datepicker loginInput02 w-100px" ' +
            'id="input-platform-date-' + index + '" ' +
            'name="platforms[' + index + '][depdt]" ' +
            'placeholder="' + today + '" ' +
            'readonly="' +readonly+ '" ' +
            'value="' + depdt + '" />' +depdt+
        '<span class="m-l50">出発場所:</span>' +
        '<input type="hidden" name="platforms[' + index + '][platform]" ' +
            'class="loginInput02 w-100px text-c" ' + 
            'placeholder="新宿駅" maxlength="10" ' +
            'readonly="' +readonly+ '" ' +
            'value="' + platformData.platform + '" />' + platformData.platform +
        '<a class="train-icon" href="#"><img src="' + baseUrl + '/img/train.svg" ' +
            'height="40" alt="train" class="train '+hide+'" /></a>' +
        '<span class="m-250" style="margin-left:100px;">&nbsp;</span>' +
        '<input type="button" value="削除" ' +
            'class="adminbutton02 imghover btn-delete-platform-row '+hide+'" />';
    
    if (platformData.id) {
        platformRow = platformRow +
            '<input type="hidden" value="' + platformData.id + '" ' +
                'name="platforms[' + index + '][id]" />';
    }

    platformRow = platformRow + '</div>';
    jQuery("#tour-platforms").append(platformRow);
    if(!edit){
        jQuery("#input-platform-date-" + index).datepicker({
            showOn: "both",
            buttonImage: baseUrl + "/img/calendar.svg",
            buttonImageOnly: true,
            buttonText: "Select date"			
        });
    }
    countPlaformRows++;
}
function addPlatformRows()
{
    var platformData = {};
    if (tourPlatforms.length) {
        var index;
        for (index = 0; index < tourPlatforms.length; index++) {
            platformData = tourPlatforms[index];
            addPlatformRow(index, platformData);
        }
    } else {
        platformData.id = "";
        platformData.depdt = "";
        platformData.platform = "";
        addPlatformRow(countPlaformRows, platformData);
    }
}

var countScheduleRows = 0;
function addScheduleRow(index, scheduleData)
{

    if (scheduleData.nichiji === null) {
        scheduleData.nichiji = "";
    }

    if (scheduleData.gyono === null) {
        scheduleData.gyono = "";
    }

    var scheduleRow = "<div class='tour-schedule-row' style='border-bottom: 1px solid #dddddd;margin-bottom: 20px'>" +
        "<p>" +
            "<span>日次</span>&nbsp;&nbsp;&nbsp;<input style='width:70px;text-align:center' class='loginInput02 force-number schedule-row-required'" +
                " name='schedules[" + index + "][nichiji]' value='" + scheduleData.nichiji + "'/>" +
            "&nbsp;&nbsp;&nbsp;<span> 行No </span> <input style='width:70px;text-align:center' class='loginInput02 force-number schedule-row-required'" +
                " name='schedules[" + index + "][gyono]' value='" + scheduleData.gyono + "'/>" +
        "</p>" +
        "<p><span>内容1 </span> <input style='width:85%' class='loginInput02' type='text'" +
            " name='schedules[" + index + "][naiyo1]' maxlength='500' value='" + scheduleData.naiyo1 + "'/><p>" +
        "<p><span>内容2 </span> <input style='width:85%' class='loginInput02' type='text'" +
            " name='schedules[" + index + "][naiyo2]' maxlength='500' value='" + scheduleData.naiyo2 + "'/><p>" +
        "<p><span>内容3 </span> <input style='width:85%' class='loginInput02' type='text'" +
            " name='schedules[" + index + "][naiyo3]' maxlength='500' value='" + scheduleData.naiyo3 + "'/><p>" +
        "<p style='text-align:right;margin-right:60px'><input type='button' value='削除' " +
            "class='adminbutton02 imghover btn-delete-schedule-row' /></p>";
    
    if (scheduleData.id) {
        scheduleRow = scheduleRow +
            '<input type="hidden" value="' + scheduleData.id + '" ' +
                'name="schedules[' + index + '][id]" />';
    }

    scheduleRow = scheduleRow + '<br /></div>';
    jQuery("#tour-schedules").append(scheduleRow);
    
    countScheduleRows++;
}
function addScheduleRows()
{
    if (tourSchedules.length) {
        var index;
        for (index = 0; index < tourSchedules.length; index++) {
            scheduleData = tourSchedules[index];
            addScheduleRow(index, scheduleData);
        }
    } else {
        jQuery("#btn-add-schedule").trigger("click");
    }
}