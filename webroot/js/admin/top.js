jQuery(document).ready(function() {
    // use local storage to get paging paramters and re-apply to grid
    if (localStorage.getItem("paging")) {
        var pagingData = localStorage.getItem("paging");
        pagingData     = JSON.parse(pagingData);
        jQuery("#input-page-number").val(pagingData.pageNumber);
        jQuery("#input-status").val(pagingData.status);
        jQuery("#input-search").val(pagingData.search);
        jQuery("#input-sort-field").val(pagingData.sortByField);
        jQuery("#input-sort-order").val(pagingData.sortOrder);
        localStorage.removeItem("paging");
    }

    jQuery(document).ajaxError(function(event, request, setting) {
        if (request.status == 403) {
            location.reload(true);
        }
    });


    reloadGrid();

    jQuery(".admin-top-paging").on("click", ".page-link", function(event) {
        event.preventDefault();
        var pageNumber = jQuery(this).data("page-number");
        jQuery("#input-page-number").val(pageNumber);
        reloadGrid();
        window.scrollTo(0, jQuery('.statusUl01').offset().top - 100);
    });

    jQuery('#btn-apply-status').click(function() {
        var items   = jQuery(".item-checkbox:checked");
        var itemIds = [];
        var index;
        for (index = 0; index < items.length; index++) {
            var item = items[index];
            var id   = jQuery(item).data('item-id');
            itemIds.push(id);
        }
        if (!itemIds.length) {
            return;
        }
        var status = jQuery("#select-apply-status").val();
        if (!status || status == '0') {
            return;
        }

        if (status == 'delete') {
            swal("このコースを削除します。" + "\n" + "よろしいですか？", {
                buttons: {
                    cancel: "いいえ",
                    catch: {
                        text: "はい",
                        value: "catch",
                    }
                }
            })
            .then(function(value) {
                if (value == "catch") {
                    massAction('delete', itemIds);
                }
            });
        } else {
            // -1 because status value on form is + 1 to void 0 or empty problem
            status = status - 1;
            massAction(status, itemIds);
        }
    });

    jQuery(".btn-big-status").click(function(event) {
        event.preventDefault();
        var status = jQuery(this).data('status');
        // the select has value +1
        jQuery("#input-status").val(status);
        jQuery("#input-page-number").val(1);
        // status filter will clear search
        jQuery("#input-search").val("");
        reloadGrid();
    });

    jQuery(".admin-top-counter").click(function(event) {
        event.preventDefault();
        var status = jQuery(this).data('status');
        jQuery("#input-status").val(status);
        jQuery("#input-page-number").val(1);
        // status filter will clear search
        jQuery("#input-search").val("");
        reloadGrid();
    });

    jQuery('#btn-search').click(function(event) {
        event.preventDefault();
        // search for all status so clear status filter
        jQuery("#input-status").val("");
        jQuery("#input-page-number").val(1);
        reloadGrid();
    });

    jQuery("#btn-import").click(function(event) {
        event.preventDefault();

        utilsModule().displayLoadingModal();
        jQuery.ajax({
            type: 'post',
            url: baseUrl + 'admin/top/import',
            headers: {'X-CSRF-TOKEN': csrfToken},
            data: {}
        })
        .done(function (response) {
            utilsModule().hideLoadingModal();
            if (response.success) {
                console.log(response);
                reloadGrid();
            } else {
                swal({
                    title: "更新処理に失敗しました。",
                    text: response.error,
                    buttons: {
                        cancel: "OK",
                    },
                    className: "swal-360"
                });
                return;
            }
        });
    });

    jQuery("#admin-top-items").on("change", "#mass-item-select", function() {
        if (jQuery(this).is(':checked')) {
            jQuery(".item-checkbox").prop("checked", true);
        } else {
            jQuery(".item-checkbox").prop("checked", false);
        }
    });

    jQuery("#admin-top-items").on("click", ".admin-detail-link", function() {
        // use local storage to save paging paramters when head to detail page
        var pageNumber  = jQuery("#input-page-number").val();
        var status      = jQuery("#input-status").val();
        var search      = jQuery("#input-search").val();
        var sortByField = jQuery("#input-sort-field").val();
        var sortOrder   = jQuery("#input-sort-order").val();
        var data        = {
            pageNumber: pageNumber, status: status, search: search,
            sortByField: sortByField, sortOrder: sortOrder
        };
        localStorage.setItem("paging", JSON.stringify(data));
    });

    jQuery("#admin-top-items").on("click", ".tour_link", function(event) {
        event.preventDefault();
        var link = jQuery(this).prop("href");
        showPreviewPoup(link);
    });

    // enter on search input
    jQuery("#input-search").on('keyup', function (e) {
        if (e.keyCode == 13) {
            reloadGrid();
        }
    });

    // click on sort arrow icon
    jQuery("#admin-top-items").on("click", ".sortable-col", function(event) {
        event.preventDefault();
        var sortField = jQuery(this).data("col");
        var sortOrder = jQuery(this).data("sort-order");
        if (sortOrder == "DESC") {
            sortOrder = "ASC";
        } else {
            sortOrder = "DESC";
        }
        jQuery("#input-sort-field").val(sortField);
        jQuery("#input-sort-order").val(sortOrder);
        reloadGrid();
    });

});

function reloadGrid()
{
    utilsModule().displayLoadingModal();

    var pageNumber    = jQuery("#input-page-number").val();
    var sortByField   = jQuery("#input-sort-field").val();
    var sortOrder     = jQuery("#input-sort-order").val();
    var status = jQuery("#input-status").val();
    var search = jQuery("#input-search").val();

    jQuery.ajax({
        type: 'post',
        url: baseUrl + 'admin/top/grid',
        headers: {'X-CSRF-TOKEN': csrfToken},
        data: {
            pageNumber: pageNumber, sortByField: sortByField, sortOrder: sortOrder,
            status: status, search: search
        }
    })
    .done(function (response) {
        if (response.success) {
            jQuery('#admin-top-items').html(response.html);
            jQuery('.admin-top-paging').html(response.paging);
            var counts = response.counts;
            for (status in counts) {
                jQuery('#big-total-' + status).html(counts[status]);
                jQuery('#count-total-' + status).html("(" + counts[status] + ")");
            }
            var lastUpdate = response.lastUpdate;
            var parts      = lastUpdate.split(" ");
            var lastDate   = parts[0];
            var lastTime   = parts[1];
            jQuery("#last-update-date").html(lastDate);
            jQuery("#last-update-time").html(lastTime);
            
            utilsModule().hideLoadingModal();
        } else {
            swal(response.error, {
                buttons: {
                    cancel: "OK",
                },
            });
            return;
        }
    });
}

function massAction(status, itemIds)
{
    jQuery.ajax({
        type: 'post',
        url: baseUrl + 'admin/top/massUpdate',
        headers: {'X-CSRF-TOKEN': csrfToken},
        data: {
            itemIds: itemIds,  status: status
        }
    })
    .done(function (response) {
        utilsModule().hideLoadingModal();
        if (response.success) {
            jQuery("#input-page-number").val(1);
            reloadGrid();
        } else {
            swal(response.error, {
                buttons: {
                    cancel: "OK",
                },
            });
            return;
        }
    });
}

function showPreviewPoup(link)
{
    var wrapper = document.createElement('div');
    var innerHTML = "<div class='iframe-wrapper' " +
        "style=''>" +
            "<iframe style='" +
                "width:1100px;height:1000px' " +
                "src='" + link + "'>" +
            "</iframe>" +
        "</div>";
    wrapper.innerHTML = innerHTML;

    swal({
        title: "プレビュー",
        content: wrapper,
        className: 'item-preview-popup',
        onClose: onClosePreview
    }).then(function() {
        onClosePreview();
    });

    jQuery(".swal-overlay").scrollTop(0);
}

function onClosePreview()
{
    setTimeout(function() {
        jQuery('.swal-overlay').remove();
    }, 50);   
}