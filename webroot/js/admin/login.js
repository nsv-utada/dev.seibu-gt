jQuery(document).ready(function() {
    // this key is set on admin.js which is called on admin pages
    if (localStorage.getItem('admin_logged_in')) {
        localStorage.removeItem('admin_logged_in');
        jQuery('#logout-message').show();
    }
});