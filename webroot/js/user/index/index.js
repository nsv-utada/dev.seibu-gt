var tab1_date_depdted = '';
var tab1_purpose = ''
var tab1_destination = '';
var tab1_platform = '';
var tab1_Depdted = '';
var tab1_TourBus = '';
var tab1_Price_From = 10000;
var tab1_Price_To = 30000;
var tab1_purpose_title = '';
var tab1_destination_title = '';
var tab1_platform_title = '';
var tab1_search_key = '';
var tab1_date_from = '';
var tab1_date_from_title = '';
var tab1_date_to = '';
var tab1_date_to_title = ''
var tab1_dates = [];
var tab1_Depdted_title = '';
var tab1_month = '';

var tab2_date_depdted = '';
var tab2_purpose = ''
var tab2_destination = '';
var tab2_platform = '';
var tab2_Depdted = ''
var tab2_TourBus = '';
var tab2_Price_From = 10000;
var tab2_Price_To = 30000;
var tab2_purpose_title = '';
var tab2_destination_title = '';
var tab2_platform_title= '';
var tab2_search_key = '';
var tab2_month = '';
var tab2_Depdted_title = '';

var tab = '';
var tab1 = [];
var tab2 = [];
var value_slider = [10000, 30000 ];
var _currentYearDate =new Date().getFullYear();
var _Defaultmonth1 = 0;
var _Defaultmonth2 = 0;
var page_show = 1
var direction = "asc";
var sort_name = '';
var pagination = '';
var pagi = '';
var holidays = [];
   
var unavailableDates1 = [];
var unavailableDates2 = [];    
    
$(function () {
    $("body").click(function() {
        $(".pulldown").removeAttr('style');
                
    });
//     $('body').click(function(e) {

//     var target = $(e.target), article;

//     if (target.is('#news_gallery li .over')) {
//        article = $('#news-article .news-article');
//     } else if (target.is('#work_gallery li .over')) {
//        article = $('#work-article .work-article');
//     } else if (target.is('#search-item li')) {
//        article = $('#search-item .search-article');
//     }

//     if (article) {
//        // Do Something
//     }
// });​
    getHoliday();
    if(window.history.state !='' && window.history.state !=null){
        tab1 = window.history.state
        page_show = tab1['state']
        $('#count').val(tab1['itemsCount']) 
        $('#page-size').val(tab1['pageSize'])
        $('.resultsH201 .span02').html(tab1['itemsCount']);
         pagination  = tab1['pagination'] 
         $('.pagination').html(pagination.replace(/\\/g, ""))
         tab_depdted_title = tab1['tab_depdted_title'];
         tab1_Depdted_title =  tab1['tab_depdted_title'];
        setvalueForSubmit(tab1)
        initData();
        settabValue(1);
        setTimeout(function(){                  
        }, 500);
    }

    loadPage = function () {
        pagi = new Pagination('#tablePaging', {
            itemsCount: $('#count').val(),
            pageSize:$('#page-size').val(),
            pageRange: false,         
            currentPage:   page_show,
            onPageSizeChange: function (ps) {
            },
            onPageChange: function (paging) {
               if(window.history.state != null){
                   page_show = window.history.state['state']
                   pagination  =  window.history.state['pagination'] 
                    showPage(page_show);  
                    $('.pagination').html(pagination.replace(/\\/g, ""))     
                    window.history.pushState(null,"", '')
                }else{
                    showPage(paging.currentPage);  
                    page_show = paging.currentPage;
                }                   

                $(".page-number[data-page='"+(page_show)+"']").addClass('active')
                
                $('.page-range').hide();
            }
        });
    }
    if ($('#tablePaging').length > 0) {
        loadPage();
    }
    $('#slider-range').attr('style','pointer-events: none;opacity:0.5')
    $('#slider-range02').attr('style','pointer-events: none;opacity:0.5')
    
});

function getHoliday(){
    flickerAPI = "https://holidays-jp.github.io/api/v1/date.json";
    $.getJSON( flickerAPI, {
        tags: "mount rainier",
        tagmode: "any",
        format: "json"
    })
    .done(function( data ) {
        $.each( data, function( key, val ) {
            holidays.push(key)
        });
    });
}
function getDepdtDate(tab){
    if(tab == 1){
        settabValue(1)
        $(".tabNaviAcCont01").slideUp();
    }else{
        settabValue(2)
    }    
    
    var params = $('#ApForm1').serializeArray();    

    $.ajax({
        method: 'POST',
        url: rootUrl + 'tour/getDepdtDate',
        data: params
    })
    .done(function (msg) {
        if(tab == 1){
             unavailableDates1= []
            $.each($.parseJSON(msg), function( key, val ) {
                unavailableDates1.push(val['value'])
            });
        }else{
             unavailableDates2= []
            $.each( $.parseJSON(msg), function( key, val ) {
                unavailableDates2.push(val['value'])
            });
        } 
       
    })
}

function showPage(page) {
   // window.history.pushState(page,"", rootUrl + 'tour/main01');
    $(".loading-spinner").css("display", "block");
    var url = rootUrl + 'tour/main01?page='+page;
    var params = $('#ApForm1').serializeArray();
    $.ajax({
        method: 'POST',
        url: url,
        data: params
    })
    .done(function (msg) {
        $(".resultsListWrap").html('');
        $(".resultsListWrap").html(msg);
        $(".loading-spinner").css("display", "none");
        $('.detail_link').attr('state', page);
        $("html, body").animate({ scrollTop: 0 }, 10);
    });

    

}

jQuery(document).ready(function() {
    if($('#submit_name').val() != ''){
        initData();        
    }
    getDepdtDate(1)
    $(document).on('click', '#tab1 .radioWrap02 .searchInputWrap01 label', function() {
        $('#tab1 .radioWrap01 .searchInputWrap01 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab1 .radioWrap02 .text02").text(t);
        tab1_purpose = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab1_purpose_title = t
        getDepdtDate(1)
    });

    $(document).on('click', '#tab2 .radioWrap02 .searchInputWrap01 label', function() {
        $('#tab2 .radioWrap02 .searchInputWrap01 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab2 .radioWrap02 .text02").text(t);
        tab2_purpose = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab2_purpose_title = t;
         getDepdtDate(2)
         $(".tabNaviAcCont01").slideUp();
    });   
    $(document).on('click', '#tab1 .radioWrap01 .searchInputWrap01 label', function() {
        $('#tab1 .radioWrap01 .searchInputWrap01 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab1 .radioWrap01 .text02").text(t);
        tab1_destination = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab1_destination_title = t;
         getDepdtDate(1)
    }); 
     $(document).on('click', '#boxregion1 label', function() {
        $('#boxregion1 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab1 .radioWrap01 .text02").text(t);
        tab1_destination = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab1_destination_title = t;
         getDepdtDate(1)
    }); 
    $(document).on('click', '.regionTab1', function() {
        
        InitCityByRegion(1, $(this).attr('regionId'), $(this).attr('regionText'))

    })
   

    $(document).on('click', '#tab2 .radioWrap01 .searchInputWrap01 label', function() {
        $('#tab2 .radioWrap01 .searchInputWrap01 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab2 .radioWrap01 .text02").text(t);
        tab2_destination = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab2_destination_title = t;
         getDepdtDate(2)
         $(".tabNaviAcCont01").slideUp();
    });

    $(document).on('click', '#boxregion2 label', function() {
        $('#boxregion2 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab2 .radioWrap01 .text02").text(t);
        tab2_destination = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab2_destination_title = t;
         getDepdtDate(2)
         $(".tabNaviAcCont01").slideUp();
    }); 
    $(document).on('click', '.regionTab2', function() {
        InitCityByRegion(2, $(this).attr('regionId'),$(this).attr('regionText'))


    })
    $(document).on('click', '#boxRoute1 label', function() {
        $('#boxRoute1 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab1 .radioWrap03 .text02").text(t);
        tab1_platform = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab1_platform_title = t; 
         getDepdtDate(1)
    }); 
    $(document).on('click', '#boxPlatform1 label', function() {
        $('#tab1 .radioWrap01 .searchInputWrap03 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab1 .radioWrap03 .text02").text(t);
        tab1_platform = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab1_platform_title = t;
         getDepdtDate(1)
    });
    $(document).on('click', '.platformTab1', function() {
        InitPlatformByRoute(1, $(this).attr('value'))

    })

    $(document).on('click', '#boxPlatform2 label', function() {
        $('#tab2 .radioWrap01 .searchInputWrap03 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab2 .radioWrap03 .text02").text(t);
        tab2_platform = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab2_platform_title = t;
         getDepdtDate(2)
         $(".tabNaviAcCont01").slideUp();
    });
    $(document).on('click', '#boxRoute2 label', function() {
        $('#boxRoute2 label').removeClass('over');
        $(this).addClass('over');
        var t = $(this).find("input").attr('data-title');
        $("#tab2 .radioWrap03 .text02").text(t);
        tab2_platform = $(this).find("input").attr('value');
        t = (t =='指定しない')? '':$(this).find("input").attr('data-title');
        tab2_platform_title = t; 
         getDepdtDate(1)
    }); 
    $(document).on('click', '.platformTab2', function() {
        InitPlatformByRoute(2, $(this).attr('value'))

    })
    //出発決定日あり
    $( 'input[name="tab1_isDepdted"]').on('click', function() {
        if($('input[name="tab1_isDepdted"]').is(":checked") === true){
            tab1_Depdted = 1;
        }else{
            tab1_Depdted = 0;
        }
         getDepdtDate(1)
    })
    $( 'input[name="tab2_isDepdted"]').on('click', function() {        
        if($('input[name="tab2_isDepdted"]').is(":checked") === true){
            tab2_Depdted = 1;
        }else{
            tab2_Depdted = 0;
        }
         getDepdtDate(2)
         $('.tabNaviAcCont02').attr('style',"display: block;")
    })
    $( 'input[name="show_price2"]').on('click', function() {        
        if($('input[name="show_price2"]').is(":checked") === true){
            $('#slider-range02').removeAttr('style');
            $('.tabNaviAcCont02').attr('style',"display: block;")
        }else{
            $('#slider-range02').attr('style','pointer-events: none;opacity:0.5');            
        }
        $('.tabNaviAcCont02').attr('style',"display: block;")
    })
    $( 'input[name="show_price1"]').on('click', function() {        
        if($('input[name="show_price1"]').is(":checked") === true){
            $('#slider-range').removeAttr('style')
        }else{
            $('#slider-range').attr('style','pointer-events: none;opacity:0.5');
        }
    })
    //添乗員・ガイド同行
    $( 'input[name="tab1_isTourBus"]').on('click', function() {
        if($('input[name="tab1_isTourBus"]').is(":checked") === true){
            tab1_TourBus = 1
        }else{
            tab1_TourBus = 0
        }
         getDepdtDate(1)

    })
    $( 'input[name="tab2_isTourBus"]').on('click', function() {        
        if($('input[name="tab2_isTourBus"]').is(":checked") === true){
            tab2_TourBus = 1
        }else{
            tab2_TourBus = 0
        }
         getDepdtDate(2)
         $('.tabNaviAcCont02').attr('style',"display: block;")
    })
    //datetime
    $( "#datepicker").on("change", function() {
        tab1_date_depdted = $('#datepicker').val()
    })
    $( "#datepicker02").on("change", function() {
        tab2_date_depdted = $('#datepicker02').val()
    })
    //key word
    $( "#tab1_keyword").on("change", function() {
        tab1_search_key = $('#tab1_keyword').val()
         getDepdtDate(1)
    })
    $( "#tab2_keyword").on("change", function() {
        tab2_search_key = $('#tab2_keyword').val()
         getDepdtDate(2)
    })
    //日数
    $('.resultsCell02 .radioWrap01 .searchInputWrap01 label').click(function(e){
        tab1_date_from = $(this).find("input").attr('value');
        tab1_date_from_title = $(this).find("input").attr('data-title');
        if(parseInt(tab1_date_from) > parseInt(tab1_date_to)) {
            e.stopImmediatePropagation();
            e.preventDefault();
            alert("上限値が下限値より小さくなるような選択はできません");
            tab1_date_from = '';
            tab1_date_from_title = '';
            $(".resultsCell02 .radioWrap01 .text02").text('指定しない');
            return false;
        }
        tab1_dates = generateDate(tab1_date_from, tab1_date_to)
         getDepdtDate(1)
    })

    $('.resultsCell02 .radioWrap02 .searchInputWrap01 label').click(function(e){
        tab1_date_to = ($(this).find("input").attr('value') != '0')?$(this).find("input").attr('value'):30;
        tab1_date_to_title = $(this).find("input").attr('data-title');
        if(parseInt(tab1_date_from) > parseInt(tab1_date_to)) {
            e.stopImmediatePropagation();
            e.preventDefault();
            alert("上限値が下限値より小さくなるような選択はできません");
            tab1_date_to = '';
            tab1_date_to_title = '';
            $(".resultsCell02 .radioWrap02 .text02").text('指定しない');
            return false;
        }
        tab1_dates =  generateDate(tab1_date_from, tab1_date_to)
         getDepdtDate(1)
    })

    $(document).on('click', '#submit1', function (e) {
        e.preventDefault()
        settabValue(1)
        $('#sort').val("asc")
        url = getStringUrl(tab1)
        window.location.href = (url);
    })
    $(document).on('click', '#submit2', function (e) {
        e.preventDefault()
        settabValue(2)
        $('#sort').val("asc")
        url = getStringUrl(tab2)
        window.location.href = (url);
    })
    $(document).on('click', '.sort_button', function (e) {
        e.preventDefault()
        sort = $(this).attr('attr');
        $('#sort').val(sort)
        settabValue(1)
        url = getStringUrl(tab1)
        window.location.href = (url);
    })
     $( "#tab1_keyword" ).on('keydown', function(e) {
        if (e.keyCode == 13) {
            tab1_search_key =  $(this).val()
            settabValue(1)
            $('#sort').val("asc")
            url = getStringUrl(tab1)
            window.location.href = (url);
        }
    })
     $( "#tab2_keyword" ).on('keydown', function(e) {
        if (e.keyCode == 13) {
            tab2_search_key =  $(this).val()
            settabValue(2)
            $('#sort').val("asc")
            url = getStringUrl(tab2)
            window.location.href = (url);
        }
    })
    $(document).on('click', '.detail_link', function (e) {       
        state = $(this).attr('state');
        href = $(this).attr('fref');
        settabValue(1)
        tab1['tab_depdted_title'] = tab1_Depdted_title;
        tab1['state'] = state;
        tab1['itemsCount'] = $('#count').val();
        tab1['pageSize']  =$('#page-size').val();
        pagination =  ($('.pagination').html());
        pagination.replace('"', "'");
        tab1['pagination'] = pagination;    
        current_url = document.URL;    
        window.history.pushState(tab1,"Detail",current_url)
    })
    $("#datepicker").datepicker({
        showButtonPanel: true,
        defaultDate: '+'+_Defaultmonth1+'m',
        beforeShow: function (input, date) {
            setTimeout(function () {

                var buttonPane = $(input)
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");
                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">'+(parseInt(date.selectedMonth) + 1)+'月未定</button>');
                btn
                .unbind("click")
                .bind("click", function () {
                 setFullMonthSeletedDate(date.selectedMonth + 1,"datepicker",date.selectedYear);
             });

                btn.appendTo(buttonPane);
            }, 1);
             getDepdtDate(1)
        },
        onChangeMonthYear: function (year, month, inst) {
            setTimeout(function () {
                var buttonPane = $(inst)
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");

                _currentYearDate = year;
                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">' + month + '月未定</button>');
                btn
                .unbind("click")
                .bind("click", function () {
                    setFullMonthSeletedDate(month,"datepicker",year)
                });
                btn.appendTo(buttonPane);
            }, 1);
        },
        onSelect: function (dateText) {
            tab1_date_depdted = dateText;
            tab1_date_depdted_from = '';        
            tab1_date_depdted_to =  '';
            tab1_Depdted_title = '';
            $(".ui-datepicker").hide();
        },
        beforeShowDay: function highLight(date) {    
            check = false;         
                $.each( unavailableDates1, function( key1, val ) {
                    month = new Date(val).getMonth()
                    dates = new Date(val).getDate()
                    year = new Date(val).getFullYear()
                    
                    if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                        check = true;

                    }
                })
                if(check){
                     for (var i = 0; i < holidays.length; i++) {
                        
                        month = new Date(holidays[i]).getMonth()
                        dates = new Date(holidays[i]).getDate()
                        year = new Date(holidays[i]).getFullYear()
                        if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                            return [true, 'ui-state-holiday'];
                        }
                    }
                    return [true, ''];
                }else{
                     for (var i = 0; i < holidays.length; i++) {
                        
                        month = new Date(holidays[i]).getMonth()
                        dates = new Date(holidays[i]).getDate()
                        year = new Date(holidays[i]).getFullYear()
                        if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                            
                            return [false, 'ui-state-holiday'];

                        }
                    }
                    return [false, "", "Unavailable"];
                }         
                return [false, "", "Unavailable"];
           
        }

    });



    $("#datepicker02").datepicker({
        showButtonPanel: true,
        defaultDate: '+'+_Defaultmonth2+'m',
        beforeShow: function (input, date) {
            setTimeout(function () {

                var buttonPane = $(input)
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");
                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">'+(parseInt(date.selectedMonth) + 1)+'月未定</button>');
                btn
                .unbind("click")
                .bind("click", function () {
                 setFullMonthSeletedDate(date.selectedMonth + 1,"datepicker02",date.selectedYear );
             });

                btn.appendTo(buttonPane);
            }, 1);
             getDepdtDate(2)
        },
        onChangeMonthYear: function (year, month, inst) {
            setTimeout(function () {
                var buttonPane = $(inst)
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");

                _currentYearDate = year;
                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">' + month + '月未定</button>');
                btn
                .unbind("click")
                .bind("click", function () {

                 setFullMonthSeletedDate(month,"datepicker02",year)
             });
                btn.appendTo(buttonPane);
            }, 1);
        },
        onSelect: function (dateText) {
            tab2_date_depdted = dateText;
            tab2_date_depdted_from = '';        
            tab2_date_depdted_to =  '';
            tab2_Depdted_title = '';
            $('.ui-datepicker').hide()
        },
        beforeShowDay: function highLight(date) {
            check = false;         
            $.each( unavailableDates2, function( key1, val ) {
                month = new Date(val).getMonth()
                dates = new Date(val).getDate()
                year = new Date(val).getFullYear()
                
                if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                    check = true;

                }
            })
            if(check){
                 for (var i = 0; i < holidays.length; i++) {
                    
                    month = new Date(holidays[i]).getMonth()
                    dates = new Date(holidays[i]).getDate()
                    year = new Date(holidays[i]).getFullYear()
                    if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                        return [true, 'ui-state-holiday'];
                    }
                }
                return [true, ''];
            }else{
                 for (var i = 0; i < holidays.length; i++) {
                    
                    month = new Date(holidays[i]).getMonth()
                    dates = new Date(holidays[i]).getDate()
                    year = new Date(holidays[i]).getFullYear()
                    if(month ==(date.getMonth()) && dates == parseInt(date.getDate())&& year == parseInt(date.getFullYear()) ){
                        
                        return [false, 'ui-state-holiday'];

                    }
                }
                return [false, "", "Unavailable"];
            }         
            return [false, "", "Unavailable"];
        }
    });
    var old_goToToday = $.datepicker._gotoToday
    $.datepicker._gotoToday = function (id) {
        $(id).val('');
        $(id).attr("placeholder", "日付未定");
        if(id == '#datepicker'){
            tab1_date_depdted_from = '';        
            tab1_date_depdted_to =  '';
            tab1_Depdted_title = '';
            tab1_date_depdted = '';
        }else{
            tab2_date_depdted_from = '';        
            tab2_date_depdted_to =  '';
            tab2_Depdted_title = '';
            tab2_date_depdted = '';
        }
        $(".ui-datepicker").hide();
    }


     $("#slider-range02").click(function() {
        $('.tabNaviAcCont02').attr('style',"display: block;")
    });
        
    $("#clear_tab").click(function() {
        $('.tabNaviAcCont02').attr('style',"display: block;")
    });  
});

$(function() {

    $( "#slider-range" ).slider({
        range: true,
        min: 1000,
        max: 50000,
        step: 1000,
        values: value_slider,
        slide: function( event, ui ) {

            var ijyou01 = '';
            if(ui.values[ 1 ] >= 50000){
                ijyou01 = '以上';
            }else{
                ijyou01 = '';
            }
            tab1_Price_From = ui.values[0];
            tab1_Price_To = ui.values[1]
              $('.tabNaviAcCont01').hide()
            $( "#amount" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou01 );

        }
    });
    
    $( "#slider-range02" ).slider({

        range: true,
        min: 1000,
        max: 50000,
        step: 1000,
        values: value_slider,
        slide: function( event, ui ) {

            var ijyou02 = '';
            if(ui.values[ 1 ] >= 50000){
                ijyou02 = '以上';
            }else{
                ijyou02 = '';
            }
            tab2_Price_From = ui.values[0];
            tab2_Price_To = ui.values[1]
            $('.tabNaviAcCont01').hide()
            $( "#amount02" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou02 );
            $('.tabNaviAcCont02').attr('style',"display: block;")
        }
    });
    

});
function setvalueForSubmit(tab){
    $.each(tab, function( classSelector, value ) {
        $('#'+classSelector).val(value)
    });
}
function getStringUrl(tab){
    string_url = '';
    $.each(tab, function( classSelector, value ) {
        if(value !='') string_url += "&"+classSelector+'='+value
    });
    if(string_url !='') string_url ="?"+string_url;
    url = rootUrl + "tour/main01"+string_url;
    return url;
}
function initData(){

    if($('#tab_date_depdted').val() !='' ){
        $('input[name="tab1_datedepdted"]').val($('#tab_date_depdted').val());
        tab1_date_depdted = $('#tab_date_depdted').val();

    }
    if($('#tab_purpose').val() !='' ){
        $("#tab1 .radioWrap02 .text02").text($('#tab_purpose_title').val());
        tab1_purpose = $('#tab_purpose').val();
        tab1_purpose_title = $('#tab_purpose_title').val();
    }
    if($('#tab_destination').val() !='' ){
        $("#tab1 .radioWrap01 .text02").text($('#tab_destination_title').val());
        tab1_destination = $('#tab_destination').val();
        tab1_destination_title = $('#tab_destination_title').val();
    }
    if($('#tab_platform').val() !='' ){
        $("#tab1 .radioWrap03 .text02").text($('#tab_platform_title').val());
        tab1_platform = $('#tab_platform').val();
        tab1_platform_title = $('#tab_platform_title').val()
    }
    if($('#tab_price_from').val() !='' && $('#tab_price_to').val() !=''){
        value_slider = [parseInt($('#tab_price_from').val()), parseInt($('#tab_price_to').val())]
        ijyou01 = '';
        if(parseInt($('#tab_price_to').val()) >= 50000){
            ijyou01 = '以上';
        }


        tab1_Price_From = parseInt($('#tab_price_from').val());
        tab1_Price_To =  parseInt($('#tab_price_to').val())
        $( "#slider-range" ).slider({
            range: true,
            min: 1000,
            max: 50000,
            step: 1000,
            values: [tab1_Price_From,tab1_Price_To],
            slide: function( event, ui ) {

                var ijyou01 = '';
                if(ui.values[ 1 ] >= 50000){
                    ijyou01 = '以上';
                }else{
                    ijyou01 = '';
                }
                tab1_Price_From = ui.values[0];
                tab1_Price_To = ui.values[1];
                $('.tabNaviAcCont01').hide()
                $( "#amount" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou01 );

            }
        });
        $( "#amount").text( "" + tab1_Price_From+ "円　 ～ 　" +tab1_Price_To+ "円" + ijyou01 );
       $('#show_price1').prop('checked', true);
       $('#slider-range').removeAttr('style');
    }else if($('#tab_price_from').val() !='' ){
        value_slider = [parseInt($('#tab_price_from').val()),50000]
        ijyou01 = '以上';



        tab1_Price_From = parseInt($('#tab_price_from').val());
        tab1_Price_To = 50000
        $( "#slider-range" ).slider({
            range: true,
            min: 1000,
            max: 50000,
            step: 1000,
            values: [tab1_Price_From,tab1_Price_To],
            slide: function( event, ui ) {

                var ijyou01 = '';
                if(ui.values[ 1 ] >= 50000){
                    ijyou01 = '以上';
                }else{
                    ijyou01 = '';
                }
                tab1_Price_From = ui.values[0];
                tab1_Price_To = ui.values[1];
                $('.tabNaviAcCont01').hide()
                $( "#amount" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou01 );

            }
        });
        $( "#amount").text( "" + tab1_Price_From+ "円　 ～ 　" +tab1_Price_To+ "円" + ijyou01 );
        $('#show_price1').prop('checked', true);
        $('#slider-range').removeAttr('style');
    }else if($('#tab_price_to').val() !='' ){
        value_slider = [1000,parseInt($('#tab_price_to').val())]
        ijyou01 = '';
        if(parseInt($('#tab_price_to').val()) >= 50000){
            ijyou01 = '以上';
        }
        tab1_Price_From = 1000;
        tab1_Price_To =  parseInt($('#tab_price_to').val());
        $( "#slider-range" ).slider({
            range: true,
            min: 1000,
            max: 50000,
            step: 1000,
            values: [tab1_Price_From,tab1_Price_To],
            slide: function( event, ui ) {

                var ijyou01 = '';
                if(ui.values[ 1 ] >= 50000){
                    ijyou01 = '以上';
                }else{
                    ijyou01 = '';
                }
                tab1_Price_From = ui.values[0];
                tab1_Price_To = ui.values[1];
                $('.tabNaviAcCont01').hide()
                $( "#amount" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou01 );

            }
        });
        $( "#amount").text( "" + tab1_Price_From+ "円　 ～ 　" +tab1_Price_To+ "円" + ijyou01 );
        $('#show_price1').prop('checked', true);
        $('#slider-range').removeAttr('style');
    }
    
    if($('#tab_depdted').val() !='' ){
        if($('#tab_depdted').val() == "1") $('.tab1_isDepdted').prop('checked', true);
        tab1_Depdted = $('#tab_depdted').val();
    }
    if($('#tab_tourbus').val() !='' ){
        if($('#tab_tourbus').val() == "1") $('.tab1_isTourBus').prop('checked', true);
        tab1_TourBus = $('#tab_tourbus').val()
    }
    if($('#tab_search_key').val() !='' ){
        $('input[name="tab1_keyword"]').val($('#tab_search_key').val())
        tab1_search_key = $('#tab_search_key').val();
    }
    if($('#tab_date_from_title').val() !='' ){
        $(".resultsCell02 .radioWrap01 .text02").text($('#tab_date_from_title').val());
        tab1_date_from = $('#tab_date_from').val();
        tab1_date_from_title = $('#tab_date_from_title').val();
    }
    if($('#tab_date_to_title').val() !='' ){
        $(".resultsCell02 .radioWrap02 .text02").text($('#tab_date_to_title').val());
        tab1_date_to = $('#tab_date_to').val();
        tab1_date_to_title = $('#tab_date_to_title').val()
    }
    if($('#tab_depdted_to').val() !='' ){        
        tab1_date_depdted_to = $('#tab_depdted_to').val();
        //$('input[name="tab1_datedepdted"]').val( tab1_date_depdted_to);
    }
    if($('#tab_depdted_from').val() !='' ){        
        tab1_date_depdted_from = $('#tab_depdted_from').val();
        //$('input[name="tab1_datedepdted"]').val( tab1_date_depdted_from);
    }

    if($('#tab_depdted_title').val() !='' ){   
        tab1_Depdted_title =  $('#tab_depdted_title').val();

        month = new Date(tab1_date_depdted_from).getMonth();
        tab1_date_depdted =new Date(tab1_date_depdted_from).getFullYear()+'/'+ (parseInt(new Date(tab1_date_depdted_from).getMonth())+1)
        _Defaultmonth = month -  new Date().getMonth();
        tempDate = new Date(tab1_date_depdted_from);
        setTimeout(function(){
                   
                    var currentDate = new Date()
                    var currentMonth = currentDate.getMonth()
                    var currentYear = currentDate.getFullYear()
                    var monthDiff = (currentMonth) - tempDate.getMonth()
                    var yearDiff = currentYear - tempDate.getFullYear()
                    if(yearDiff != 0) {
                        if (yearDiff>0) {
                            monthDiff += 12
                        } else {
                            monthDiff -= 12
                        }
                    }
                    $('#datepicker').datepicker( "option", "defaultDate", (monthDiff*-1)+"m" )
                    $('#datepicker').datepicker( "option", "defaultDate", new Date(tempDate.getFullYear(),tempDate.getMonth(),1) )
        }, 10);
        setTimeout(function(){                  
            $('input[name="tab1_datedepdted"]').val( tab1_Depdted_title);
        }, 20);
        
    }
   
}
function generateDate(from, to){
    from = (from == '' || parseInt(from) == 0)?0:from;
    to = (to == '' || parseInt(to) == 10)? 30: to;
    var _tempDates = []
    for (var i = from; i <= to; i++) {
        _tempDates.push(parseInt(i));
    }
    tab1_dates = (_tempDates)
    return tab1_dates;

}
function setFullMonthSeletedDate(selectedMonth,id,year) {
    m = selectedMonth;
    if(id == 'datepicker'){
        // tab1_date_depdted_from =  new Date(_currentYearDate, m, 1);
        // tab1_date_depdted_from  = _currentYearDate+'/'+m+'/'+tab1_date_depdted_from.getDate();
        // tab1_date_depdted_to =  new Date(_currentYearDate, m + 1, 0);
        // tab1_date_depdted_to  = _currentYearDate+'/'+m+'/'+tab1_date_depdted_to.getDate();
        //tab1_month = year+'/'+selectedMonth;
        tab1_Depdted_title = selectedMonth + "月未定";        
        tab1_date_depdted =  year+'/'+selectedMonth;
        $('#datepicker').datepicker("option", "defaultDate",new Date(_currentYearDate, m,1));
            var currentDate = new Date()
            var currentMonth = currentDate.getMonth()
            var currentYear = currentDate.getFullYear()
            var monthDiff = (currentMonth+1) - selectedMonth
            var yearDiff = currentYear - year
            if(yearDiff != 0) {
                if (yearDiff>0) {
                    monthDiff += 12
                } else {
                    monthDiff -= 12
                }
        }
        $('#datepicker').datepicker( "option", "defaultDate", new Date(year, m-1,1) )
    }else{
        // tab2_date_depdted_from =  new Date(_currentYearDate, m, 2);
        // tab2_date_depdted_from  = _currentYearDate+'/'+m+'/'+tab2_date_depdted_from.getDate();
        // tab2_date_depdted_to =  new Date(_currentYearDate, m + 2, 0);
        // tab2_date_depdted_to  = _currentYearDate+'/'+m+'/'+tab2_date_depdted_to.getDate();
        // tab2_month =year+'/'+selectedMonth;
        tab2_Depdted_title = selectedMonth + "月未定";        
        tab2_date_depdted =  year+'/'+selectedMonth;
        $('#datepicker02').datepicker("option", "defaultDate",new Date(_currentYearDate, m,1));
            var currentDate = new Date()
            var currentMonth = currentDate.getMonth()
            var currentYear = currentDate.getFullYear()
            var monthDiff = (currentMonth+1) - selectedMonth
            var yearDiff = currentYear - year
            if(yearDiff != 0) {
                if (yearDiff>0) {
                    monthDiff += 12
                } else {
                    monthDiff -= 12
                }
        }
        $('#datepicker02').datepicker( "option", "defaultDate", new Date(year, m-1,1) )
    }  


    $(".ui-datepicker").hide();
    $("#"+id).val(selectedMonth + "月未定");
    
}
function settabValue(tab){

    if(tab == 1){
        if(tab1_date_from != '' || tab1_date_to != ''){
            generateDate(tab1_date_from,tab1_date_to)
        }
       
        tab1 = {
            tab_date_depdted: tab1_date_depdted,
            tab_platform: tab1_platform,
            tab_platform_title:tab1_platform_title,
            tab_destination_title: tab1_destination_title,
            tab_destination:tab1_destination,
            tab_purpose_title: tab1_purpose_title,
            tab_purpose:tab1_purpose,
            tab_depdted: tab1_Depdted,            
            tab_tourbus: tab1_TourBus,
            tab_price_from: tab1_Price_From,
            tab_price_to: tab1_Price_To,
            tab_search_key: tab1_search_key,            
            tab_date_from: tab1_date_from,
            tab_date_from_title : tab1_date_from_title,
            tab_date_to: tab1_date_to,
            tab_date_to_title : tab1_date_to_title,
            tab_dates : tab1_dates.toString(),
            tab_depdted_title : tab1_Depdted_title,
            sort: $('#sort').val(),
            action: $('#action').val(),
            submit_name: 'submit1'}
        if($('input[name="show_price1"]').is(":checked") !== true){
            tab1['tab_price_from'] = '';
            tab1['tab_price_to'] = '';            
        }
        if($('#action').val() == 'index'){
            tab1['tab_dates']  = 1;
        }
        if(tab1_dates.length >= 30){
            tab1['tab_dates'] = '';
        }else if(tab1_dates[tab1_dates.length - 1] == 30){
             tab1['tab_dates'] = '';
        }
        setvalueForSubmit(tab1)

    }else{
        tab2 = {
            tab_date_depdted: tab2_date_depdted,
            tab_platform: tab2_platform,
            tab_platform_title:tab2_platform_title,
            tab_destination:tab2_destination,
            tab_destination_title: tab2_destination_title,
            tab_purpose_title: tab2_purpose_title,
            tab_purpose:tab2_purpose,   
            tab_depdted : tab2_Depdted,         
            tab_tourbus: tab2_TourBus,
            tab_price_from: tab2_Price_From,
            tab_price_to: tab2_Price_To,
            tab_search_key: tab2_search_key,            
            tab_depdted_title : tab2_Depdted_title,
            action: $('#action').val(),
            submit_name: 'submit2'
        }
        if($('input[name="show_price2"]').is(":checked") !== true){
            tab2['tab_price_from'] = '';
            tab2['tab_price_to'] = '';
            
        }
        
        setvalueForSubmit(tab2)
    }
}
function InitPurpose(tab){
    $(".loading-spinner").css("display", "block");
    if(tab == 1){
        settabValue(1)
    }else{
        settabValue(2)
    }    
    var url = rootUrl + 'tour/getPullDownPurposeByCondition';
    var params = $('#ApForm1').serializeArray(); 

    $.ajax({
        method: 'POST',
        url: url,
        data: params
    })
    .done(function (msg) {
        list_purpose = $.parseJSON(msg);
        html = '<li class="li01"><label class="over"><input type="radio" name="tab1_cmsPurpose" value=""  data-title="指定しない" >指定しない</label> </li>';
        
        for (var i = 0; i <= list_purpose.length - 1; i++) {
            if(list_purpose[i]['count'] == 0){
                    list_purpose[i]['count'] = ''; 
                    html+= '<li><label style="opacity:0.5; pointer-events: none "><input disable type="radio" name="tab1_cmsPurpose"  value="'+list_purpose[i]['key']+'" class="Tour_purpose" data-title="'+list_purpose[i]['value']+'">'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>';
                }else{
                     list_purpose[i]['count'] = '（'+list_purpose[i]['count']+'）';
                     html+= '<li><label><input type="radio" name="tab1_cmsPurpose"  value="'+list_purpose[i]['key']+'" class="Tour_purpose" data-title="'+list_purpose[i]['value']+'">'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>';
                }   
            }
        if(tab == 1){

            $('#tab1 .radioWrap02 .searchInputWrap01').html('');
            $('#tab1 .radioWrap02 .searchInputWrap01').html(html);
        }else{

            $('#tab2 .radioWrap02 .searchInputWrap01').html('');
            $('#tab2 .radioWrap02 .searchInputWrap01').html(html);
        }
        setTimeout(function(){ $(".loading-spinner").css("display", "none"); }, 300);
    });

}
function InitRegion(tab){
    $(".loading-spinner").css("display", "block");
     class_tab= 'regionTab1';
     $('#tab1 .radioWrap01 .searchInputWrap01').html('');
     $('#tab2 .radioWrap01 .searchInputWrap01').html('');
     boxregion = 'boxregion1'
   if(tab == 1){
        settabValue(1)
    }else{
        class_tab = 'regionTab2';
        boxregion = 'boxregion2';
        settabValue(2)
    }    
    var url = rootUrl + 'tour/getPullDownRegionByCondition';
    var params = $('#ApForm1').serializeArray();   
    $.ajax({
        method: 'POST',
        url: url,
        data: params
    })
    .done(function (msg) {
        list_purpose = $.parseJSON(msg);
        html = '<li class="li01"><label class="over"><input type="radio" name="tab1_cmsCity" class="Tour_destination" value="" data-title="指定しない">指定しない</label></li>';
        for (var i = 0; i <= list_purpose.length - 1; i++) {
            if(list_purpose[i]['key'] == 1 || list_purpose[i]['key'] == 11) {       
                if(list_purpose[i]['count'] == 0){
                    list_purpose[i]['count'] = ''; 
                    html +='<li><label style="opacity:0.5; pointer-events:none"><input type="radio" name="tab2_cmsCity" class="Tour_destination" value="'+list_purpose[i]['value']+'" data-title="'+list_purpose[i]['value']+'">'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>';
            
                }else{
                    list_purpose[i]['count'] =   '（'+list_purpose[i]['count']+'）';
                    html +='<li><label><input type="radio" name="tab2_cmsCity" class="Tour_destination" value="'+list_purpose[i]['value']+'" data-title="'+list_purpose[i]['value']+'">'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>';
            
                }         
            }else {
                if(list_purpose[i]['count'] == 0){
                  list_purpose[i]['count'] = ''; 
                  html += '<li style="opacity:0.5; pointer-events:none"><a class="modal noSmoothScroll '+class_tab+'" regionText="'+ list_purpose[i]['value']+'" regionId="'+list_purpose[i]['key']+'" href="#'+boxregion+'"><p>'+list_purpose[i]['value']+list_purpose[i]['count']+' ></p></a></li>';
            
                }else{
                     list_purpose[i]['count'] =   '（'+list_purpose[i]['count']+'）';
                     html += '<li  ><a class="modal noSmoothScroll '+class_tab+'" regionText="'+ list_purpose[i]['value']+'" regionId="'+list_purpose[i]['key']+'" href="#'+boxregion+'"><p>'+list_purpose[i]['value']+list_purpose[i]['count']+' ></p></a></li>';
            
                }
            }
        }
        if(tab == 1){
            $('#tab1 .radioWrap01 .searchInputWrap01').html('');
            $('#tab1 .radioWrap01 .searchInputWrap01').html(html);
        }else{
            $('#tab2 .radioWrap01 .searchInputWrap01').html('');
            $('#tab2 .radioWrap01 .searchInputWrap01').html(html);
        }
        setTimeout(function(){ $(".loading-spinner").css("display", "none"); }, 300);
    });

}
function InitCityByRegion(tab,region_id, region_text){
    $(".loading-spinner").css("display", "block");
    if(tab == 1){
        settabValue(1)
    }else{
        settabValue(2)
    }    
    var url = rootUrl + 'tour/getPullDownCityByCondition';
    var params = $('#ApForm1').serializeArray();    
    params.push({name: 'region_id', value: region_id});
    $.ajax({
        method: 'POST',
        url: url,
        data: params,
    })
    .done(function (msg) {
        list_purpose = $.parseJSON(msg);
        html = '';        
        for (var i = 0; i <= list_purpose.length - 1; i++) {    
            if(list_purpose[i]['count'] == 0){
              list_purpose[i]['count'] = ''; 
              html += '<li><label style="opacity:0.5; pointer-events:none"><input type="radio" name="tab1_cmsCity" class="Tour_destination" value="'+list_purpose[i]['value']+'" data-title="'+list_purpose[i]['value']+'">・'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>'
            }else{
                 list_purpose[i]['count'] =   '（'+list_purpose[i]['count']+'）';
                 html += '<li><label ><input type="radio" name="tab1_cmsCity" class="Tour_destination" value="'+list_purpose[i]['value']+'" data-title="'+list_purpose[i]['value']+'">・'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>'
            }            
        }
        if(tab == 1){
            $('#boxregion1 .modalRadioWrap').html('');
            $('#boxregion1 .clickP01').html('');
            $('#boxregion1 .modalRadioWrap').html(html);
            $('#boxregion1 .clickP01').html(region_text);
        }else{
            $('#boxregion2 .modalRadioWrap').html('');
            $('#boxregion2 .modalRadioWrap').html(html);
            $('#boxregion2 .clickP01').html('');
            $('#boxregion2 .clickP01').html(region_text);
        }
        setTimeout(function(){ $(".loading-spinner").css("display", "none"); }, 300);
    });
}

function InitPlatformByRoute(tab,route_sort){
    $('#boxPlatform1 .modalRadioWrap').html('');
    $('#boxPlatform2 .modalRadioWrap').html('');
    $(".loading-spinner").css("display", "block");
    if(tab == 1){
        settabValue(1)
    }else{
        settabValue(2)
    }    
    var url = rootUrl + 'tour/getPullDownPlatformByCondition';
    var params = $('#ApForm1').serializeArray();    
    params.push({name: 'route_sort', value: route_sort});
    $.ajax({
        method: 'POST',
        url: url,
        data: params
    })
    .done(function (msg) {
        list_purpose = $.parseJSON(msg);
        html = '';        
        for (var i = 0; i <= list_purpose.length - 1; i++) {
            if(list_purpose[i]['count'] == 0){
              list_purpose[i]['count'] = ''; 
              html += '<li><label style="opacity:0.5; pointer-events:none" ><input type="radio" name="tab1_cmsDeparture" value="'+list_purpose[i]['value']+'"  class="Tour_platforn" data-title="'+list_purpose[i]['value']+'">・'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>'; 
        
            }else{
                 list_purpose[i]['count'] =   '（'+list_purpose[i]['count']+'）';
                 html += '<li><label><input type="radio" name="tab1_cmsDeparture" value="'+list_purpose[i]['value']+'"  class="Tour_platforn" data-title="'+list_purpose[i]['value']+'">・'+list_purpose[i]['value']+list_purpose[i]['count']+'</label></li>'; 
        
            }
        }
        if(tab == 1){
            $('#boxPlatform1 .modalRadioWrap').html('');
            $('#boxPlatform1 .modalRadioWrap').html(html);
            $('#boxPlatform1 .clickP01').html('');
            $('#boxPlatform1 .clickP01').html(route_sort);
        }else{

            $('#boxPlatform2 .modalRadioWrap').html('');
            $('#boxPlatform2 .modalRadioWrap').html(html);
            $('#boxPlatform2 .clickP01').html('');
            $('#boxPlatform2 .clickP01').html(route_sort);
        }
        setTimeout(function(){ $(".loading-spinner").css("display", "none"); }, 300);
    });
}
function InitRoute(tab){
    $(".loading-spinner").css("display", "block");
    boxRoute = 'boxPlatform1'
    class_tab = 'platformTab1'
    if(tab == 1){
        settabValue(1)
    }else{
         boxRoute = 'boxPlatform2'
         class_tab = 'platformTab2'
        settabValue(2)
    }    
    var params = $('#ApForm1').serializeArray();
    var url = rootUrl + 'tour/getPullDownRouteByCondition'; 
    $.ajax({
        method: 'POST',
        url: url,
        data: params
    })
    .done(function (msg) {
        list_purpose = $.parseJSON(msg);
        html = '<li class="li01"><label class="over"><input type="radio"  name="tab2_cmsDeparture" class="tab2_cmsDeparture" value="" data-title="指定しない"/>指定しない</label></li>';        
        if(tab == 1){
             boxRoute = 'boxPlatform1'
            class_tab = 'platformTab1'
        }else{
             boxRoute = 'boxPlatform2'
             class_tab = 'platformTab2'
        }  
        for (var i = 0; i <= list_purpose.length - 1; i++) {
            if(list_purpose[i]['count'] == 0){
                list_purpose[i]['count'] = ''; 
                html += '<li style="opacity:0.5; pointer-events:none"><a class="modal noSmoothScroll '+class_tab+'"  value="'+list_purpose[i]['value']+'" href="#'+boxRoute+'"><p>'+list_purpose[i]['value']+list_purpose[i]['count']+' ></p></a></li>'; 
            
            }else{
                list_purpose[i]['count'] =   '（'+list_purpose[i]['count']+'）';
                html += '<li><a class="modal noSmoothScroll '+class_tab+'"  value="'+list_purpose[i]['value']+'" href="#'+boxRoute+'"><p>'+list_purpose[i]['value']+list_purpose[i]['count']+' ></p></a></li>'; 
            
            }
           
        }
        if(tab == 1){        
            $('#boxRoute1').html('');
            $('#boxRoute1').html(html);
        }else{
            $('#boxRoute2').html('');
            $('#boxRoute2 ').html(html);
        }
        setTimeout(function(){ $(".loading-spinner").css("display", "none"); }, 300);
        
    });
}