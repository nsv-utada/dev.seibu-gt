! function (n) {
	var o = "is_open";
	/*n("html").prepend('<div class="windowload" style="position: fixed;background-color: #fff;width: 100%; height: 100%;z-index: 20000;"></div>');*/
	n("html").prepend('');	//2019.01.23 修正
	var t, e;
	t = this, e = function (a) {
		"use strict";
		var d, t, r, c, l, s = {},
			u = function () {},
			f = function (t, e) {
				if (null === t.offsetParent) return !1;
				var n = t.getBoundingClientRect();
				return n.right >= e.l && n.bottom >= e.t && n.left <= e.r && n.top <= e.b
			},
			h = function () {
				(c || !t) && (clearTimeout(t), t = setTimeout(function () {
					s.render(), t = null
				}, r))
			};
		return s.init = function (t) {
			var e = (t = t || {}).offset || 0,
				n = t.offsetVertical || e,
				o = t.offsetHorizontal || e,
				i = function (t, e) {
					return parseInt(t || e, 10)
				};
			d = {
				t: i(t.offsetTop, n),
				b: i(t.offsetBottom, n),
				l: i(t.offsetLeft, o),
				r: i(t.offsetRight, o)
			}, r = i(t.throttle, 250), c = !1 !== t.debounce, l = !!t.unload, u = t.callback || u, s.render(), document.addEventListener ? (a.addEventListener("scroll", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onscroll", h), a.attachEvent("onload", h))
		}, s.render = function (t) {
			for (var e, n, o = (t || document).querySelectorAll("[data-echo], [data-echo-background]"), i = o.length, r = {
					l: 0 - d.l,
					t: 0 - d.t,
					b: (a.innerHeight || document.documentElement.clientHeight) + d.b,
					r: (a.innerWidth || document.documentElement.clientWidth) + d.r
				}, c = 0; c < i; c++) n = o[c], f(n, r) ? (l && n.setAttribute("data-echo-placeholder", n.src), null !== n.getAttribute("data-echo-background") ? n.style.backgroundImage = "url(" + n.getAttribute("data-echo-background") + ")" : n.src !== (e = n.getAttribute("data-echo")) && (n.src = e), l || (n.removeAttribute("data-echo"), n.removeAttribute("data-echo-background")), u(n, "load")) : l && (e = n.getAttribute("data-echo-placeholder")) && (null !== n.getAttribute("data-echo-background") ? n.style.backgroundImage = "url(" + e + ")" : n.src = e, n.removeAttribute("data-echo-placeholder"), u(n, "unload"));
			i || s.detach()
		}, s.detach = function () {
			document.removeEventListener ? a.removeEventListener("scroll", h) : a.detachEvent("onscroll", h), clearTimeout(t)
		}, s
	}, "function" == typeof define && define.amd ? define(function () {
		return e(t)
	}) : "object" == typeof exports ? module.exports = e : t.echo = e(t);
	n(function () {
		var t, e;
		contentWidth = n(window).innerWidth(), echo.init({
				offset: 300
			}),
			function () {
				var t = location.pathname.split("/");
				t[t.indexOf("sp") + 1];
				n(".js_gnav").find(".menu").hide(), n(".js_gnav").find(".menuBtn").on("click", function () {
					n(this).toggleClass(o).next().slideToggle()
				})
			}(), t = location.pathname.split("/"), e = t[t.indexOf("sp") + 1], n(".js_accordion").find(".inner").hide(), n("#subContents > .category").find('.inner > a[href="/sp/' + e + '/"]').parent().show().prev().addClass(o), n(".js_accordion").find(".heading, .button").on("click", function () {
				n(this).toggleClass(o).next().slideToggle()
			})
	}), n(window).on("load", function () {
		var t, e;
		//n('.pagetop > a[href*="#document"], a[href*="#anc_"]').click(function () {
//			var t = n(n(this).attr("href")).offset();
//			return n("html, body").animate({
//				scrollTop: t.top
//			}, 400), !1
//		}), t = "is_open", e = ".accordionContent", n(".js_accordion").find(e).hide(), n(".js_accordion").find(".accordionHeading").on("click", function () {
//			n(this).toggleClass(t).next().stop().slideToggle()
//		}), n(".js_accordion").find(".closeBtn button").on("click", function () {
//			n(this).parents(e).stop().slideUp().prev().removeClass(t)
//		}), n(".windowload").fadeOut(250, function () {
//			n(".windowload").remove()
//		})
	})
}(jQuery);