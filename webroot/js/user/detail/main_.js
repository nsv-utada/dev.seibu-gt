jQuery(function($){

	$('.slickMainvisual').slick({
		autoplay:true,
		autoplaySpeed: 5000,
		dots: false,
		arrows: true
	});

	/*$('.slickTour').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		initialSlide: 0,
		dots: true,
		arrows: true,
		focusOnSelect: true,
		centerMode: true,
		centerPadding: 300,
		slidesToShow: 5,
		slidesToScroll: 1
	});*/
	$('.slickTour').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		initialSlide: 0,
		dots: true,
		arrows: true,
		focusOnSelect: true,
		centerMode: true,
		centerPadding: '2000px',
		slidesToShow: 1,
		//slidesToScroll: 1
	});


	$('.slickDtail').slick({
		autoplay: true,
		centerMode: true,
		centerPadding: '450px',
		slidesToShow: 1,
		responsive: [
   /* {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '100px',
        slidesToShow: 1
      }
    },*/
    {
      breakpoint: 670,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '100px',
        slidesToShow: 1
      }
    }
		]
	});


});


