

// forked from hibiki443's "アコーディオン（複数開ける）" http://jsdo.it/hibiki443/r8hF
(function($) {
    // 読み込んだら開始
    $(function() {
    
            // $("body").click(function() {
                
            //    // $(".switch").removeClass("open");
            // });
		
		
		
        // アコーディオン
        var accordion = $(".accordion");
        accordion.each(function () {
            var noTargetAccordion = $(this).siblings(accordion);
            $(this).find(".switch").click(function(event) {
							event.stopPropagation();
                $(this).next(".tabNaviAcCont01").slideToggle(0);
                $(this).toggleClass("open");
                noTargetAccordion.find(".tabNaviAcCont01").slideUp();
                //noTargetAccordion.find(".switch").removeClass("open");
            });
            $(this).find(".close").click(function() {
                var targetContentWrap = $(this).parent(".tabNaviAcCont01");
                $(targetContentWrap).slideToggle();
                $(targetContentWrap).prev(".switch").toggleClass("open");
            });
        });
    
    });
})(jQuery);

(function($) {
    // 読み込んだら開始
    $(function() {
		
        // アコーディオン
        var accordion = $(".accordion");
        accordion.each(function () {
            var noTargetAccordion = $(this).siblings(accordion);
            $(this).find(".switch02").click(function(event) {
							event.stopPropagation();
                $(this).next(".tabNaviAcCont02").slideToggle(0);
                $(this).toggleClass("open");
                noTargetAccordion.find(".tabNaviAcCont02").slideUp();
               // noTargetAccordion.find(".switch02").removeClass("open");
            });
            $(this).find(".close").click(function() {
                var targetContentWrap = $(this).parent(".tabNaviAcCont02");
                $(targetContentWrap).slideToggle();
                $(targetContentWrap).prev(".switch02").toggleClass("open");
            });
        });
    
    });
})(jQuery);



//スマホメニュー
jQuery(function() {
	jQuery('#menuSp').mmenu({
	   "extensions": [
		  "pagedim-black"
	   ],
	   "offCanvas": {
		  "position": "right"
	   }
	});
});


//検索エリア
$(function(){

	$('#tab1 .radioWrap01 .searchInputWrap01 label').click(function(){
		$('#tab1 .radioWrap01 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab1 .radioWrap01 .text02").text(t);
		//return false;
	})

	$('#tab1 .radioWrap02 .searchInputWrap01 label').click(function(){
		$('#tab1 .radioWrap02 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab1 .radioWrap02 .text02").text(t);
		//return false;
	})

	$('#tab1 .radioWrap03 .searchInputWrap01 label').click(function(){
		$('#tab1 .radioWrap03 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab1 .radioWrap03 .text02").text(t);
		//return false;
	})

//tab2用
	$('#tab2 .radioWrap01 .searchInputWrap01 label').click(function(){
		$('#tab2 .radioWrap01 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab2 .radioWrap01 .text02").text(t);
		//return false;
	})

	$('#tab2 .radioWrap02 .searchInputWrap01 label').click(function(){
		$('#tab2 .radioWrap02 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab2 .radioWrap02 .text02").text(t);
		//return false;
	})

	$('#tab2 .radioWrap03 .searchInputWrap01 label').click(function(){
		$('#tab2 .radioWrap03 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$("#tab2 .radioWrap03 .text02").text(t);
		//return false;
	})

//検索結果ページ用
	$('.resultsCell02 .radioWrap01 .searchInputWrap01 label').click(function(){
		$('.resultsCell02 .radioWrap01 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$(".resultsCell02 .radioWrap01 .text02").text(t);
		//return false;
	})

	$('.resultsCell02 .radioWrap02 .searchInputWrap01 label').click(function(){
		$('.resultsCell02 .radioWrap02 .searchInputWrap01 label').removeClass('over');
		$(this).addClass('over');
		var t = $(this).find("input").attr('data-title');
		$(".resultsCell02 .radioWrap02 .text02").text(t);
		//return false;
	})

//-----------------



});



/* .imghover ホバーした時の処理  */
$(function(){
	$('.imghover').live('mouseenter',function(){
		$(this).not(':animated').animate({'opacity':'0.7'},{duration:200});
	});
	$('.imghover').live('mouseleave',function(){
		$(this).animate({'opacity':'1'},{duration:200});
	});
});


	$(function() {
		$(".accordion02 .ac").click(function(){
				$(this).next("div").slideToggle();
				$(this).toggleClass("open");//.children(".ac2")
		});
	});


	$(function() {

		$( "#slider-range" ).slider({
			range: true,
			min: 1000,
			max: 50000,
			step: 1000,
			values: [ 10000, 30000 ],
			slide: function( event, ui ) {

				var ijyou01 = '';
				if(ui.values[ 1 ] >= 50000){
					ijyou01 = '以上';
				}else{
					ijyou01 = '';
				}
				$( "#amount" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou01 );
			}
		});
		$( "#amount" ).text( "" + $( "#slider-range" ).slider( "values", 0 ) +
			"円　 ～ 　" + $( "#slider-range" ).slider( "values", 1 ) + "円" );

		$( "#slider-range02" ).slider({
			range: true,
			min: 1000,
			max: 50000,
			step: 1000,
			values: [ 10000, 30000 ],
			slide: function( event, ui ) {

				var ijyou02 = '';
				if(ui.values[ 1 ] >= 50000){
					ijyou02 = '以上';
				}else{
					ijyou02 = '';
				}
				$( "#amount02" ).text( "" + ui.values[ 0 ] + "円　 ～ 　" + ui.values[ 1 ]　+ "円" + ijyou02 );
			}
		});
		$( "#amount02" ).text( "" + $( "#slider-range02" ).slider( "values", 0 ) +
			"円　 ～ 　" + $( "#slider-range02" ).slider( "values", 1 ) + "円" );

	});


