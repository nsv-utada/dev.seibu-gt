var utilsModule = (function() {
    var displayLoadingModal = function() {
        swal({
            text: '処理中です..... ' + "\n" + 'しばらくお待ちください。',
            icon: baseUrl + "/img/ajax-loader-tr.gif",
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: false
        });
    }
    
    var hideLoadingModal = function() {
        swal.close();
    } 

    return {
        displayLoadingModal: displayLoadingModal,
        hideLoadingModal: hideLoadingModal
    }
});